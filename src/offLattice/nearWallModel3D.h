
#ifndef NEAR_WALL_MODEL_3D_H
#define NEAR_WALL_MODEL_3D_H

#include <stack>
#include "offLattice/boundaryShapes3D.h"
#include "core/globalDefs.h"
#include "core/geometry3D.h"
#include "offLattice/triangleSet.h"
#include "offLattice/triangularSurfaceMesh.h"
#include "offLattice/offLatticeBoundaryProfiles3D.h"
#include "particles/multiParticleField3D.h"
#include "multiBlock/redistribution3D.h"
#include "multiBlock/multiDataField3D.h"
#include "atomicBlock/atomicContainerBlock3D.h"
#include "multiBlock/multiContainerBlock3D.h"
#include "offLattice/guoOffLatticeModel3D.h"

namespace plb {

///data processor for computing dynamicID from lattice
template<typename T, template<typename U> class Descriptor>
class DynamicIDFunctional3D : public BoxProcessingFunctional3D_LS<T,Descriptor,int>
{
public:
    virtual void process(Box3D domain, BlockLattice3D<T,Descriptor>& lattice,
                                       ScalarField3D<int>& scalarField);
    virtual DynamicIDFunctional3D<T,Descriptor>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
};

///Wrapper for compute dynamicID from lattice
template<typename T, template<typename U> class Descriptor>
void computeDynamicID(MultiBlockLattice3D<T,Descriptor>& lattice, MultiScalarField3D<int>& dynamicID, Box3D domain);

//data prepare for OffLatticeModel3D
template<typename T, template<typename U> class Descriptor>
class PackedRhoVelocityNufunctional3D : public BoxProcessingFunctional3D_LN<T,Descriptor,T>
{
public:
    PackedRhoVelocityNufunctional3D();
    virtual void process(Box3D domain, BlockLattice3D<T,Descriptor>& lattice,
                                       NTensorField3D<T>& rhoVelocityNu);
    virtual PackedRhoVelocityNufunctional3D<T,Descriptor>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
private:
    int initialized;
    T alphaFilter;
};

/// Description of a shape as the boundary of a given volume.
template<typename T, class SurfaceData>
struct BoundaryNearWallShape3D {
    virtual ~BoundaryNearWallShape3D() { }
    /// Decide whether a given discrete lattice node is inside the solid shape.
    /** The reason why there is an isInside function instead of a isOutside
     *  one is that while there exists only one voxelFlag::inside flag,
     *  there are several flags which count as outside: undetermined,
     *  outside, and borderline. This is particularly important for the
     *  undetermined flag, because the outer envelopes in a multi-block
     *  structure have no special meaning and are default-initialized to
     *  undetermined.
     **/
    virtual bool isInside(Dot3D const& location) const =0;
    /// Decide whether a given discrete lattice node is outside the solid shape.
    /** The reason why there is an isOutside function now, is that 
     *  there exists voxelFlag::outside, voxelFlag::outerBorder flags,
     *  there are several flags which count as outside: undetermined,
     *  outside, and borderline. This is particularly important for the
     *  undetermined flag, because the outer envelopes in a multi-block
     *  structure have no special meaning and are default-initialized to
     *  undetermined. In any case isInside or isOutside will be false in this case.
     **/
    virtual bool isOutside(Dot3D const& location) const =0;
    /// Get the distance to the wall, and the velocity value on the wall,
    ///   from a given real-valued position (in lattice units), and along
    ///   a given direction. Returns true if there exists an intersection
    ///   along the indicated direction.
    ///   ATTENTION: The id is an in-and-out value. If you know the right
    ///   id of the surface intersection (e.g. the triangle ID in case of
    ///   a triangular mesh) you can provide it to speed up the calculation.
    ///   However, if you don't know it, you MUST provide the value -1, 
    ///   because otherwise the result might be wrong.
    virtual bool pointOnSurface (
            Array<T,3> const& fromPoint, Array<T,3> const& direction,
            Array<T,3>& locatedPoint, T& distance,
            Array<T,3>& wallNormal, SurfaceData& surfaceData,
            OffBoundary::Type& bdType, plint& id ) const =0;
    /// Get the distance to the wall, and the data on the wall,
    ///   from a given discrete lattice node, and along a given direction.
    ///   Returns true if there exists an intersection along the indicated
    ///   direction.
    ///   ATTENTION: The id is an in-and-out value. If you know the right
    ///   id of the surface intersection (e.g. the triangle ID in case of
    ///   a triangular mesh) you can provide it to speed up the calculation.
    ///   However, if you don't know it, you MUST provide the value -1, 
    ///   because otherwise the result might be wrong.
    virtual bool gridPointOnSurface (
            Dot3D const& fromPoint, Dot3D const& direction,
            Array<T,3>& locatedPoint, T& distance,
            Array<T,3>& wallNormal, SurfaceData& surfaceData,
            OffBoundary::Type& bdType, plint& id ) const
    {
        return pointOnSurface(Array<T,3>((T)fromPoint.x, (T)fromPoint.y, (T)fromPoint.z),
                              Array<T,3>((T)direction.x, (T)direction.y, (T)direction.z),
                              locatedPoint, distance, wallNormal, surfaceData, bdType, id);
    }
    /// Given a point p on the surface of the shape, determine its "continuous normal".
    ///   If the shape is for example piecewise linear, the normal is adjusted to vary
    ///   continuously over the surface.
    virtual Array<T,3> computeContinuousNormal (
            Array<T,3> const& p, plint id, bool isAreaWeighted = false ) const =0;
    /// Say if a given segment intersects the surface.
    virtual bool intersectsSurface (
            Array<T,3> const& p1, Array<T,3> const& p2, plint& id ) const =0;
    /// Say if a given segment with integer-valued endpoints intersects the surface.
    virtual bool gridIntersectsSurface (
            Dot3D const& p1, Dot3D const& p2, plint& id ) const
    {
        return intersectsSurface( Array<T,3>((T)p1.x, (T)p1.y, (T)p1.z),
                                  Array<T,3>((T)p2.x, (T)p2.y, (T)p2.z), id );
    }
    /// Get the shortest distance to the wall. Returns true in case of success.
    ///   The flag isBehind indicates if the point is "behind" the wall, i.e.
    ///   in the direction opposite to the wall normal.
    virtual bool distanceToSurface( Array<T,3> const& point,
                                    T& distance, bool& isBehind ) const =0;
    /// Get the shortest distance to the wall. Returns true in case of success.
    ///   The flag isBehind indicates if the point is "behind" the wall, i.e.
    ///   in the direction opposite to the wall normal.
    virtual bool gridDistanceToNearWallSurface( Dot3D const& point,
                                        T& distance, Array<T,3>& normal, bool& isBehind ) const
    {
        return distanceToNearWallSurface( Array<T,3>((T)point.x, (T)point.y, (T)point.z),
                                  distance, normal, isBehind );
    }
    virtual bool distanceToNearWallSurface( Array<T,3> const& point,
                                    T& distance, Array<T,3>& normal, bool& isBehind ) const = 0;
    /// Return the tag (id of boundary-portion with specific boundary,
    ///   condition); the id is the one returned by pointOnSurface.
    virtual plint getTag(plint id) const =0;
    /// Plain clone function.
    virtual BoundaryNearWallShape3D<T,SurfaceData>* clone() const =0;
    /// In case the shape class needs additional meshed data, this clone function
    ///   offers the possibility to provide the data.
    virtual BoundaryNearWallShape3D<T,SurfaceData>* clone(std::vector<AtomicBlock3D*> args) const {
        return clone();
    }
};

template< typename T, class SurfaceData >
class TriangleFlowNearWallShape3D : public BoundaryNearWallShape3D<T,SurfaceData> {
public:
    TriangleFlowNearWallShape3D (
            TriangleBoundary3D<T> const& boundary_,
            BoundaryProfiles3D<T,SurfaceData> const& profiles_ );
    virtual bool isInside(Dot3D const& location) const;
    virtual bool isOutside(Dot3D const& location) const;
    virtual bool pointOnSurface (
            Array<T,3> const& fromPoint, Array<T,3> const& direction,
            Array<T,3>& locatedPoint, T& distance,
            Array<T,3>& wallNormal, SurfaceData& surfaceData,
            OffBoundary::Type& bdType, plint& id ) const;
    virtual Array<T,3> computeContinuousNormal (
            Array<T,3> const& p, plint id, bool isAreaWeighted = false ) const;
    virtual bool intersectsSurface (
            Array<T,3> const& p1, Array<T,3> const& p2, plint& id ) const;
    virtual plint getTag(plint id) const;
    virtual bool distanceToSurface( Array<T,3> const& point,
                                    T& distance, bool& isBehind ) const;
    virtual bool distanceToNearWallSurface( Array<T,3> const& point,
                                    T& distance, Array<T,3>& normal, bool& isBehind ) const;
    virtual TriangleFlowNearWallShape3D<T,SurfaceData>* clone() const;
    /// Use this clone function to provide the meshed data to this object.
    virtual TriangleFlowNearWallShape3D<T,SurfaceData>*
                clone(std::vector<AtomicBlock3D*> args) const;
private:
    TriangleBoundary3D<T> const& boundary;
    BoundaryProfiles3D<T,SurfaceData> const& profiles;
    /// Data from previous voxelization.
    ScalarField3D<int>* voxelFlags;
    /// Needed for fast access to the mesh.
    AtomicContainerBlock3D* hashContainer;
    AtomicBlock3D* boundaryArg;
};

template< typename T, class SurfaceData >
class OffLatticeNearWallModel3D {
public:
    OffLatticeNearWallModel3D(BoundaryNearWallShape3D<T,SurfaceData>* shape_, int flowType_);
    OffLatticeNearWallModel3D(OffLatticeNearWallModel3D<T,SurfaceData> const& rhs);
    OffLatticeNearWallModel3D<T,SurfaceData>& operator= (
            OffLatticeNearWallModel3D<T,SurfaceData> const& rhs );
    virtual ~OffLatticeNearWallModel3D();
    int getFlowType() const { return flowType; }
    void provideShapeArguments(std::vector<AtomicBlock3D*> args);
    plint getTag(plint id) const;
    bool pointOnSurface (
            Dot3D const& fromPoint, Dot3D const& direction,
            Array<T,3>& locatedPoint, T& distance,
            Array<T,3>& wallNormal, SurfaceData& surfaceData,
            OffBoundary::Type& bdType, plint& id ) const;
    bool distanceToNearWallSurface (
            Dot3D const& fromPoint, T& distance,
            Array<T,3>& wallNormal, bool isBehind ) const;
    bool distanceToSurface (
        Dot3D const& point, T& distance, bool& isBehind ) const {
                return shape->distanceToSurface(Array<T,3>(point.x,point.y,point.z), distance, isBehind);}
    Array<T,3> computeContinuousNormal (
            Array<T,3> const& p, plint id, bool isAreaWeighted = false ) const;
    bool intersectsSurface (
            Dot3D const& p1, Dot3D const& p2, plint& id ) const;
    bool isFluid(Dot3D const& location) const;
    bool isSolid(Dot3D const& location) const;

    bool velIsJ() const { return velIsJflag; }
    void setVelIsJ(bool velIsJflag_) { velIsJflag = velIsJflag_; }
    bool getPartialReplace() const { return partialReplaceFlag; }
    void setPartialReplace(bool prFlag) { partialReplaceFlag = prFlag; }
    void selectSecondOrder(bool flag) { secondOrderFlag = flag; }
    bool usesSecondOrder() const { return secondOrderFlag; }
    void selectUseRegularizedModel(bool flag) { regularizedModel = flag; }
    bool usesRegularizedModel() const { return regularizedModel; }
    void selectComputeStat(bool flag) { computeStat = flag; }
    bool computesStat() const { return computeStat; }
    void setDefineVelocity(bool defineVelocity_) { defineVelocity = defineVelocity_; }
    bool getDefineVelocity() const { return defineVelocity; }

    virtual OffLatticeNearWallModel3D<T,SurfaceData>* clone() const =0;
    virtual plint getNumNeighbors() const =0;
    virtual bool isExtrapolated() const =0;
    virtual void prepareCell (
            Dot3D const& cellLocation, AtomicContainerBlock3D& container ) =0;
    //compute wall distance and normal of cellLocation
    virtual void distanceYNormalCell (
            Dot3D const& cellLocation, T &minut, T &maxut, AtomicBlock3D& nonTypeLattice, std::vector<AtomicBlock3D *> & args ) =0;
    virtual void boundaryCompletion (
            AtomicBlock3D& lattice,
            AtomicContainerBlock3D& container,
            std::vector<AtomicBlock3D *> const& args ) =0;
    virtual ContainerBlockData* generateOffLatticeInfo() const =0;
    virtual Array<T,9> getLocalForce(AtomicContainerBlock3D& container) const =0;
    virtual Array<T, 9>& getForce(AtomicContainerBlock3D &container) = 0;
private:
    BoundaryNearWallShape3D<T,SurfaceData>* shape;
    int flowType;
    bool velIsJflag;
    bool partialReplaceFlag;
    bool secondOrderFlag;
    bool regularizedModel;
    bool computeStat;
    bool defineVelocity;
};

/// Precompute a list of nodes which are close to the off-lattice boundary
///   (both wet and dry ones).
template<typename T, class SurfaceData>
class OffLatticeNearWallPatternFunctional3D : public BoxProcessingFunctional3D
{
public:
    /// numNeighbors is the number of neighbors a boundary node must be able
    ///   to access along a given direction.
    OffLatticeNearWallPatternFunctional3D (
            OffLatticeNearWallModel3D<T,SurfaceData>* offLatticeModel_ );
    virtual ~OffLatticeNearWallPatternFunctional3D();
    OffLatticeNearWallPatternFunctional3D(OffLatticeNearWallPatternFunctional3D const& rhs);
    OffLatticeNearWallPatternFunctional3D& operator= (
            OffLatticeNearWallPatternFunctional3D const& rhs );
    void swap(OffLatticeNearWallPatternFunctional3D& rhs);
    virtual OffLatticeNearWallPatternFunctional3D<T,SurfaceData>* clone() const;

    /// First AtomicBlock: OffLatticeInfo.
    ///   If there are more atomic-blocks then they are forwarded to the
    ///   shape function, to provide additional read-only parameters.
    /// It is very important that the "offLatticePattern" container block
    /// (the first atomic-block passed) has the same multi-block management
    /// as the lattice used in the simulation.
    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> fields);
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
    virtual BlockDomain::DomainT appliesTo() const;
private:
    OffLatticeNearWallModel3D<T,SurfaceData>* offLatticeModel;
};

template< typename T, class SurfaceData >
class GetForceOnAhmedFunctional3D : public PlainReductiveBoxProcessingFunctional3D
{
public:
    GetForceOnAhmedFunctional3D (
            OffLatticeNearWallModel3D<T,SurfaceData>* offLatticeModel_ );
    virtual ~GetForceOnAhmedFunctional3D();
    GetForceOnAhmedFunctional3D(GetForceOnAhmedFunctional3D<T,SurfaceData> const& rhs);
    GetForceOnAhmedFunctional3D<T,SurfaceData>& operator= (
            GetForceOnAhmedFunctional3D<T,SurfaceData> const& rhs );

    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> fields);
    virtual GetForceOnAhmedFunctional3D<T,SurfaceData>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
    virtual BlockDomain::DomainT appliesTo() const;
    Array<T,9> getForce() const;
private:
    OffLatticeNearWallModel3D<T,SurfaceData>* offLatticeModel;
    Array<plint,9> forceId;
};

template< typename T, class BoundaryType >
Array<T,9> getForceOnAhmed (
        MultiBlock3D& offLatticePattern, 
        OffLatticeNearWallModel3D<T,BoundaryType> const& offLatticeModel );

template<typename T, template<typename U> class Descriptor, class SurfaceData>
class OffLatticeNearWallCompletionFunctional3D : public BoxProcessingFunctional3D
{
public:
    OffLatticeNearWallCompletionFunctional3D (
            OffLatticeNearWallModel3D<T,SurfaceData>* offLatticeModel_,
            plint numShapeArgs_, plint numCompletionArgs_ );
    virtual ~OffLatticeNearWallCompletionFunctional3D();
    OffLatticeNearWallCompletionFunctional3D(OffLatticeNearWallCompletionFunctional3D<T,Descriptor,SurfaceData> const& rhs);
    OffLatticeNearWallCompletionFunctional3D& operator= (
            OffLatticeNearWallCompletionFunctional3D<T,Descriptor,SurfaceData> const& rhs );
    void swap(OffLatticeNearWallCompletionFunctional3D<T,Descriptor,SurfaceData>& rhs);

    /// First AtomicBlock: Lattice; Second AtomicBlock: Container for off-
    ///   lattice info.
    ///   If there are more atomic-blocks then they are forwarded to the
    ///   shape function, to provide additional read-only parameters.
    ///
    /// It is very important that the "offLatticePattern" container block
    /// which is passed as the second atomic-block with the off-lattice info
    /// has the same multi-block management as the lattice used in the simulation.
    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> fields);
    virtual OffLatticeNearWallCompletionFunctional3D<T,Descriptor,SurfaceData>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
    virtual BlockDomain::DomainT appliesTo() const;
private:
    OffLatticeNearWallModel3D<T,SurfaceData>* offLatticeModel;
    plint numShapeArgs, numCompletionArgs;
};

template< typename T, class SurfaceData >
class GetForceOnObjectNearWallFunctional3D : public PlainReductiveBoxProcessingFunctional3D
{
public:
    GetForceOnObjectNearWallFunctional3D (
            OffLatticeNearWallModel3D<T,SurfaceData>* offLatticeModel_ );
    virtual ~GetForceOnObjectNearWallFunctional3D();
    GetForceOnObjectNearWallFunctional3D(GetForceOnObjectNearWallFunctional3D<T,SurfaceData> const& rhs);
    GetForceOnObjectNearWallFunctional3D<T,SurfaceData>& operator= (
            GetForceOnObjectNearWallFunctional3D<T,SurfaceData> const& rhs );

    /// First AtomicBlock: Container for off-lattice info.
    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> fields);
    virtual GetForceOnObjectNearWallFunctional3D<T,SurfaceData>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
    virtual BlockDomain::DomainT appliesTo() const;
    Array<T,9> getForce() const;
private:
    OffLatticeNearWallModel3D<T,SurfaceData>* offLatticeModel;
    Array<plint,9> forceId;
};

template< typename T, class BoundaryType >
Array<T,9> getForceOnObject (
        MultiBlock3D& offLatticePattern, 
        OffLatticeNearWallModel3D<T,BoundaryType> const& offLatticeModel );

///compute wallFunctionY and normal on stencil cells, wallFunctionY is the shortest distance between stencil cells and wall surface. normal is the normal of a triangle corresponding to the shortest distance.
template<typename T, template<typename U> class Descriptor, class SurfaceData>
class OffLatticeWallDistanceYNormalFunctional3D : public BoxProcessingFunctional3D
{
public:
    OffLatticeWallDistanceYNormalFunctional3D (
            OffLatticeNearWallModel3D<T,SurfaceData>* offLatticeModel_,
            plint numShapeArgs_, plint numCompletionArgs_ );
    virtual ~OffLatticeWallDistanceYNormalFunctional3D();
    OffLatticeWallDistanceYNormalFunctional3D(OffLatticeWallDistanceYNormalFunctional3D<T,Descriptor,SurfaceData> const& rhs);
    OffLatticeWallDistanceYNormalFunctional3D& operator= (
            OffLatticeWallDistanceYNormalFunctional3D<T,Descriptor,SurfaceData> const& rhs );
    void swap(OffLatticeWallDistanceYNormalFunctional3D<T,Descriptor,SurfaceData>& rhs);

    /// First AtomicBlock: Lattice; Second AtomicBlock: Container for off-
    ///   lattice info.
    ///   If there are more atomic-blocks then they are forwarded to the
    ///   shape function, to provide additional read-only parameters.
    ///
    /// It is very important that the "offLatticePattern" container block
    /// which is passed as the second atomic-block with the off-lattice info
    /// has the same multi-block management as the lattice used in the simulation.
    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> fields);
    virtual OffLatticeWallDistanceYNormalFunctional3D<T,Descriptor,SurfaceData>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
    virtual BlockDomain::DomainT appliesTo() const;
private:
    OffLatticeNearWallModel3D<T,SurfaceData>* offLatticeModel;
    plint numShapeArgs, numCompletionArgs;
    T minut, maxut;
};

template<typename T, template<typename U> class Descriptor>
class FilippovaHaenelJohanNearWallOffLatticeModel3D : public OffLatticeNearWallModel3D<T, Array < T,3> >
{
public:
    FilippovaHaenelJohanNearWallOffLatticeModel3D(BoundaryNearWallShape3D<T,Array<T,3> >* shape_, int flowType_);
    virtual FilippovaHaenelJohanNearWallOffLatticeModel3D<T,Descriptor>* clone() const;
    virtual plint getNumNeighbors() const;
    virtual bool isExtrapolated() const;
    virtual void prepareCell (
            Dot3D const& cellLocation, AtomicContainerBlock3D& container );
    virtual void distanceYNormalCell (
            Dot3D const& cellLocation, T &minut, T &maxut, AtomicBlock3D& nonTypeLattice, std::vector<AtomicBlock3D *> & args );
    virtual void boundaryCompletion (
            AtomicBlock3D& lattice, AtomicContainerBlock3D& container,
            std::vector<AtomicBlock3D *> const& args );

    virtual ContainerBlockData* generateOffLatticeInfo() const;
    virtual Array<T,9> getLocalForce(AtomicContainerBlock3D& container) const;
    virtual Array<T, 9>& getForce(AtomicContainerBlock3D &container) ;    
    void selectComputeStat(bool flag) { computeStat = flag; }
    bool computesStat() const { return computeStat; }
    virtual void setDx(T dx_, T groundClearance_);
private:
    void cellCompletion (
            BlockLattice3D<T,Descriptor>& lattice,
            Dot3D const& guoNode,
            std::vector<int> const& dryNodeFluidDirections,
            std::vector<plint> const& dryNodeIds, std::vector<bool> const &hasFluidNeighbor,Dot3D const& absoluteOffset,
            Array<T,9>& localForce, std::vector<AtomicBlock3D *> const& args, pluint iDry );

    void stencilMark (
            BlockLattice3D<T,Descriptor>& lattice,
            Dot3D const& guoNode,
            std::vector<int> const& dryNodeFluidDirections,
            std::vector<AtomicBlock3D *> & args, pluint iDry, Box3D const & domain );
private:
    std::vector<T> invAB;
    bool computeStat;
    T dx;
    T groundClearance;
private:
    /// Store the location of wall nodes, as well as the pattern of missing vs. known
    ///   populations.
    class OffLatticeInfo3D : public ContainerBlockData {
    public:
        OffLatticeInfo3D()
            : localForce(Array<T,9>::zero())
        { }
        std::vector<Dot3D> const&                getDryNodes() const
        { return dryNodes; }
        std::vector<Dot3D>&                      getDryNodes()
        { return dryNodes; }
        std::vector<std::vector<int > > const&   getDryNodeFluidDirections() const
        { return dryNodeFluidDirections; }
        std::vector<std::vector<int > >&         getDryNodeFluidDirections()
        { return dryNodeFluidDirections; }
        std::vector<std::vector<plint> > const&  getDryNodeIds() const
        { return dryNodeIds; }
        std::vector<std::vector<plint> >&        getDryNodeIds()
        { return dryNodeIds; }
        std::vector<Dot3D> const&                getBdNodes() const
        { return bdNodes; }
        std::vector<Dot3D>&                      getBdNodes()
        { return bdNodes; }
        std::vector<std::vector<int > > const&   getBdNodeSolidDirections() const
        { return bdNodeSolidDirections; }
        std::vector<std::vector<int > >&         getBdNodeSolidDirections()
        { return bdNodeSolidDirections; }
        std::vector<std::vector<plint> > const&  getBdNodeIds() const
        { return bdNodeIds; }
        std::vector<std::vector<plint> >&        getBdNodeIds()
        { return bdNodeIds; }

        std::vector<std::vector<bool> > const &getHasFluidNeighbor() const
        {
            return hasFluidNeighbor;
        }
        std::vector<std::vector<bool> > &getHasFluidNeighbor()
        {
            return hasFluidNeighbor;
        }

        Array<T,9> const&                        getLocalForce() const
        { return localForce; }
        Array<T,9>&                              getLocalForce()
        { return localForce; }
        virtual OffLatticeInfo3D* clone() const {
            return new OffLatticeInfo3D(*this);
        }
    private:
        std::vector<Dot3D> dryNodes;
        std::vector<Dot3D> bdNodes;
        std::vector<std::vector<int> >   dryNodeFluidDirections;
        std::vector<std::vector<int> >   bdNodeSolidDirections;
        std::vector<std::vector<plint> > dryNodeIds;
        std::vector<std::vector<plint> > bdNodeIds;
	std::vector<std::vector<bool>>   hasFluidNeighbor;
        Array<T,9>                       localForce;
    };
};

template< typename T, class SurfaceData >
class GetForceOnMeshNearWallFunctional3D : public PlainReductiveBoxProcessingFunctional3D
{
public:
    GetForceOnMeshNearWallFunctional3D (
        OffLatticeNearWallModel3D<T,SurfaceData>* offLatticeModel_ );
    virtual ~GetForceOnMeshNearWallFunctional3D();
    GetForceOnMeshNearWallFunctional3D(GetForceOnMeshNearWallFunctional3D<T,SurfaceData> const& rhs);
    GetForceOnMeshNearWallFunctional3D<T,SurfaceData>& operator= (
        GetForceOnMeshNearWallFunctional3D<T,SurfaceData> const& rhs );

    /// First AtomicBlock: Container for off-lattice info.
    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> fields);
    virtual GetForceOnMeshNearWallFunctional3D<T,SurfaceData>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
    virtual BlockDomain::DomainT appliesTo() const;
    Array<T, 9> getForce() const;
private:
    OffLatticeNearWallModel3D<T,SurfaceData>* offLatticeModel;
    Array<plint, 9> forceId;
};

template< typename T, class BoundaryType >
Array<T, 9> getForceOnMesh (
    MultiBlock3D& offLatticePattern,
    OffLatticeNearWallModel3D<T,BoundaryType> const& offLatticeModel );
/////////////////////////////////////////////////////////
//data processor for calculating utau on donor nodes of stencil
template<typename T, template<typename U> class Descriptor>
class stencilUtauCompFunctional3D : public BoxProcessingFunctional3D
{
public:
    stencilUtauCompFunctional3D(int numShapeArgs_, int numCompletionArgs_);
    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> blocks);
    virtual stencilUtauCompFunctional3D<T,Descriptor>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
    virtual BlockDomain::DomainT appliesTo() const;
private:
    int numShapeArgs;
    int numCompletionArgs;
    int wallFunctionMethod;
    //void computeUtau(T & utauDonor, Array<T,3> distanceNormal, Array<T,3> velocity, T nu, int wallMethod);
    //void computeUtau(Dot3D const & globalNode, T & utauDonor, Array<T,3> distanceNormal, Array<T,3> velocity, T nu, int wallMethod);
    void computeUtau(Dot3D const & globalNode, T & utauDonor, T wallDistance, T tangentialVelocity, T rho, T dpds, T nu, int wallMethod, bool isforward);
};
/////////////////////////////////////
//data processor for interpolating utau and density from donor nodes to boundry nodes (fluid neighbor nodes)
template<typename T, template<typename U> class Descriptor>
class boundaryUtauDensityInterpFunctional3D : public BoxProcessingFunctional3D
{
public:
    boundaryUtauDensityInterpFunctional3D(int numShapeArgs_, int numCompletionArgs_);
    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> blocks);
    virtual boundaryUtauDensityInterpFunctional3D<T,Descriptor>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
    virtual BlockDomain::DomainT appliesTo() const;
private:
    int numShapeArgs;
    int numCompletionArgs;
    void interpolateUtauDensity(const Dot3D& boundaryNode, BlockLattice3D<T,Descriptor> const & lattice, ScalarField3D<int> const & stencilFlag, TensorField3D<T,3> const & wallDistanceY, ScalarField3D<T> & utau, NTensorField3D<T> & rhoVelocityNuField);
    void interpolateUtauDensity(const Dot3D& boundaryNode, BlockLattice3D<T,Descriptor> const & lattice, ScalarField3D<int> const & stencilFlag, TensorField3D<T,3> const & wallDistanceY, ScalarField3D<T> & utau, NTensorField3D<T> const & rhoVelocityNuField, TensorField3D<T,4> & modifyRhoVelocity);
    void updateSumWeightAndUtauDonor(const Dot3D & boundaryNode, const Dot3D & neighbor, Array<T,3> const & wallDistanceYNormal, T const & utauDonor, T & sumWeightUtau, T & sumWeightUtauXUtau);
    void updateSumWeightAndRhoDonor(Array<T,3> const & boundaryNode, const Dot3D & neighbor, Array<T,3> const & wallDistanceYNormal, T const & rhoDonor, Array<T,3> const & velocity, T & sumWeightRho, T & sumWeightU, Array<T,3> &sumWeightVelocity, T & sumWeightRhoXRho);
};
////////////////////////////////////////////////////////////////////
//data processor for updating velocity on boudnary nodes (neighbor fluid cells) with the help of utau
template<typename T, template<typename U> class Descriptor>
class velocityOnBoundaryUpdateFunctional3D : public BoxProcessingFunctional3D
{
public:
    velocityOnBoundaryUpdateFunctional3D(int numShapeArgs_, int numCompletionArgs_);
    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> blocks);
    virtual velocityOnBoundaryUpdateFunctional3D<T,Descriptor>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
    virtual BlockDomain::DomainT appliesTo() const;
private:
    int numShapeArgs;
    int numCompletionArgs;
    int wallFunctionMethod;
    T uPlusOrgLog(const T yPlus);
    T uPlusMusker(const T yPlus);
    T uPlusAPG(const T yPlus);
    T uPlusSA(const T yPlus);
    T uPlusAfzal(const T yPlus, const T kPlus, bool isforward);
};
////////////////////////////////////////////////////////////////////
//data processor for computing stress on boundary nodes
template<typename T, template<typename U> class Descriptor>
class stressOnBoundaryCompFunctional3D : public BoxProcessingFunctional3D
{
public:
    stressOnBoundaryCompFunctional3D(int numShapeArgs_, int numCompletionArgs_);
    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> blocks);
    virtual stressOnBoundaryCompFunctional3D<T,Descriptor>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
    virtual BlockDomain::DomainT appliesTo() const;
private:
    int numShapeArgs;
    int numCompletionArgs;
    int wallFunctionMethod;
    int computeVelocityDeriveDirection(Dot3D const & boundaryNode, Dot3D const & direction, BlockLattice3D<T,Descriptor> const & lattice, NTensorField3D<T> const & rhoVelocityNuField, TensorField3D<T,3> const & wallDistanceY, Array<T,3> & velocityDerive, T & U_tDerive);
    T getVelocityTangentialMagnitude(Array<T,3> const & velocity, Array<T,3> const & distanceNormal);
    void U_tDeriveComp(T & U_tDerive, const bool fluidPos, const bool fluidNeg, T const & U_tPos, T const & U_tBoundaryNode, T const & U_tNeg);
    void velocityDeriveComp(Array<T,3> & velocityDerive, const bool fluidPos, const bool fluidNeg, Array<T,3> const & velocityPos, Array<T,3> const & velocityBoundaryNode, Array<T,3> const & velocityNeg);
    void computeVelocityTangentialDeriveNormal(Dot3D const & boundaryNode, BlockLattice3D<T,Descriptor> const & lattice, ScalarField3D<T> const & utau, NTensorField3D<T> const & rhoVelocityNuField, TensorField3D<T,3> const & wallDistanceY, T & dU_tdn);
    T uPlusOrgLog(const T yPlus);
    T uPlusMusker(const T yPlus);
    T uPlusSA(const T yPlus);
    T uPlusAPG(const T yPlus);
    void updateVelocityDeriveByWallFunction(Dot3D const & boundaryNode, BlockLattice3D<T,Descriptor> const & lattice, NTensorField3D<T> const & rhoVelocityNuField, TensorField3D<T,3> const & wallDistanceY, T const & dU_tdx, T const & dU_tdy, T const & dU_tdz, T const & dU_tdn, Array<T,3> & dUdx, Array<T,3> & dUdy, Array<T,3> & dUdz);
    void computeStress(Dot3D const & boundaryNode, BlockLattice3D<T,Descriptor> const & lattice, Array<T,3> const & dUdx, Array<T,3> const & dUdy, Array<T,3> const & dUdz, TensorField3D<T,6> & stressField);


};

/////////////////////////////////////////////////////////
//data processor for finding and marking boundary nodes of stencil
template<typename T, template<typename U> class Descriptor>
class boundaryNodeStencilCompFunctional3D : public BoxProcessingFunctional3D
{
public:
    boundaryNodeStencilCompFunctional3D(int numShapeArgs_, int numCompletionArgs_);
    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> blocks);
    virtual boundaryNodeStencilCompFunctional3D<T,Descriptor>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
    virtual BlockDomain::DomainT appliesTo() const;
private:
    int numShapeArgs;
    int numCompletionArgs;
};
/////////////////////////////////////
//data processor for finding and marking donor nodes of stencil
//It can only be used after boundary node treatment
template<typename T, template<typename U> class Descriptor>
class donorNodeStencilCompFunctional3D : public BoxProcessingFunctional3D
{
public:
    donorNodeStencilCompFunctional3D(int numShapeArgs_, int numCompletionArgs_);
    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> blocks);
    virtual donorNodeStencilCompFunctional3D<T,Descriptor>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
    virtual BlockDomain::DomainT appliesTo() const;
private:
    int numShapeArgs;
    int numCompletionArgs;
};
//data analysis processor for setting Csmago in LES model for Johan model
template<typename T, template<typename U> class Descriptor>
class SetLESCsmagoJohanFunctional3D : public BoxProcessingFunctional3D
{
public:
    SetLESCsmagoJohanFunctional3D(int numShapeArgs_, int numCompletionArgs_, T cSmago_);
    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> blocks);
    virtual SetLESCsmagoJohanFunctional3D<T,Descriptor>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
private:
    T cSmago;
    int numShapeArgs;
    int numCompletionArgs;
};

template<typename T>
struct OffLatticeBoundaryCondition2Base3D {
    virtual ~OffLatticeBoundaryCondition2Base3D() { }
    virtual void apply() = 0;
    virtual void insert(plint processorLevel) = 0;
    virtual void apply(std::vector<MultiBlock3D*> const& completionArg) = 0;
    virtual void insert(std::vector<MultiBlock3D*> const& completionArg, plint processorLevel) = 0;
    virtual Array<T,9> getForceOnObject() = 0;
};
template< typename T,
          template<typename U> class Descriptor,
          class BoundaryType >
class OffLatticeNearWallBoundaryCondition3D : public OffLatticeBoundaryCondition2Base3D<T> {
public:
    OffLatticeNearWallBoundaryCondition3D (
            OffLatticeNearWallModel3D<T,BoundaryType>* offLatticeModel_,
            VoxelizedDomain3D<T>& voxelizedDomain_,
            MultiBlockLattice3D<T,Descriptor>& lattice_ );
    OffLatticeNearWallBoundaryCondition3D (
            OffLatticeNearWallModel3D<T,BoundaryType>* offLatticeModel_,
            VoxelizedDomain3D<T>& voxelizedDomain_,
            MultiBlockLattice3D<T,Descriptor>& lattice_,
            MultiParticleField3D<DenseParticleField3D<T,Descriptor> >& particleField_ );
    OffLatticeNearWallBoundaryCondition3D (
            OffLatticeNearWallBoundaryCondition3D<T,Descriptor,BoundaryType> const& rhs );
    ~OffLatticeNearWallBoundaryCondition3D();
    MultiBlockLattice3D<T,Descriptor> const& getLattice() const { return lattice; }
    VoxelizedDomain3D<T> const& getVoxelizedDomain() const { return voxelizedDomain; }
    VoxelizedDomain3D<T>& getVoxelizedDomain() { return voxelizedDomain; }
    virtual void apply();
    virtual void insert(plint processorLevel = 1);
    virtual void apply(std::vector<MultiBlock3D*> const& completionArg);
    virtual void insert(std::vector<MultiBlock3D*> const& completionArg, plint processorLevel = 1);
    virtual Array<T,9> getForceOnObject();
    virtual Array<T, 9> getForceOnMesh(RawConnectedTriangleMesh<T>* triangleMesh, plint propertyID, std::vector<MultiBlock3D *>& completionArg, T fScale);    
    std::unique_ptr<MultiTensorField3D<T,3> > computeVelocity(Box3D domain);
    std::unique_ptr<MultiTensorField3D<T,3> > computeVelocity();
    std::unique_ptr<MultiTensorField3D<T,3> > computeVorticity(Box3D domain);
    std::unique_ptr<MultiTensorField3D<T,3> > computeVorticity();
    std::unique_ptr<MultiScalarField3D<T> > computeVelocityNorm(Box3D domain);
    std::unique_ptr<MultiScalarField3D<T> > computeVelocityNorm();
    std::unique_ptr<MultiScalarField3D<T> > computeVorticityNorm(Box3D domain);
    std::unique_ptr<MultiScalarField3D<T> > computeVorticityNorm();
    std::unique_ptr<MultiScalarField3D<T> > computeVelocityComponent(Box3D domain, plint iComp);
    std::unique_ptr<MultiScalarField3D<T> > computeVelocityComponent(plint iComp);
    std::unique_ptr<MultiScalarField3D<T> > computePressure(Box3D domain);
    std::unique_ptr<MultiScalarField3D<T> > computePressure();
    std::unique_ptr<MultiScalarField3D<T> > computeDensity(Box3D domain, T solidDensity=T());
    std::unique_ptr<MultiScalarField3D<T> > computeDensity(T solidDensity=T());
    std::unique_ptr<MultiScalarField3D<T> > computeStrainRateNorm();
    std::unique_ptr<MultiScalarField3D<T> > computeStrainRateNorm(Box3D domain);
    std::unique_ptr<MultiTensorField3D<T,SymmetricTensor<T,Descriptor>::n> > computeStrainRate();
    std::unique_ptr<MultiTensorField3D<T,SymmetricTensor<T,Descriptor>::n> > computeStrainRate(Box3D domain);
    std::unique_ptr<MultiScalarField3D<T> > computeShearStressNorm();
    std::unique_ptr<MultiScalarField3D<T> > computeShearStressNorm(Box3D domain);
    std::unique_ptr<MultiTensorField3D<T,SymmetricTensor<T,Descriptor>::n> > computeShearStress();
    std::unique_ptr<MultiTensorField3D<T,SymmetricTensor<T,Descriptor>::n> > computeShearStress(Box3D domain);
    T computeAverageVelocityComponent(Box3D domain, plint iComponent);
    Array<T,3> computeAverageVelocity(Box3D domain);
    T computeAverageDensity(Box3D domain);
    T computeAverageDensity();
    T computeAverageEnergy(Box3D domain);
    T computeAverageEnergy();
    T computeRMSvorticity(Box3D domain);
    T computeRMSvorticity();
    T computeAverageShearStressNorm(Box3D domain);
    T computeAverageShearStressNorm();
    T computeRMSshearStressNorm(Box3D domain);
    T computeRMSshearStressNorm();
private:
    VoxelizedDomain3D<T>& voxelizedDomain;
    MultiBlockLattice3D<T,Descriptor>& lattice;
    MultiBlock3D& boundaryShapeArg;
    OffLatticeNearWallModel3D<T,BoundaryType>* offLatticeModel;
    MultiContainerBlock3D offLatticePattern;
};

template< typename T,
          template<typename U> class Descriptor,
          class BoundaryType >
class OffLatticeImplicitWallBoundaryCondition3D : public OffLatticeBoundaryCondition2Base3D<T> {
public:
    OffLatticeImplicitWallBoundaryCondition3D (
            OffLatticeNearWallModel3D<T,BoundaryType>* offLatticeModel_,
            VoxelizedDomain3D<T>& voxelizedDomain_,
            MultiBlockLattice3D<T,Descriptor>& lattice_ );
    OffLatticeImplicitWallBoundaryCondition3D (
            OffLatticeNearWallModel3D<T,BoundaryType>* offLatticeModel_,
            VoxelizedDomain3D<T>& voxelizedDomain_,
            MultiBlockLattice3D<T,Descriptor>& lattice_,
            MultiParticleField3D<DenseParticleField3D<T,Descriptor> >& particleField_ );
    OffLatticeImplicitWallBoundaryCondition3D (
            OffLatticeImplicitWallBoundaryCondition3D<T,Descriptor,BoundaryType> const& rhs );
    ~OffLatticeImplicitWallBoundaryCondition3D();
    MultiBlockLattice3D<T,Descriptor> const& getLattice() const { return lattice; }
    VoxelizedDomain3D<T> const& getVoxelizedDomain() const { return voxelizedDomain; }
    VoxelizedDomain3D<T>& getVoxelizedDomain() { return voxelizedDomain; }
    virtual void apply();
    virtual void insert(plint processorLevel = 1);
    virtual void apply(std::vector<MultiBlock3D*> const& completionArg);
    virtual void insert(std::vector<MultiBlock3D*> const& completionArg, plint processorLevel = 1);
    virtual Array<T,9> getForceOnObject();
    std::unique_ptr<MultiTensorField3D<T,3> > computeVelocity(Box3D domain);
    std::unique_ptr<MultiTensorField3D<T,3> > computeVelocity();
    std::unique_ptr<MultiTensorField3D<T,3> > computeVorticity(Box3D domain);
    std::unique_ptr<MultiTensorField3D<T,3> > computeVorticity();
    std::unique_ptr<MultiScalarField3D<T> > computeVelocityNorm(Box3D domain);
    std::unique_ptr<MultiScalarField3D<T> > computeVelocityNorm();
    std::unique_ptr<MultiScalarField3D<T> > computeVorticityNorm(Box3D domain);
    std::unique_ptr<MultiScalarField3D<T> > computeVorticityNorm();
    std::unique_ptr<MultiScalarField3D<T> > computeVelocityComponent(Box3D domain, plint iComp);
    std::unique_ptr<MultiScalarField3D<T> > computeVelocityComponent(plint iComp);
    std::unique_ptr<MultiScalarField3D<T> > computePressure(Box3D domain);
    std::unique_ptr<MultiScalarField3D<T> > computePressure();
    std::unique_ptr<MultiScalarField3D<T> > computeDensity(Box3D domain, T solidDensity=T());
    std::unique_ptr<MultiScalarField3D<T> > computeDensity(T solidDensity=T());
    std::unique_ptr<MultiScalarField3D<T> > computeStrainRateNorm();
    std::unique_ptr<MultiScalarField3D<T> > computeStrainRateNorm(Box3D domain);
    std::unique_ptr<MultiTensorField3D<T,SymmetricTensor<T,Descriptor>::n> > computeStrainRate();
    std::unique_ptr<MultiTensorField3D<T,SymmetricTensor<T,Descriptor>::n> > computeStrainRate(Box3D domain);
    std::unique_ptr<MultiScalarField3D<T> > computeShearStressNorm();
    std::unique_ptr<MultiScalarField3D<T> > computeShearStressNorm(Box3D domain);
    std::unique_ptr<MultiTensorField3D<T,SymmetricTensor<T,Descriptor>::n> > computeShearStress();
    std::unique_ptr<MultiTensorField3D<T,SymmetricTensor<T,Descriptor>::n> > computeShearStress(Box3D domain);
    T computeAverageVelocityComponent(Box3D domain, plint iComponent);
    Array<T,3> computeAverageVelocity(Box3D domain);
    T computeAverageDensity(Box3D domain);
    T computeAverageDensity();
    T computeAverageEnergy(Box3D domain);
    T computeAverageEnergy();
    T computeRMSvorticity(Box3D domain);
    T computeRMSvorticity();
    T computeAverageShearStressNorm(Box3D domain);
    T computeAverageShearStressNorm();
    T computeRMSshearStressNorm(Box3D domain);
    T computeRMSshearStressNorm();
private:
    VoxelizedDomain3D<T>& voxelizedDomain;
    MultiBlockLattice3D<T,Descriptor>& lattice;
    MultiBlock3D& boundaryShapeArg;
    OffLatticeNearWallModel3D<T,BoundaryType>* offLatticeModel;
    MultiContainerBlock3D offLatticePattern;
};

template<typename T, class SurfaceData>
class OffLatticeImplicitWallPatternFunctional3D : public BoxProcessingFunctional3D
{
public:
    /// numNeighbors is the number of neighbors a boundary node must be able
    ///   to access along a given direction.
    OffLatticeImplicitWallPatternFunctional3D (
            OffLatticeNearWallModel3D<T,SurfaceData>* offLatticeModel_ );
    virtual ~OffLatticeImplicitWallPatternFunctional3D();
    OffLatticeImplicitWallPatternFunctional3D(OffLatticeImplicitWallPatternFunctional3D const& rhs);
    OffLatticeImplicitWallPatternFunctional3D& operator= (
            OffLatticeImplicitWallPatternFunctional3D const& rhs );
    void swap(OffLatticeImplicitWallPatternFunctional3D& rhs);
    virtual OffLatticeImplicitWallPatternFunctional3D<T,SurfaceData>* clone() const;

    /// First AtomicBlock: OffLatticeInfo.
    ///   If there are more atomic-blocks then they are forwarded to the
    ///   shape function, to provide additional read-only parameters.
    /// It is very important that the "offLatticePattern" container block
    /// (the first atomic-block passed) has the same multi-block management
    /// as the lattice used in the simulation.
    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> fields);
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
    virtual BlockDomain::DomainT appliesTo() const;
private:
    OffLatticeNearWallModel3D<T,SurfaceData>* offLatticeModel;
};

template<typename T, template<typename U> class Descriptor>
class boundaryNodeStencilFunctional3D : public BoxProcessingFunctional3D
{
public:
    boundaryNodeStencilFunctional3D(int numShapeArgs_, int numCompletionArgs_);
    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> blocks);
    virtual boundaryNodeStencilFunctional3D<T,Descriptor>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
    virtual BlockDomain::DomainT appliesTo() const;
private:
    int numShapeArgs;
    int numCompletionArgs;
};

template<typename T, template<typename U> class Descriptor, class SurfaceData>
class OffLatticeWallDistanceYFunctional3D : public BoxProcessingFunctional3D
{
public:
    OffLatticeWallDistanceYFunctional3D (
            OffLatticeNearWallModel3D<T,SurfaceData>* offLatticeModel_,
            plint numShapeArgs_, plint numCompletionArgs_ );
    virtual ~OffLatticeWallDistanceYFunctional3D();
    OffLatticeWallDistanceYFunctional3D(OffLatticeWallDistanceYFunctional3D<T,Descriptor,SurfaceData> const& rhs);
    OffLatticeWallDistanceYFunctional3D& operator= (
            OffLatticeWallDistanceYFunctional3D<T,Descriptor,SurfaceData> const& rhs );
    void swap(OffLatticeWallDistanceYFunctional3D<T,Descriptor,SurfaceData>& rhs);

    /// First AtomicBlock: Lattice; Second AtomicBlock: Container for off-
    ///   lattice info.
    ///   If there are more atomic-blocks then they are forwarded to the
    ///   shape function, to provide additional read-only parameters.
    ///
    /// It is very important that the "offLatticePattern" container block
    /// which is passed as the second atomic-block with the off-lattice info
    /// has the same multi-block management as the lattice used in the simulation.
    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> fields);
    virtual OffLatticeWallDistanceYFunctional3D<T,Descriptor,SurfaceData>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
    virtual BlockDomain::DomainT appliesTo() const;
private:
    OffLatticeNearWallModel3D<T,SurfaceData>* offLatticeModel;
    plint numShapeArgs, numCompletionArgs;
};

template<typename T, template<typename U> class Descriptor>
class SetLESCsmagoFunctional3D : public BoxProcessingFunctional3D
{
public:
    SetLESCsmagoFunctional3D(int numShapeArgs_, int numCompletionArgs_, T cSmago_);
    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> blocks);
    virtual SetLESCsmagoFunctional3D<T,Descriptor>* clone() const;
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const;
private:
    T cSmago;
    int numShapeArgs;
    int numCompletionArgs;
};


template<typename T, template<typename U> class Descriptor>
class ModifyInitialStatFunction3D : public BoxProcessingFunctional3D
{
public:
    ModifyInitialStatFunction3D(T maxDistance_ = 4.0);
    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> blocks);
    virtual ModifyInitialStatFunction3D<T,Descriptor>* clone() const {
        return new ModifyInitialStatFunction3D<T,Descriptor>(*this);
    }
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::staticVariables;
    }
private:
    T maxDistance;
};

template<typename T, template<typename U> class Descriptor>
class GenDatFunction3D : public BoxProcessingFunctional3D
{
public:
    GenDatFunction3D(T maxDistance_, T dx_, T height_);
    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> blocks);
    virtual GenDatFunction3D<T,Descriptor>* clone() const {
        return new GenDatFunction3D<T,Descriptor>(*this);
    }
    virtual void getTypeOfModification(std::vector<modif::ModifT>& modified) const {
        modified[0] = modif::staticVariables;
    }
private:
    T maxDistance;
    T dx;
    T height;
};

/// copy pressure data to triangleMesh
template <typename T>
class CopyDataToTriangleMesh3D : public ScalarFieldBoxProcessingFunctional3D<T> {
public:
    CopyDataToTriangleMesh3D(RawConnectedTriangleMesh<T>* triangleMesh_, 
    plint propertyID_, plint vertexIDTagging_):
        triangleMesh(triangleMesh_), propertyID(propertyID_), 
        vertexIDTagging(vertexIDTagging_)  { }
    virtual void process(Box3D domain,  std::vector<ScalarField3D<T> *> scalarFields);

    virtual CopyDataToTriangleMesh3D<T> *clone() const
    {
        return new CopyDataToTriangleMesh3D<T>(*this);
    }

    virtual void getTypeOfModification(std::vector<modif::ModifT> &modified) const
    {
        modified[0] = modif::staticVariables;
    }

    virtual BlockDomain::DomainT appliesTo() const
    {
        return BlockDomain::bulkAndEnvelope;
    }
private:
    void collectSurfacePressure(std::vector<int> &tagData, std::vector<T> &pressureData);

private:
    RawConnectedTriangleMesh<T>* triangleMesh;
    plint propertyID;
    plint vertexIDTagging;

};

template <typename T, template <typename U> class Descriptor, typename BoundaryType>
class CalculateDataOnTriangleMesh3D : public BoxProcessingFunctional3D {
public:
    CalculateDataOnTriangleMesh3D(RawConnectedTriangleMesh<T>* triangleMesh_, 
    plint propertyID_, plint numCompletionArgs_, T fScale_, OffLatticeNearWallModel3D<T, BoundaryType> * offLatticeModel3D_):
        triangleMesh(triangleMesh_), propertyID(propertyID_), numCompletionArgs(numCompletionArgs_), fScale(fScale_),
        offLatticeModel3D(offLatticeModel3D_) { }
    virtual void processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> blocks);

    virtual CalculateDataOnTriangleMesh3D<T, Descriptor, BoundaryType> *clone() const
    {
        return new CalculateDataOnTriangleMesh3D<T, Descriptor, BoundaryType>(*this);
    }

    virtual void getTypeOfModification(std::vector<modif::ModifT> &modified) const
    {
        modified[0] = modif::staticVariables;
    }

    virtual BlockDomain::DomainT appliesTo() const
    {
        return BlockDomain::bulk;
    }
    
private:
    RawConnectedTriangleMesh<T>* triangleMesh;
    plint propertyID;
    plint numCompletionArgs;
    OffLatticeNearWallModel3D<T, BoundaryType> * offLatticeModel3D;
    T fScale;
};

}

#endif // NEAR_WALL_MODEL_3D_H