
#ifndef NEAR_WALL_MODEL_3D_HH
#define NEAR_WALL_MODEL_3D_HH

#include "nearWallModel3D.h"

#define NO_DISTANCE 10000
#define TOO_CLOSE 10001

#define WALL_FUNCTION 4
// #define MIN_DISTANCE 0.2
#define FULL_POP_CONSTRUCTION
//#define AHMED_ONLY
//#define FILTERED_RHO_U
//#define GEN_GRID_DAT

#define MAX_BUF_SIZE 100000

namespace plb {

/// data processor for computing dynamicID from lattice
template <typename T, template <typename U> class Descriptor>
void DynamicIDFunctional3D<T, Descriptor>::process(
    Box3D domain, BlockLattice3D<T, Descriptor> &lattice, ScalarField3D<int> &scalarField)
{
    Dot3D offset = computeRelativeDisplacement(lattice, scalarField);
    for (plint iX = domain.x0; iX <= domain.x1; ++iX)
    {
        for (plint iY = domain.y0; iY <= domain.y1; ++iY)
        {
            for (plint iZ = domain.z0; iZ <= domain.z1; ++iZ)
            {
                scalarField.get(iX + offset.x, iY + offset.y, iZ + offset.z) = lattice.get(iX, iY, iZ).getDynamics().getId();
            }
        }
    }
}

template <typename T, template <typename U> class Descriptor>
DynamicIDFunctional3D<T, Descriptor> *DynamicIDFunctional3D<T, Descriptor>::clone() const
{
    return new DynamicIDFunctional3D<T, Descriptor>(*this);
}

template <typename T, template <typename U> class Descriptor>
void DynamicIDFunctional3D<T, Descriptor>::getTypeOfModification(std::vector<modif::ModifT> &modified) const
{
    modified[0] = modif::nothing;
    modified[1] = modif::staticVariables;
}
/// wrapper processor for computing dynamicID from lattice
template <typename T, template <typename U> class Descriptor>
void computeDynamicID(MultiBlockLattice3D<T, Descriptor> &lattice, MultiScalarField3D<int> &dynamicID, Box3D domain)
{
    applyProcessingFunctional(
        new DynamicIDFunctional3D<T, Descriptor>, domain, lattice, dynamicID);
}
// data prepare for off-lattice boundary
template <typename T, template <typename U> class Descriptor>
PackedRhoVelocityNufunctional3D<T, Descriptor>::PackedRhoVelocityNufunctional3D()
{
    initialized = 0;
    alphaFilter = 0.98;
}

template <typename T, template <typename U> class Descriptor>
void PackedRhoVelocityNufunctional3D<T, Descriptor>::process(
    Box3D domain, BlockLattice3D<T, Descriptor> &lattice,
    NTensorField3D<T> &rhoVelocityNuField)
{
    Dot3D offset = computeRelativeDisplacement(lattice, rhoVelocityNuField);

    T rho;
    Array<T, 3> velocity;
    T omega;
    T nu;
    T nuavg = 0;
    int counter = 0;
    for (plint iX = domain.x0; iX <= domain.x1; ++iX)
    {
        for (plint iY = domain.y0; iY <= domain.y1; ++iY)
        {
            for (plint iZ = domain.z0; iZ <= domain.z1; ++iZ)
            {
                Cell<T, Descriptor> const &cell = lattice.get(iX, iY, iZ);
                rho = cell.getDynamics().computeDensity(cell);
                cell.getDynamics().computeVelocity(cell, velocity);
                // TODO: should turbulence viscosity included??
                // omega = cell.getDynamics().getDynamicParameter(dynamicParams::dynamicOmega, cell);
                omega = cell.getDynamics().getOmega();
                // for cumulant dynamics, it may be different
                if (!util::isZero(omega))
                    nu = (1.0 / omega - 0.5) * Descriptor<T>::cs2;
                else
                    nu = 0.0;
                nuavg += omega;
                counter++;
                T *rhoVelocityNu = rhoVelocityNuField.get(iX + offset.x, iY + offset.y, iZ + offset.z);
#ifdef FILTERED_RHO_U
                {
                    if (initialized < 100)
                    {
                        rho = (*rhoVelocityNu * initialized + rho) / (1.0 + initialized);
                        velocity[0] = (*(rhoVelocityNu + 1) * initialized + velocity[0]) / (1.0 + initialized);
                        velocity[1] = (*(rhoVelocityNu + 2) * initialized + velocity[1]) / (1.0 + initialized);
                        velocity[2] = (*(rhoVelocityNu + 3) * initialized + velocity[2]) / (1.0 + initialized);
                    }
                    else
                    {
                        rho = *rhoVelocityNu * alphaFilter + rho * (1.0 - alphaFilter);
                        velocity[0] = *(rhoVelocityNu + 1) * alphaFilter + velocity[0] * (1.0 - alphaFilter);
                        velocity[1] = *(rhoVelocityNu + 2) * alphaFilter + velocity[1] * (1.0 - alphaFilter);
                        velocity[2] = *(rhoVelocityNu + 3) * alphaFilter + velocity[2] * (1.0 - alphaFilter);
                    }
                }
#endif
                *rhoVelocityNu = rho;
                velocity.to_cArray(rhoVelocityNu + 1);
                rhoVelocityNu[4] = nu;
            }
        }
    }
    if (initialized <= 100)
        initialized++;
    // if(counter>0) std::cout<<"avg omega = " << nuavg/counter << std::endl;
}

template <typename T, template <typename U> class Descriptor>
PackedRhoVelocityNufunctional3D<T, Descriptor> *PackedRhoVelocityNufunctional3D<T, Descriptor>::clone() const
{
    return new PackedRhoVelocityNufunctional3D<T, Descriptor>(*this);
}

template <typename T, template <typename U> class Descriptor>
void PackedRhoVelocityNufunctional3D<T, Descriptor>::getTypeOfModification(std::vector<modif::ModifT> &modified) const
{
    modified[0] = modif::nothing;         // lattice
    modified[1] = modif::staticVariables; // rhoBarJ
}

template <typename T, class SurfaceData>
TriangleFlowNearWallShape3D<T, SurfaceData>::TriangleFlowNearWallShape3D(
    TriangleBoundary3D<T> const &boundary_,
    BoundaryProfiles3D<T, SurfaceData> const &profiles_)
    : boundary(boundary_),
      profiles(profiles_),
      voxelFlags(0), hashContainer(0), boundaryArg(0)
{
}

template <typename T, class SurfaceData>
bool TriangleFlowNearWallShape3D<T, SurfaceData>::isInside(
    Dot3D const &location) const
{
    PLB_PRECONDITION(voxelFlags);
    Dot3D localPos = location - voxelFlags->getLocation();
    return voxelFlag::insideFlag(voxelFlags->get(localPos.x, localPos.y, localPos.z));
}

template <typename T, class SurfaceData>
bool TriangleFlowNearWallShape3D<T, SurfaceData>::isOutside(
    Dot3D const &location) const
{
    PLB_PRECONDITION(voxelFlags);
    Dot3D localPos = location - voxelFlags->getLocation();
    return voxelFlag::outsideFlag(voxelFlags->get(localPos.x, localPos.y, localPos.z));
}

template <typename T, class SurfaceData>
bool TriangleFlowNearWallShape3D<T, SurfaceData>::pointOnSurface(
    Array<T, 3> const &fromPoint, Array<T, 3> const &direction,
    Array<T, 3> &locatedPoint, T &distance,
    Array<T, 3> &wallNormal, SurfaceData &surfaceData,
    OffBoundary::Type &bdType, plint &id) const
{
    PLB_PRECONDITION(hashContainer); // Make sure these arguments have
    PLB_PRECONDITION(boundaryArg);   //   been provided by the user through
                                     //   the clone function.
    static const T maxDistance = std::sqrt((T)3);
    Array<T, 2> xRange(fromPoint[0] - maxDistance, fromPoint[0] + maxDistance);
    Array<T, 2> yRange(fromPoint[1] - maxDistance, fromPoint[1] + maxDistance);
    Array<T, 2> zRange(fromPoint[2] - maxDistance, fromPoint[2] + maxDistance);
    TriangleHash<T> triangleHash(*hashContainer);
    std::vector<plint> possibleTriangles;
    if (id >= 0 && id < boundary.getMesh().getNumTriangles())
    {
        possibleTriangles.push_back(id);
    }
    else
    {
        triangleHash.getTriangles(xRange, yRange, zRange, possibleTriangles);
    }

    Array<T, 3> tmpLocatedPoint;
    T tmpDistance;
    Array<T, 3> tmpNormal;
    T shortestDistance = T();
    plint locatedTriangle = -1;

    for (pluint iPossible = 0; iPossible < possibleTriangles.size(); ++iPossible)
    {
        plint iTriangle = possibleTriangles[iPossible];
        if (boundary.intersectSegment(
                iTriangle, boundaryArg,
                fromPoint, direction,
                tmpLocatedPoint, tmpDistance, tmpNormal))
        {
            if (locatedTriangle == -1 || tmpDistance < shortestDistance)
            {
                shortestDistance = tmpDistance;
                locatedTriangle = iTriangle;
                locatedPoint = tmpLocatedPoint;
                distance = tmpDistance;
                wallNormal = tmpNormal;
                profiles.getProfile(boundary, iTriangle).getData(locatedPoint, iTriangle, boundaryArg, surfaceData, bdType);
            }
        }
    }
    if (locatedTriangle != -1)
    {
        id = locatedTriangle;
        return true;
    }
    else
    {
        return false;
    }
}

template <typename T, class SurfaceData>
Array<T, 3> TriangleFlowNearWallShape3D<T, SurfaceData>::computeContinuousNormal(
    Array<T, 3> const &p, plint id, bool isAreaWeighted) const
{
    return boundary.computeContinuousNormal(p, id, isAreaWeighted);
}

template <typename T, class SurfaceData>
bool TriangleFlowNearWallShape3D<T, SurfaceData>::intersectsSurface(
    Array<T, 3> const &p1, Array<T, 3> const &p2, plint &id) const
{
    PLB_PRECONDITION(hashContainer); // Make sure these arguments have
    PLB_PRECONDITION(boundaryArg);   //   been provided by the user through
                                     //   the clone function.
    static const T maxDistance = std::sqrt((T)3);
    Array<T, 2> xRange(p1[0] - maxDistance, p1[0] + maxDistance);
    Array<T, 2> yRange(p1[1] - maxDistance, p1[1] + maxDistance);
    Array<T, 2> zRange(p1[2] - maxDistance, p1[2] + maxDistance);
    TriangleHash<T> triangleHash(*hashContainer);
    std::vector<plint> possibleTriangles;
    if (id >= 0 && id < boundary.getMesh().getNumTriangles())
    {
        possibleTriangles.push_back(id);
    }
    else
    {
        triangleHash.getTriangles(xRange, yRange, zRange, possibleTriangles);
    }

    std::vector<plint> selection;
    for (pluint iPossible = 0; iPossible < possibleTriangles.size(); ++iPossible)
    {
        plint iTriangle = possibleTriangles[iPossible];
        int flag = 0;
        Array<T, 3> intersection, normal;
        T distance;
        if (boundary.getMesh().pointOnTriangle(p1, p2, flag, iTriangle, intersection, normal, distance))
        {
            selection.push_back(iTriangle);
        }
    }
    if (selection.empty())
    {
        return false;
    }
    else if (selection.size() == 1)
    {
        id = selection[0];
        return true;
    }
    else
    {
        Array<T, 3> locatedPoint;
        T distance;
        Array<T, 3> wallNormal;
        SurfaceData surfaceData;
        OffBoundary::Type bdType;
        return pointOnSurface(p1, p2 - p1, locatedPoint, distance, wallNormal, surfaceData, bdType, id);
    }
}

template <typename T, class SurfaceData>
plint TriangleFlowNearWallShape3D<T, SurfaceData>::getTag(plint id) const
{
    return boundary.getTag(id);
}

template <typename T, class SurfaceData>
bool TriangleFlowNearWallShape3D<T, SurfaceData>::distanceToSurface(
    Array<T, 3> const &point, T &distance, bool &isBehind) const
{
    PLB_PRECONDITION(hashContainer); // Make sure these arguments have
    PLB_PRECONDITION(boundaryArg);   //   been provided by the user through
                                     //   the clone function.
    T maxDistance = 4.01;
    Array<T, 2> xRange(point[0] - maxDistance, point[0] + maxDistance);
    Array<T, 2> yRange(point[1] - maxDistance, point[1] + maxDistance);
    Array<T, 2> zRange(point[2] - maxDistance, point[2] + maxDistance);
    TriangleHash<T> triangleHash(*hashContainer);
    std::vector<plint> possibleTriangles;
    triangleHash.getTriangles(xRange, yRange, zRange, possibleTriangles);

    T tmpDistance;
    bool tmpIsBehind;
    bool triangleFound = false;

    for (pluint iPossible = 0; iPossible < possibleTriangles.size(); ++iPossible)
    {
        plint iTriangle = possibleTriangles[iPossible];
        boundary.getMesh().distanceToTriangle(
            point, iTriangle, tmpDistance, tmpIsBehind);
        if (!triangleFound || tmpDistance < distance)
        {
            distance = tmpDistance;
            isBehind = tmpIsBehind;
            triangleFound = true;
        }
    }
    return triangleFound;
}

template <typename T, class SurfaceData>
bool TriangleFlowNearWallShape3D<T, SurfaceData>::distanceToNearWallSurface(
    Array<T, 3> const &point, T &distance, Array<T, 3> &normal, bool &isBehind) const
{
    PLB_PRECONDITION(hashContainer); // Make sure these arguments have
    PLB_PRECONDITION(boundaryArg);   //   been provided by the user through
                                     //   the clone function.
    // T maxDistance = std::sqrt((T)3);
    T maxDistance = 4;
    Array<T, 2> xRange(point[0] - maxDistance, point[0] + maxDistance);
    Array<T, 2> yRange(point[1] - maxDistance, point[1] + maxDistance);
    Array<T, 2> zRange(point[2] - maxDistance, point[2] + maxDistance);
    TriangleHash<T> triangleHash(*hashContainer);
    std::vector<plint> possibleTriangles;
    triangleHash.getTriangles(xRange, yRange, zRange, possibleTriangles);

    T tmpDistance;
    bool tmpIsBehind;
    bool triangleFound = false;

    for (pluint iPossible = 0; iPossible < possibleTriangles.size(); ++iPossible)
    {
        plint iTriangle = possibleTriangles[iPossible];
        boundary.getMesh().distanceToTriangle(
            point, iTriangle, tmpDistance, tmpIsBehind);
        if (!triangleFound || tmpDistance < distance)
        {
            distance = tmpDistance;
            isBehind = tmpIsBehind;
            triangleFound = true;
            normal = boundary.getMesh().computeTriangleNormal(iTriangle);
        }
    }
    return triangleFound;
}

template <typename T, class SurfaceData>
TriangleFlowNearWallShape3D<T, SurfaceData> *
TriangleFlowNearWallShape3D<T, SurfaceData>::clone() const
{
    return new TriangleFlowNearWallShape3D<T, SurfaceData>(*this);
}

template <typename T, class SurfaceData>
TriangleFlowNearWallShape3D<T, SurfaceData> *
TriangleFlowNearWallShape3D<T, SurfaceData>::clone(
    std::vector<AtomicBlock3D *> args) const
{
    PLB_PRECONDITION(args.size() == 3);
    TriangleFlowNearWallShape3D<T, SurfaceData> *
        newShape = new TriangleFlowNearWallShape3D<T, SurfaceData>(*this);
    newShape->voxelFlags = dynamic_cast<ScalarField3D<int> *>(args[0]);
    newShape->hashContainer = dynamic_cast<AtomicContainerBlock3D *>(args[1]);
    newShape->boundaryArg = args[2];
    PLB_ASSERT(newShape->voxelFlags);
    PLB_ASSERT(newShape->hashContainer);
    PLB_ASSERT(newShape->boundaryArg);
    return newShape;
}
// #include "../../src/offLatticeNearWallModel3D.hh"
template <typename T, class SurfaceData>
OffLatticeNearWallModel3D<T, SurfaceData>::OffLatticeNearWallModel3D(
    BoundaryNearWallShape3D<T, SurfaceData> *shape_, int flowType_)
    : shape(shape_),
      flowType(flowType_),
      velIsJflag(false),
      partialReplaceFlag(false),
      secondOrderFlag(true),
      regularizedModel(true),
      computeStat(true),
      defineVelocity(true)
{
    PLB_ASSERT(flowType == voxelFlag::inside || flowType == voxelFlag::outside);
}

template <typename T, class SurfaceData>
OffLatticeNearWallModel3D<T, SurfaceData>::OffLatticeNearWallModel3D(
    OffLatticeNearWallModel3D<T, SurfaceData> const &rhs)
    : shape(rhs.shape->clone()),
      flowType(rhs.flowType),
      velIsJflag(rhs.velIsJflag),
      partialReplaceFlag(rhs.partialReplaceFlag),
      secondOrderFlag(rhs.secondOrderFlag),
      regularizedModel(rhs.regularizedModel),
      computeStat(rhs.computeStat),
      defineVelocity(rhs.defineVelocity)
{
}

template <typename T, class SurfaceData>
OffLatticeNearWallModel3D<T, SurfaceData> &OffLatticeNearWallModel3D<T, SurfaceData>::operator=(
    OffLatticeNearWallModel3D<T, SurfaceData> const &rhs)
{
    delete shape;
    shape = rhs.shape->clone();
    flowType = rhs.flowType;
    velIsJflag = rhs.velIsJflag;
    partialReplaceFlag = rhs.partialReplaceFlag;
    secondOrderFlag = rhs.secondOrderFlag;
    regularizedModel = rhs.regularizedModel;
    computeStat = rhs.computeStat;
    defineVelocity = rhs.defineVelocity;
    return *this;
}

template <typename T, class SurfaceData>
OffLatticeNearWallModel3D<T, SurfaceData>::~OffLatticeNearWallModel3D()
{
    delete shape;
}

template <typename T, class SurfaceData>
void OffLatticeNearWallModel3D<T, SurfaceData>::provideShapeArguments(
    std::vector<AtomicBlock3D *> args)
{
    BoundaryNearWallShape3D<T, SurfaceData> *newShape = shape->clone(args);
    std::swap(shape, newShape);
    delete newShape;
}

template <typename T, class SurfaceData>
plint OffLatticeNearWallModel3D<T, SurfaceData>::getTag(plint id) const
{
    return shape->getTag(id);
}

template <typename T, class SurfaceData>
bool OffLatticeNearWallModel3D<T, SurfaceData>::pointOnSurface(
    Dot3D const &fromPoint, Dot3D const &direction,
    Array<T, 3> &locatedPoint, T &distance,
    Array<T, 3> &wallNormal, SurfaceData &surfaceData,
    OffBoundary::Type &bdType, plint &id) const
{
    return shape->gridPointOnSurface(
        fromPoint, direction, locatedPoint, distance, wallNormal, surfaceData, bdType, id);
}

template <typename T, class SurfaceData>
bool OffLatticeNearWallModel3D<T, SurfaceData>::distanceToNearWallSurface(
    Dot3D const &fromPoint, T &distance,
    Array<T, 3> &wallNormal, bool isBehind) const
{
    return shape->gridDistanceToNearWallSurface(
        fromPoint, distance, wallNormal, isBehind);
}
template <typename T, class SurfaceData>
Array<T, 3> OffLatticeNearWallModel3D<T, SurfaceData>::computeContinuousNormal(
    Array<T, 3> const &p, plint id, bool isAreaWeighted) const
{
    return shape->computeContinuousNormal(p, id, isAreaWeighted);
}

template <typename T, class SurfaceData>
bool OffLatticeNearWallModel3D<T, SurfaceData>::intersectsSurface(
    Dot3D const &p1, Dot3D const &p2, plint &id) const
{
    return shape->intersectsSurface(p1, p2, id);
}

template <typename T, class SurfaceData>
bool OffLatticeNearWallModel3D<T, SurfaceData>::isFluid(Dot3D const &location) const
{
    if (flowType == voxelFlag::inside)
    {
        return shape->isInside(location);
    }
    else
    {
        return shape->isOutside(location);
    }
}

template <typename T, class SurfaceData>
bool OffLatticeNearWallModel3D<T, SurfaceData>::isSolid(Dot3D const &location) const
{
    if (flowType == voxelFlag::inside)
    {
        return shape->isOutside(location);
    }
    else
    {
        return shape->isInside(location);
    }
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
OffLatticeNearWallCompletionFunctional3D<T, Descriptor, SurfaceData>::OffLatticeNearWallCompletionFunctional3D(
    OffLatticeNearWallModel3D<T, SurfaceData> *offLatticeModel_,
    plint numShapeArgs_, plint numCompletionArgs_)
    : offLatticeModel(offLatticeModel_),
      numShapeArgs(numShapeArgs_),
      numCompletionArgs(numCompletionArgs_)
{
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
OffLatticeNearWallCompletionFunctional3D<T, Descriptor, SurfaceData>::~OffLatticeNearWallCompletionFunctional3D()
{
    delete offLatticeModel;
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
OffLatticeNearWallCompletionFunctional3D<T, Descriptor, SurfaceData>::
    OffLatticeNearWallCompletionFunctional3D(
        OffLatticeNearWallCompletionFunctional3D<T, Descriptor, SurfaceData> const &rhs)
    : offLatticeModel(rhs.offLatticeModel->clone()),
      numShapeArgs(rhs.numShapeArgs),
      numCompletionArgs(rhs.numCompletionArgs)
{
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
OffLatticeNearWallCompletionFunctional3D<T, Descriptor, SurfaceData> &
OffLatticeNearWallCompletionFunctional3D<T, Descriptor, SurfaceData>::operator=(
    OffLatticeNearWallCompletionFunctional3D<T, Descriptor, SurfaceData> const &rhs)
{
    OffLatticeNearWallCompletionFunctional3D<T, Descriptor, SurfaceData>(rhs).swap(*this);
    return *this;
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
void OffLatticeNearWallCompletionFunctional3D<T, Descriptor, SurfaceData>::swap(
    OffLatticeNearWallCompletionFunctional3D<T, Descriptor, SurfaceData> &rhs)
{
    std::swap(offLatticeModel, rhs.offLatticeModel);
    std::swap(numShapeArgs, rhs.numShapeArgs);
    std::swap(numCompletionArgs, rhs.numCompletionArgs);
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
OffLatticeNearWallCompletionFunctional3D<T, Descriptor, SurfaceData> *
OffLatticeNearWallCompletionFunctional3D<T, Descriptor, SurfaceData>::clone() const
{
    return new OffLatticeNearWallCompletionFunctional3D<T, Descriptor, SurfaceData>(*this);
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
void OffLatticeNearWallCompletionFunctional3D<T, Descriptor, SurfaceData>::getTypeOfModification(
    std::vector<modif::ModifT> &modified) const
{
    PLB_ASSERT((plint)modified.size() == 2 + numShapeArgs + numCompletionArgs);
    modified[0] = modif::staticVariables; // Lattice.
    // It is very important that the "offLatticePattern" container block
    // which is passed as the second atomic-block with the off-lattice info
    // has the same multi-block management as the lattice used in the simulation.
    modified[1] = modif::nothing; // Container for wet/dry nodes.
    // Possible additional parameters for the shape function and
    //   for the completion algorithm are read-only.
    for (pluint i = 2; i < modified.size(); ++i)
    {
        modified[i] = modif::nothing;
    }
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
BlockDomain::DomainT OffLatticeNearWallCompletionFunctional3D<T, Descriptor, SurfaceData>::appliesTo() const
{
    return BlockDomain::bulk;
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
void OffLatticeNearWallCompletionFunctional3D<T, Descriptor, SurfaceData>::processGenericBlocks(
    Box3D domain, std::vector<AtomicBlock3D *> fields)
{
    PLB_PRECONDITION((plint)fields.size() == 2 + numShapeArgs + numCompletionArgs);
    AtomicBlock3D *lattice = fields[0];
    PLB_ASSERT(lattice);

    // It is very important that the "offLatticePattern" container block
    // which is passed as the second atomic-block with the off-lattice info
    // has the same multi-block management as the lattice used in the simulation.
    AtomicContainerBlock3D *container = // Container for wet/dry nodes.
        dynamic_cast<AtomicContainerBlock3D *>(fields[1]);
    PLB_ASSERT(container);

    if (numShapeArgs > 0)
    {
        std::vector<AtomicBlock3D *> shapeParameters(numShapeArgs);
        for (plint i = 0; i < numShapeArgs; ++i)
        {
            shapeParameters[i] = fields[i + 2];
        }
        offLatticeModel->provideShapeArguments(shapeParameters);
    }
    std::vector<AtomicBlock3D *> completionParameters(numCompletionArgs);
    for (plint i = 0; i < numCompletionArgs; ++i)
    {
        completionParameters[i] = fields[i + 2 + numShapeArgs];
    }

    offLatticeModel->boundaryCompletion(*lattice, *container, completionParameters);
}

template <typename T, class SurfaceData>
OffLatticeNearWallPatternFunctional3D<T, SurfaceData>::
    OffLatticeNearWallPatternFunctional3D(
        OffLatticeNearWallModel3D<T, SurfaceData> *offLatticeModel_)
    : offLatticeModel(offLatticeModel_)
{
}

template <typename T, class SurfaceData>
OffLatticeNearWallPatternFunctional3D<T, SurfaceData>::~OffLatticeNearWallPatternFunctional3D()
{
    delete offLatticeModel;
}

template <typename T, class SurfaceData>
OffLatticeNearWallPatternFunctional3D<T, SurfaceData>::
    OffLatticeNearWallPatternFunctional3D(
        OffLatticeNearWallPatternFunctional3D<T, SurfaceData> const &rhs)
    : offLatticeModel(rhs.offLatticeModel->clone())
{
}

template <typename T, class SurfaceData>
OffLatticeNearWallPatternFunctional3D<T, SurfaceData> &
OffLatticeNearWallPatternFunctional3D<T, SurfaceData>::operator=(
    OffLatticeNearWallPatternFunctional3D<T, SurfaceData> const &rhs)
{
    OffLatticeNearWallPatternFunctional3D<T, SurfaceData>(rhs).swap(*this);
    return *this;
}

template <typename T, class SurfaceData>
void OffLatticeNearWallPatternFunctional3D<T, SurfaceData>::swap(
    OffLatticeNearWallPatternFunctional3D<T, SurfaceData> &rhs)
{
    std::swap(offLatticeModel, rhs.offLatticeModel);
}

template <typename T, class SurfaceData>
OffLatticeNearWallPatternFunctional3D<T, SurfaceData> *
OffLatticeNearWallPatternFunctional3D<T, SurfaceData>::clone() const
{
    return new OffLatticeNearWallPatternFunctional3D<T, SurfaceData>(*this);
}

template <typename T, class SurfaceData>
void OffLatticeNearWallPatternFunctional3D<T, SurfaceData>::getTypeOfModification(
    std::vector<modif::ModifT> &modified) const
{
    // It is very important that the "offLatticePattern" container block
    // (the first atomic-block passed) has the same multi-block management
    // as the lattice used in the simulation.
    modified[0] = modif::staticVariables; // Container.
    // Possible additional parameters for the shape function are read-only.
    for (pluint i = 1; i < modified.size(); ++i)
    {
        modified[i] = modif::nothing;
    }
}

template <typename T, class SurfaceData>
BlockDomain::DomainT OffLatticeNearWallPatternFunctional3D<T, SurfaceData>::appliesTo() const
{
    return BlockDomain::bulk;
}

template <typename T, class SurfaceData>
void OffLatticeNearWallPatternFunctional3D<T, SurfaceData>::processGenericBlocks(
    Box3D domain, std::vector<AtomicBlock3D *> fields)
{
    PLB_PRECONDITION(fields.size() >= 1);
    AtomicContainerBlock3D *container =
        dynamic_cast<AtomicContainerBlock3D *>(fields[0]);
    PLB_ASSERT(container);
    ContainerBlockData *storeInfo =
        offLatticeModel->generateOffLatticeInfo();
    container->setData(storeInfo);

    if (fields.size() > 1)
    {
        std::vector<AtomicBlock3D *> shapeParameters(fields.size() - 1);
        for (pluint i = 0; i < shapeParameters.size(); ++i)
        {
            shapeParameters[i] = fields[i + 1];
        }
        offLatticeModel->provideShapeArguments(shapeParameters);
    }

    for (plint iX = domain.x0; iX <= domain.x1; ++iX)
    {
        for (plint iY = domain.y0; iY <= domain.y1; ++iY)
        {
            for (plint iZ = domain.z0; iZ <= domain.z1; ++iZ)
            {
                offLatticeModel->prepareCell(Dot3D(iX, iY, iZ), *container);
            }
        }
    }
}

template <typename T, class SurfaceData>
GetForceOnMeshNearWallFunctional3D<T, SurfaceData>::GetForceOnMeshNearWallFunctional3D(
    OffLatticeNearWallModel3D<T, SurfaceData> *offLatticeModel_)
    : offLatticeModel(offLatticeModel_)
{
    for (int i = 0; i < 9; i++)
        forceId[i] = this->getStatistics().subscribeSum();
}

template <typename T, class SurfaceData>
GetForceOnMeshNearWallFunctional3D<T, SurfaceData>::~GetForceOnMeshNearWallFunctional3D()
{
    delete offLatticeModel;
}

template <typename T, class SurfaceData>
GetForceOnMeshNearWallFunctional3D<T, SurfaceData>::GetForceOnMeshNearWallFunctional3D(
    GetForceOnMeshNearWallFunctional3D<T, SurfaceData> const &rhs)
    : PlainReductiveBoxProcessingFunctional3D(rhs),
      offLatticeModel(rhs.offLatticeModel->clone()),
      forceId(rhs.forceId)
{
}

template <typename T, class SurfaceData>
GetForceOnMeshNearWallFunctional3D<T, SurfaceData> &
GetForceOnMeshNearWallFunctional3D<T, SurfaceData>::operator=(
    GetForceOnMeshNearWallFunctional3D<T, SurfaceData> const &rhs)
{
    delete offLatticeModel;
    offLatticeModel = rhs.offLatticeModel->clone();
    forceId = rhs.forceId;
    PlainReductiveBoxProcessingFunctional3D::operator=(rhs);
    return *this;
}

template <typename T, class SurfaceData>
void GetForceOnMeshNearWallFunctional3D<T, SurfaceData>::processGenericBlocks(
    Box3D domain, std::vector<AtomicBlock3D *> fields)
{
    PLB_PRECONDITION(fields.size() == 1);
    AtomicContainerBlock3D *offLatticeInfo =
        dynamic_cast<AtomicContainerBlock3D *>(fields[0]);
    PLB_ASSERT(offLatticeInfo);

    Array<T, 9> force = offLatticeModel->getLocalForce(*offLatticeInfo);
    for (int i = 0; i < 9; i++)
    {
        this->getStatistics().gatherSum(forceId[i], force[i]);
    }
}

template <typename T, class SurfaceData>
GetForceOnMeshNearWallFunctional3D<T, SurfaceData> *
GetForceOnMeshNearWallFunctional3D<T, SurfaceData>::clone() const
{
    return new GetForceOnMeshNearWallFunctional3D<T, SurfaceData>(*this);
}

template <typename T, class SurfaceData>
void GetForceOnMeshNearWallFunctional3D<T, SurfaceData>::getTypeOfModification(std::vector<modif::ModifT> &modified) const
{
    modified[0] = modif::nothing; // Off-lattice info.
}

template <typename T, class SurfaceData>
BlockDomain::DomainT GetForceOnMeshNearWallFunctional3D<T, SurfaceData>::appliesTo() const
{
    return BlockDomain::bulk;
}

template <typename T, class SurfaceData>
Array<T, 9> GetForceOnMeshNearWallFunctional3D<T, SurfaceData>::getForce() const
{

    Array<T, 9> AForce;
    for (int i = 0; i < 9; i++)
        AForce[i] = this->getStatistics().getSum(forceId[i]);
    return AForce;
}

template <typename T, class BoundaryType>
Array<T, 9> getForceOnMesh(
    MultiBlock3D &offLatticePattern,
    OffLatticeNearWallModel3D<T, BoundaryType> const &offLatticeModel)
{
    std::vector<MultiBlock3D *> arg;
    arg.push_back(&offLatticePattern);
    GetForceOnMeshNearWallFunctional3D<T, BoundaryType> functional(offLatticeModel.clone());
    applyProcessingFunctional(
        functional, offLatticePattern.getBoundingBox(), arg);
    return functional.getForce();
}

template <typename T, class SurfaceData>
GetForceOnObjectNearWallFunctional3D<T, SurfaceData>::GetForceOnObjectNearWallFunctional3D(
    OffLatticeNearWallModel3D<T, SurfaceData> *offLatticeModel_)
    : offLatticeModel(offLatticeModel_)
{
    for (int i = 0; i < 9; i++)
        forceId[i] = this->getStatistics().subscribeSum();
}

template <typename T, class SurfaceData>
GetForceOnObjectNearWallFunctional3D<T, SurfaceData>::~GetForceOnObjectNearWallFunctional3D()
{
    delete offLatticeModel;
}

template <typename T, class SurfaceData>
GetForceOnObjectNearWallFunctional3D<T, SurfaceData>::GetForceOnObjectNearWallFunctional3D(
    GetForceOnObjectNearWallFunctional3D<T, SurfaceData> const &rhs)
    : PlainReductiveBoxProcessingFunctional3D(rhs),
      offLatticeModel(rhs.offLatticeModel->clone()),
      forceId(rhs.forceId)
{
}

template <typename T, class SurfaceData>
GetForceOnObjectNearWallFunctional3D<T, SurfaceData> &
GetForceOnObjectNearWallFunctional3D<T, SurfaceData>::operator=(
    GetForceOnObjectNearWallFunctional3D<T, SurfaceData> const &rhs)
{
    delete offLatticeModel;
    offLatticeModel = rhs.offLatticeModel->clone();
    forceId = rhs.forceId;
    PlainReductiveBoxProcessingFunctional3D::operator=(rhs);
    return *this;
}

template <typename T, class SurfaceData>
void GetForceOnObjectNearWallFunctional3D<T, SurfaceData>::processGenericBlocks(
    Box3D domain, std::vector<AtomicBlock3D *> fields)
{
    PLB_PRECONDITION(fields.size() == 1);
    AtomicContainerBlock3D *offLatticeInfo =
        dynamic_cast<AtomicContainerBlock3D *>(fields[0]);
    PLB_ASSERT(offLatticeInfo);

    Array<T, 9> force = offLatticeModel->getLocalForce(*offLatticeInfo);
    for (int i = 0; i < 9; i++)
    {
        this->getStatistics().gatherSum(forceId[i], force[i]);
    }
}

template <typename T, class SurfaceData>
GetForceOnObjectNearWallFunctional3D<T, SurfaceData> *
GetForceOnObjectNearWallFunctional3D<T, SurfaceData>::clone() const
{
    return new GetForceOnObjectNearWallFunctional3D<T, SurfaceData>(*this);
}

template <typename T, class SurfaceData>
void GetForceOnObjectNearWallFunctional3D<T, SurfaceData>::getTypeOfModification(std::vector<modif::ModifT> &modified) const
{
    modified[0] = modif::nothing; // Off-lattice info.
}

template <typename T, class SurfaceData>
BlockDomain::DomainT GetForceOnObjectNearWallFunctional3D<T, SurfaceData>::appliesTo() const
{
    return BlockDomain::bulk;
}

template <typename T, class SurfaceData>
Array<T, 9> GetForceOnObjectNearWallFunctional3D<T, SurfaceData>::getForce() const
{

    Array<T, 9> AForce;
    for (int i = 0; i < 9; i++)
        AForce[i] = this->getStatistics().getSum(forceId[i]);
    return AForce;
}


template <typename T, class BoundaryType>
Array<T, 9> getForceOnObject(
    MultiBlock3D &offLatticePattern,
    OffLatticeNearWallModel3D<T, BoundaryType> const &offLatticeModel)
{
    std::vector<MultiBlock3D *> arg;
    arg.push_back(&offLatticePattern);
    GetForceOnObjectNearWallFunctional3D<T, BoundaryType> functional(offLatticeModel.clone());
    applyProcessingFunctional(
        functional, offLatticePattern.getBoundingBox(), arg);
    return functional.getForce();
}

/// compute wallFunctionY and normal on stencil cells, wallFunctionY is the shortest distance between stencil cells and wall surface. normal is the normal of a triangle corresponding to the shortest distance.
template <typename T, template <typename U> class Descriptor, class SurfaceData>
OffLatticeWallDistanceYNormalFunctional3D<T, Descriptor, SurfaceData>::OffLatticeWallDistanceYNormalFunctional3D(
    OffLatticeNearWallModel3D<T, SurfaceData> *offLatticeModel_,
    plint numShapeArgs_, plint numCompletionArgs_)
    : offLatticeModel(offLatticeModel_),
      numShapeArgs(numShapeArgs_),
      numCompletionArgs(numCompletionArgs_)
{
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
OffLatticeWallDistanceYNormalFunctional3D<T, Descriptor, SurfaceData>::~OffLatticeWallDistanceYNormalFunctional3D()
{
    delete offLatticeModel;
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
OffLatticeWallDistanceYNormalFunctional3D<T, Descriptor, SurfaceData>::
    OffLatticeWallDistanceYNormalFunctional3D(
        OffLatticeWallDistanceYNormalFunctional3D<T, Descriptor, SurfaceData> const &rhs)
    : offLatticeModel(rhs.offLatticeModel->clone()),
      numShapeArgs(rhs.numShapeArgs),
      numCompletionArgs(rhs.numCompletionArgs)
{
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
OffLatticeWallDistanceYNormalFunctional3D<T, Descriptor, SurfaceData> &
OffLatticeWallDistanceYNormalFunctional3D<T, Descriptor, SurfaceData>::operator=(
    OffLatticeWallDistanceYNormalFunctional3D<T, Descriptor, SurfaceData> const &rhs)
{
    OffLatticeWallDistanceYNormalFunctional3D<T, Descriptor, SurfaceData>(rhs).swap(*this);
    return *this;
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
void OffLatticeWallDistanceYNormalFunctional3D<T, Descriptor, SurfaceData>::swap(
    OffLatticeWallDistanceYNormalFunctional3D<T, Descriptor, SurfaceData> &rhs)
{
    std::swap(offLatticeModel, rhs.offLatticeModel);
    std::swap(numShapeArgs, rhs.numShapeArgs);
    std::swap(numCompletionArgs, rhs.numCompletionArgs);
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
OffLatticeWallDistanceYNormalFunctional3D<T, Descriptor, SurfaceData> *
OffLatticeWallDistanceYNormalFunctional3D<T, Descriptor, SurfaceData>::clone() const
{
    return new OffLatticeWallDistanceYNormalFunctional3D<T, Descriptor, SurfaceData>(*this);
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
void OffLatticeWallDistanceYNormalFunctional3D<T, Descriptor, SurfaceData>::getTypeOfModification(
    std::vector<modif::ModifT> &modified) const
{
    PLB_ASSERT((plint)modified.size() == 2 + numShapeArgs + numCompletionArgs);
    modified[0] = modif::nothing; // Lattice.
    // It is very important that the "offLatticePattern" container block
    // which is passed as the second atomic-block with the off-lattice info
    // has the same multi-block management as the lattice used in the simulation.
    modified[1] = modif::nothing; // Container for wet/dry nodes.
    // Possible additional parameters for the shape function and
    //   for the completion algorithm are read-only.
    for (pluint i = 2; i < modified.size(); ++i)
    {
        modified[i] = modif::nothing;
    }
    // wallDistanceY is updated
    modified[2 + numShapeArgs + 1] = modif::staticVariables;
    modified[2 + numShapeArgs + 3] = modif::staticVariables;
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
BlockDomain::DomainT OffLatticeWallDistanceYNormalFunctional3D<T, Descriptor, SurfaceData>::appliesTo() const
{
    return BlockDomain::bulk;
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
void OffLatticeWallDistanceYNormalFunctional3D<T, Descriptor, SurfaceData>::processGenericBlocks(
    Box3D domain, std::vector<AtomicBlock3D *> fields)
{
    PLB_PRECONDITION((plint)fields.size() == 2 + numShapeArgs + numCompletionArgs);
    AtomicBlock3D *lattice = fields[0];
    PLB_ASSERT(lattice);

    // It is very important that the "offLatticePattern" container block
    // which is passed as the second atomic-block with the off-lattice info
    // has the same multi-block management as the lattice used in the simulation.
    AtomicContainerBlock3D *container = // Container for wet/dry nodes.
        dynamic_cast<AtomicContainerBlock3D *>(fields[1]);
    PLB_ASSERT(container);

    if (numShapeArgs > 0)
    {
        std::vector<AtomicBlock3D *> shapeParameters(numShapeArgs);
        for (plint i = 0; i < numShapeArgs; ++i)
        {
            shapeParameters[i] = fields[i + 2];
        }
        // It is necessary, otherwise, offLatticeModel does not own HashContainer, etc.
        offLatticeModel->provideShapeArguments(shapeParameters);
    }
    // 0:rhoVelocityNufield,1:wallDistanceY,2:stencilFlag,3:dynamicID,4:utau
    std::vector<AtomicBlock3D *> completionParameters(numCompletionArgs);
    for (plint i = 0; i < numCompletionArgs; ++i)
    {
        completionParameters[i] = fields[i + 2 + numShapeArgs];
    }

    //    ScalarField3D<int> * dynamicIDField = dynamic_cast<ScalarField3D<int>*>( completionParameters[3] );
    //    Dot3D offsetDynamicID = computeRelativeDisplacement(lattice, *dynamicIDField);
    minut = 1000.0;
    maxut = -1.0;
    for (plint iX = domain.x0; iX <= domain.x1; ++iX)
    {
        for (plint iY = domain.y0; iY <= domain.y1; ++iY)
        {
            for (plint iZ = domain.z0; iZ <= domain.z1; ++iZ)
            {
                //		int &dynamicID = dynamicIDField->get(iX+offsetDynamicID.x, iY+offsetDynamicID.y, iZ+offsetDynamicID.z);
                offLatticeModel->distanceYNormalCell(Dot3D(iX, iY, iZ), minut, maxut, *lattice, completionParameters);
            }
        }
    }
    // if(maxut>0) std::cout << "minut = " << minut << "; maxut = " << maxut << std::endl;
}

template <typename T, template <typename U> class Descriptor>
FilippovaHaenelJohanNearWallOffLatticeModel3D<T, Descriptor>::FilippovaHaenelJohanNearWallOffLatticeModel3D(
    BoundaryNearWallShape3D<T, Array<T, 3>> *shape_, int flowType_)
    : OffLatticeNearWallModel3D<T, Array<T, 3>>(shape_, flowType_)
{
    typedef Descriptor<T> D;
    invAB.resize(D::q);
    invAB[0] = T();
    for (plint iPop = 1; iPop < D::q; ++iPop)
    {
        invAB[iPop] =
            (T)1 / std::sqrt(
                       util::sqr(D::c[iPop][0]) + util::sqr(D::c[iPop][1]) + util::sqr(D::c[iPop][2]));
    }
}

template <typename T, template <typename U> class Descriptor>
FilippovaHaenelJohanNearWallOffLatticeModel3D<T, Descriptor> *FilippovaHaenelJohanNearWallOffLatticeModel3D<T, Descriptor>::clone() const
{
    return new FilippovaHaenelJohanNearWallOffLatticeModel3D(*this);
}

template <typename T, template <typename U> class Descriptor>
plint FilippovaHaenelJohanNearWallOffLatticeModel3D<T, Descriptor>::getNumNeighbors() const
{
    return 1;
}

template <typename T, template <typename U> class Descriptor>
bool FilippovaHaenelJohanNearWallOffLatticeModel3D<T, Descriptor>::isExtrapolated() const
{
    return true;
}
/*
template<typename T, template<typename U> class Descriptor>
void FilippovaHaenelJohanNearWallOffLatticeModel3D<T,Descriptor>::prepareCell (
        Dot3D const& cellLocation,
        AtomicContainerBlock3D& container )
{
    typedef Descriptor<T> D;
    Dot3D offset = container.getLocation();
    OffLatticeInfo3D* info = dynamic_cast<OffLatticeInfo3D*>(container.getData());
    PLB_ASSERT( info );
    std::vector<int> liquidNeighbors;
    std::vector<int> solidNeighbors;
    std::vector<plint> ids;
    Dot3D absLoc = cellLocation+offset;
    bool isSolidOnBothDirections = false;
    if (this->isSolid(absLoc)) {
    if(absLoc.x<=1 || absLoc.y<=1 || absLoc.z <=1) return;
        for (int iPop=0; iPop<D::q; ++iPop) {
            Dot3D neighbor(cellLocation.x+D::c[iPop][0], cellLocation.y+D::c[iPop][1], cellLocation.z+D::c[iPop][2]);
            Dot3D neighbor2(cellLocation.x+2*D::c[iPop][0], cellLocation.y+2*D::c[iPop][1], cellLocation.z+2*D::c[iPop][2]);
            Dot3D neighborLoc = neighbor + offset;
            Dot3D neighborLoc2 = neighbor2 + offset;
            // If the non-fluid node has a fluid neighbor ...
            if (this->isFluid(neighborLoc)) {
        if (this->isSolid(neighborLoc2)) { isSolidOnBothDirections=true; break;}
                // ... check how many fluid nodes it has ahead of it ...
                plint iTriangle=-1;
                global::timer("intersect").start();
                Array<T,3> locatedPoint;
                T distance;
                Array<T,3> wallNormal;
                Array<T,3> surfaceData;
                OffBoundary::Type bdType;
#ifdef PLB_DEBUG
                bool ok =
#endif
                    this->pointOnSurface (
                            cellLocation+offset, Dot3D(D::c[iPop][0],D::c[iPop][1],D::c[iPop][2]), locatedPoint, distance,
                            wallNormal, surfaceData, bdType, iTriangle );

                // In the following, the importance of directions is sorted wrt. how well they
                //   are aligned with the wall normal. It is better to take the continuous normal,
                //   because it is not sensitive to the choice of the triangle when we shoot at
                //   an edge.
                //wallNormal = this->computeContinuousNormal(locatedPoint, iTriangle);
                global::timer("intersect").stop();
                PLB_ASSERT( ok );
                // ... then add this node to the list.
                liquidNeighbors.push_back(iPop);
                ids.push_back(iTriangle);
            }
        }
        if (!liquidNeighbors.empty() && !isSolidOnBothDirections) {
            info->getDryNodes().push_back(cellLocation);
            info->getDryNodeFluidDirections().push_back(liquidNeighbors);
            info->getDryNodeIds().push_back(ids);
        }
    }

    ids.clear();
    if (this->isFluid(absLoc)) {
        for (int iPop=0; iPop<D::q; ++iPop) {
            Dot3D neighbor(cellLocation.x+D::c[iPop][0], cellLocation.y+D::c[iPop][1], cellLocation.z+D::c[iPop][2]);
            Dot3D neighbor_N(cellLocation.x-D::c[iPop][0], cellLocation.y-D::c[iPop][1], cellLocation.z-D::c[iPop][2]);
            Dot3D neighborLoc = neighbor + offset;
            Dot3D neighborLoc_N = neighbor_N + offset;
            if (this->isSolid(neighborLoc)) {
                plint iTriangle=-1;
                Array<T,3> locatedPoint;
                T distance;
                Array<T,3> wallNormal;
                Array<T,3> surfaceData;
                OffBoundary::Type bdType;
#ifdef PLB_DEBUG
                ok =
#endif
                    this->pointOnSurface (
                            cellLocation+offset, Dot3D(D::c[iPop][0],D::c[iPop][1],D::c[iPop][2]), locatedPoint, distance,
                            wallNormal, surfaceData, bdType, iTriangle );

#ifdef PLB_DEBUG
                PLB_ASSERT( ok );
#endif
                solidNeighbors.push_back(iPop);
                ids.push_back(iTriangle);
            }
        }
        if (!solidNeighbors.empty()) {
            info->getBdNodes().push_back(cellLocation);
            info->getBdNodeSolidDirections().push_back(solidNeighbors);
            info->getBdNodeIds().push_back(ids);
        }
    }

}
*/
template <typename T, template <typename U> class Descriptor>
void FilippovaHaenelJohanNearWallOffLatticeModel3D<T, Descriptor>::prepareCell(
    Dot3D const &cellLocation,
    AtomicContainerBlock3D &container)
{
    /*
        typedef Descriptor<T> D;
        Dot3D offset = container.getLocation();
        OffLatticeInfo3D* info = dynamic_cast<OffLatticeInfo3D*>(container.getData());
        PLB_ASSERT( info );
        std::vector<int> liquidNeighbors;
        std::vector<int> solidNeighbors;
        std::vector<plint> ids;
        std::vector<bool> hasFluidNeighbor;
        Dot3D absLoc = cellLocation+offset;

    #ifdef MIN_DISTANCE
        if (this->isFluid(absLoc)) {
            T distance;
            Array<T, 3> wallNormal;
            bool isBehind;
            bool ok = this->distanceToNearWallSurface(absLoc, distance, wallNormal, isBehind);
            if(!ok || !std::isfinite(distance) || isBehind) {
                return;
            }
    //#ifdef MIN_DISTANCE
        if(distance<MIN_DISTANCE || distance > 3) return;
    //#endif
            for (int iPop=0; iPop<D::q; ++iPop) {
                Dot3D neighbor(cellLocation.x+D::c[iPop][0], cellLocation.y+D::c[iPop][1], cellLocation.z+D::c[iPop][2]);
                Dot3D neighbor2(cellLocation.x+2*D::c[iPop][0], cellLocation.y+2*D::c[iPop][1], cellLocation.z+2*D::c[iPop][2]);
                Dot3D neighborLoc = neighbor + offset;
                Dot3D neighborLoc2 = neighbor2 + offset;
                if (this->isSolid(neighborLoc)) {
                    solidNeighbors.push_back(iPop);
                }

    //#ifdef MIN_DISTANCE
             else if(this->isFluid(neighborLoc)) {
                    T distance2;
                    Array<T, 3> wallNormal2;
                    bool isBehind2;
                    bool ok2 = this->distanceToNearWallSurface(neighborLoc, distance2, wallNormal2, isBehind2);
                    if(!ok2 || !std::isfinite(distance2) || isBehind2) {
                        continue;
                    }
                    if(distance2 < MIN_DISTANCE) {
                        solidNeighbors.push_back(iPop);
                    }
                }
    //#endif
            }


            if (!solidNeighbors.empty()) {
                info->getBdNodes().push_back(cellLocation);
                info->getBdNodeSolidDirections().push_back(solidNeighbors);
                //info->getBdNodeIds().push_back(ids);
            }
        }
    #else
        if (this->isFluid(absLoc)) {
            for (int iPop=1; iPop<D::q; ++iPop) {
                Dot3D neighbor(cellLocation.x+D::c[iPop][0], cellLocation.y+D::c[iPop][1], cellLocation.z+D::c[iPop][2]);
                Dot3D prevNode(
                    cellLocation.x - D::c[iPop][0], cellLocation.y - D::c[iPop][1],
                    cellLocation.z - D::c[iPop][2]);

                Dot3D neighborLoc = neighbor + offset;
                if (this->isSolid(neighborLoc)) {
                    plint iTriangle=-1;
                    Array<T,3> locatedPoint;
                    T distance;
                    Array<T,3> wallNormal;
                    Array<T,3> surfaceData;
                    OffBoundary::Type bdType;
    #ifdef PLB_DEBUG
                    ok =
    #endif
                        this->pointOnSurface (
                                cellLocation+offset, Dot3D(D::c[iPop][0],D::c[iPop][1],D::c[iPop][2]), locatedPoint, distance,
                                wallNormal, surfaceData, bdType, iTriangle );

    #ifdef PLB_DEBUG
                    PLB_ASSERT( ok );
    #endif
                    solidNeighbors.push_back(iPop);
                    ids.push_back(iTriangle);
                    bool prevNodeIsPureFluid = this->isFluid(prevNode + offset);
                    if (prevNodeIsPureFluid) {
                        hasFluidNeighbor.push_back(true);
                    } else {
                        hasFluidNeighbor.push_back(false);
                    }

                }
            }
            if (!solidNeighbors.empty()) {
                info->getBdNodes().push_back(cellLocation);
                info->getBdNodeSolidDirections().push_back(solidNeighbors);
                info->getBdNodeIds().push_back(ids);
                info->getHasFluidNeighbor().push_back(hasFluidNeighbor);
            }
        }
    #endif
    */
    typedef Descriptor<T> D;
    Dot3D offset = container.getLocation();
    OffLatticeInfo3D *info = dynamic_cast<OffLatticeInfo3D *>(container.getData());
    PLB_ASSERT(info);
    std::vector<int> solidDirections;
    std::vector<plint> boundaryIds;
    std::vector<bool> hasFluidNeighbor;
    if (this->isFluid(cellLocation + offset))
    {
        for (plint iPop = 1; iPop < D::q; ++iPop)
        {
            Dot3D neighbor(
                cellLocation.x + D::c[iPop][0], cellLocation.y + D::c[iPop][1],
                cellLocation.z + D::c[iPop][2]);
            Dot3D prevNode(
                cellLocation.x - D::c[iPop][0], cellLocation.y - D::c[iPop][1],
                cellLocation.z - D::c[iPop][2]);

            if (this->isSolid(neighbor + offset))
            {
                plint iTriangle = -1;
                global::timer("intersect").start();
                Array<T, 3> locatedPoint;
                T distance;
                Array<T, 3> wallNormal;
                Array<T, 3> surfaceData;
                OffBoundary::Type bdType;
#ifdef PLB_DEBUG
                bool ok =
#endif
                    this->pointOnSurface(
                        cellLocation + offset, Dot3D(D::c[iPop][0], D::c[iPop][1], D::c[iPop][2]),
                        locatedPoint, distance, wallNormal, surfaceData, bdType, iTriangle);

                global::timer("intersect").stop();
                PLB_ASSERT(ok);

                solidDirections.push_back(iPop);
                boundaryIds.push_back(iTriangle);
                bool prevNodeIsPureFluid = this->isFluid(prevNode + offset);
                if (prevNodeIsPureFluid)
                {
                    hasFluidNeighbor.push_back(true);
                }
                else
                {
                    hasFluidNeighbor.push_back(false);
                }
            }
        }
        if (!solidDirections.empty())
        {
            info->getBdNodes().push_back(cellLocation);
            info->getBdNodeSolidDirections().push_back(solidDirections);
            info->getBdNodeIds().push_back(boundaryIds);
            info->getHasFluidNeighbor().push_back(hasFluidNeighbor);
        }
    }
}

template <typename T, template <typename U> class Descriptor>
void FilippovaHaenelJohanNearWallOffLatticeModel3D<T, Descriptor>::distanceYNormalCell(
    Dot3D const &cellLocation, T &minut, T &maxut,
    AtomicBlock3D &nonTypeLattice,
    std::vector<AtomicBlock3D *> &args)
{
    typedef Descriptor<T> D;
    BlockLattice3D<T, Descriptor> &lattice =
        dynamic_cast<BlockLattice3D<T, Descriptor> &>(nonTypeLattice);
    // 0:rhoVelocityNu, 1:wallDistanceY, 2:stencilFlag, 3:dynamicID
    PLB_ASSERT((plint)args.size() == 4);
    ScalarField3D<int> *dynamicIDField = dynamic_cast<ScalarField3D<int> *>(args[3]);
    // ScalarField3D<int> * stencilFlag = dynamic_cast<ScalarField3D<int>*>( args[2] );
    TensorField3D<T, 3> *wallDistanceY = dynamic_cast<TensorField3D<T, 3> *>(args[1]);
    // Dot3D offsetStencil = computeRelativeDisplacement(lattice, *stencilFlag);
    Dot3D offsetWallDistanceY = computeRelativeDisplacement(lattice, *wallDistanceY);
    Dot3D offsetDynamicID = computeRelativeDisplacement(lattice, *dynamicIDField);
    // int flag = stencilFlag->get(cellLocation.x+offsetStencil.x, cellLocation.y+offsetStencil.y, cellLocation.z+offsetStencil.z);
    int &dynamicID = dynamicIDField->get(cellLocation.x + offsetDynamicID.x, cellLocation.y + offsetDynamicID.y, cellLocation.z + offsetDynamicID.z);
    dynamicID = lattice.get(cellLocation.x, cellLocation.y, cellLocation.z).getDynamics().getId();
    int noDynId = NoDynamics<T, Descriptor>().getId();
    /// It is not a stencil node.
    // if (0 == flag) return;
    // It is a solid, do not need distance
    if (noDynId == dynamicID)
        return;

    T distance;
    Array<T, 3> wallNormal;
    bool isBehind;
    Dot3D absOffset = lattice.getLocation();
    bool ok = this->distanceToNearWallSurface(cellLocation + absOffset, distance, wallNormal, isBehind);

    // if(!std::isfinite(distance)) return;
    if (!ok || !std::isfinite(distance))
    {
        dynamicID = NO_DISTANCE;
        return;
    }

    /// isBehind == true
    if (isBehind)
    {
        printf("Fatal: cell (%d, %d, %d) is behind wall surface\n", cellLocation.x + absOffset.x, cellLocation.y + absOffset.y, cellLocation.z + absOffset.z);
        exit(1);
    }

#ifdef MIN_DISTANCE
    if (distance < MIN_DISTANCE)
    {
        dynamicID = TOO_CLOSE;
    }
#endif
    if (distance < minut)
    {
        minut = distance;
    }
    if (distance > maxut)
    {
        maxut = distance;
    }

    // T normSize = std::sqrt(wallNormal[0]*wallNormal[0]+wallNormal[1]*wallNormal[1]+wallNormal[2]*wallNormal[2]);
    Array<T, 3> &wallDistance = wallDistanceY->get(cellLocation.x + offsetWallDistanceY.x, cellLocation.y + offsetWallDistanceY.y, cellLocation.z + offsetWallDistanceY.z);
    wallDistance[0] = distance * wallNormal[0]; /// normSize;
    wallDistance[1] = distance * wallNormal[1]; /// normSize;
    wallDistance[2] = distance * wallNormal[2]; /// normSize;
}

template <typename T, template <typename U> class Descriptor>
ContainerBlockData *
FilippovaHaenelJohanNearWallOffLatticeModel3D<T, Descriptor>::generateOffLatticeInfo() const
{
    return new OffLatticeInfo3D;
}

template <typename T, template <typename U> class Descriptor>
Array<T, 9> FilippovaHaenelJohanNearWallOffLatticeModel3D<T, Descriptor>::getLocalForce(
    AtomicContainerBlock3D &container) const
{
    OffLatticeInfo3D *info =
        dynamic_cast<OffLatticeInfo3D *>(container.getData());
    PLB_ASSERT(info);
    return info->getLocalForce();
}

template <typename T, template <typename U> class Descriptor>
Array<T, 9>& FilippovaHaenelJohanNearWallOffLatticeModel3D<T, Descriptor>::getForce(
    AtomicContainerBlock3D &container)
{
    OffLatticeInfo3D *info =
        dynamic_cast<OffLatticeInfo3D *>(container.getData());
    PLB_ASSERT(info);
    return info->getLocalForce();    
}

template <typename T, template <typename U> class Descriptor>
void FilippovaHaenelJohanNearWallOffLatticeModel3D<T, Descriptor>::boundaryCompletion(
    AtomicBlock3D &nonTypeLattice,
    AtomicContainerBlock3D &container,
    std::vector<AtomicBlock3D *> const &args)
{
    BlockLattice3D<T, Descriptor> &lattice =
        dynamic_cast<BlockLattice3D<T, Descriptor> &>(nonTypeLattice);
    OffLatticeInfo3D *info =
        dynamic_cast<OffLatticeInfo3D *>(container.getData());
    PLB_ASSERT(info);
    Dot3D absoluteOffset = container.getLocation();

    std::vector<Dot3D> const &
        bdNodes = info->getBdNodes();
    std::vector<std::vector<int>> const &
        bdNodeSolidDirections = info->getBdNodeSolidDirections();
    std::vector<std::vector<plint>> const &
        bdNodeIds = info->getBdNodeIds();
    std::vector<std::vector<bool>> const &hasFluidNeighbor = info->getHasFluidNeighbor();
    PLB_ASSERT(bdNodes.size() == bdNodeSolidDirections.size());

    std::vector<plint> tmp;
    Array<T, 9> &localForce = info->getLocalForce();
    localForce.resetToZero();
    for (pluint iBd = 0; iBd < bdNodes.size(); ++iBd)
    {
        cellCompletion(
            lattice, bdNodes[iBd], bdNodeSolidDirections[iBd],
#ifdef MIN_DISTANCE
            tmp, hasFluidNeighbor[iBd], absoluteOffset, localForce, args, iBd);
#else
            bdNodeIds[iBd], hasFluidNeighbor[iBd], absoluteOffset, localForce, args, iBd);
#endif
    }
}
/*
template<typename T, template<typename U> class Descriptor>
void FilippovaHaenelJohanNearWallOffLatticeModel3D<T,Descriptor>::stencilFlagComputation (
        AtomicBlock3D& nonTypeLattice,
        AtomicContainerBlock3D& container,
        std::vector<AtomicBlock3D *> & args, Box3D const & domain )
{
    BlockLattice3D<T,Descriptor>& lattice =
        dynamic_cast<BlockLattice3D<T,Descriptor>&> (nonTypeLattice);
    OffLatticeInfo3D* info =
        dynamic_cast<OffLatticeInfo3D*>(container.getData());
    PLB_ASSERT( info );
    std::vector<Dot3D> const&
        dryNodes = info->getDryNodes();
    std::vector<std::vector<int > > const&
        dryNodeFluidDirections = info->getDryNodeFluidDirections();
    std::vector<std::vector<plint> > const&
        dryNodeIds = info->getDryNodeIds();
    PLB_ASSERT( dryNodes.size() == dryNodeFluidDirections.size() );


    for (pluint iDry=0; iDry<dryNodes.size(); ++iDry) {
    //Boundary nodes and donor nodes are marked based on solid node.
        stencilMark(lattice, dryNodes[iDry], dryNodeFluidDirections[iDry],args, iDry, domain);
    }
}

template<typename T, template<typename U> class Descriptor>
void FilippovaHaenelJohanNearWallOffLatticeModel3D<T,Descriptor>::stencilMark (
        BlockLattice3D<T,Descriptor>& lattice, Dot3D const& guoNode,
        std::vector<int> const& dryNodeFluidDirections,
        std::vector<AtomicBlock3D *> & args, pluint iDry, Box3D const& domain)
{
    typedef Descriptor<T> D;
    Cell<T,Descriptor>& s_cell =
        lattice.get( guoNode.x, guoNode.y, guoNode.z );
    int noDynId = NoDynamics<T,Descriptor>().getId();
    //It means that solid cell does not collide.
    PLB_ASSERT( s_cell.getDynamics().getId() == noDynId
        && "Filippova-Haenel BC needs the dynamics to be set to NoDynamics.");
    //0:rhoVelocityNu, 1:wallDistanceY, 2:stencilFlag, 3:dynamicID, 4:utau, 5:stressField
    //PLB_ASSERT((plint) args.size() == 4);
    ScalarField3D<int> * stencilFlag = dynamic_cast<ScalarField3D<int>*>( args[2] );
    ScalarField3D<int> * dynamicID = dynamic_cast<ScalarField3D<int>*>( args[3] );
    Dot3D offsetStencil = computeRelativeDisplacement(lattice, *stencilFlag);
    Dot3D offsetDynamicID = computeRelativeDisplacement(lattice, *dynamicID);
    //Test converge
    const Dot3D absOff = lattice.getLocation();
    const Dot3D globalSolid(guoNode.x+absOff.x,guoNode.y+absOff.y,guoNode.z+absOff.z);
    //Test end
    //Is the node a solid node?
    bool sNode = false;
    //Is the node a boundary node?
    bool bNode = false;
    //stencilNodeID solid or liquid?
    int stencilNodeID;

    //loop all of neighbor fluid cells of dry nodes
    for (plint iDirection=0; iDirection<(plint)dryNodeFluidDirections.size(); ++iDirection){
        int iOpp = dryNodeFluidDirections[iDirection];
        Dot3D fluidDirection(D::c[iOpp][0],D::c[iOpp][1],D::c[iOpp][2]);
    //neighbor fluid cells
    Dot3D boundaryNode(guoNode.x+fluidDirection.x, guoNode.y+fluidDirection.y, guoNode.z+fluidDirection.z);

    //boundary node is set as one, which requires interpolation from donor node
    stencilFlag->get(boundaryNode.x+offsetStencil.x,boundaryNode.y+offsetStencil.y,boundaryNode.z+offsetStencil.z) = 1;
    for (plint qID = 0; qID < D::q; qID++){
            Dot3D stencilNodeOne(boundaryNode.x+D::c[qID][0], boundaryNode.y+D::c[qID][1], boundaryNode.z+D::c[qID][2]);
                Dot3D stencilNodeTwo(boundaryNode.x+2*D::c[qID][0], boundaryNode.y+2*D::c[qID][1], boundaryNode.z+2*D::c[qID][2]);
        //Judge stencilNodeOne donor node?
                stencilNodeID = dynamicID->get( stencilNodeOne.x+offsetDynamicID.x, stencilNodeOne.y+offsetDynamicID.y, stencilNodeOne.z+offsetDynamicID.z );
        //If the stencilNode is a solid node
                sNode = (stencilNodeID == noDynId) ? true:false;
        //Does the node owns solid neighbor?
                for (plint qID_NB = 0; qID_NB < D::q; qID_NB++){
                        Dot3D stencilNode_NB(stencilNodeOne.x+D::c[qID_NB][0],stencilNodeOne.y+D::c[qID_NB][1],stencilNodeOne.z+D::c[qID_NB][2]);
                        stencilNodeID = dynamicID->get( stencilNode_NB.x+offsetDynamicID.x, stencilNode_NB.y+offsetDynamicID.y, stencilNode_NB.z+offsetDynamicID.z );
                        bNode = (stencilNodeID == noDynId) ? true:false;
            //The stencil node owns a solid node, which is a boundry node, not a donor node
                        if (bNode == true) break;
                }
        if ((!sNode)&&(!bNode)) stencilFlag->get(stencilNodeOne.x+offsetStencil.x,stencilNodeOne.y+offsetStencil.y,stencilNodeOne.z+offsetStencil.z) = 2;
        //Judge stencilNodeOne donor node?
                stencilNodeID = dynamicID->get( stencilNodeTwo.x+offsetDynamicID.x, stencilNodeTwo.y+offsetDynamicID.y, stencilNodeTwo.z+offsetDynamicID.z );
        //If the stencilNode is a solid node
                sNode = (stencilNodeID == noDynId) ? true:false;
        //Does the node owns solid neighbor?
                for (plint qID_NB = 0; qID_NB < D::q; qID_NB++){
                        Dot3D stencilNode_NB(stencilNodeTwo.x+D::c[qID_NB][0],stencilNodeTwo.y+D::c[qID_NB][1],stencilNodeTwo.z+D::c[qID_NB][2]);
                        stencilNodeID = dynamicID->get( stencilNode_NB.x+offsetDynamicID.x, stencilNode_NB.y+offsetDynamicID.y, stencilNode_NB.z+offsetDynamicID.z );
                        bNode = (stencilNodeID == noDynId) ? true:false;
            //The stencil node owns a solid node, which is a boundry node, not a donor node
                        if (bNode == true) break;
                }
        if ((!sNode)&&(!bNode)) stencilFlag->get(stencilNodeTwo.x+offsetStencil.x,stencilNodeTwo.y+offsetStencil.y,stencilNodeTwo.z+offsetStencil.z) = 2;
    }

    }

}
*/
template <typename T, template <typename U> class Descriptor>
void FilippovaHaenelJohanNearWallOffLatticeModel3D<T, Descriptor>::setDx(T dx_, T groundClearance_)
{
    dx = dx_;
    groundClearance = groundClearance_;
}

template <typename T, template <typename U> class Descriptor>
void FilippovaHaenelJohanNearWallOffLatticeModel3D<T, Descriptor>::cellCompletion(
    BlockLattice3D<T, Descriptor> &lattice, Dot3D const &bdNode,
    std::vector<int> const &bdNodeSolidDirections,
    std::vector<plint> const &bdNodeIds, std::vector<bool> const &hasFluidNeighbor, Dot3D const &absoluteOffset,
    Array<T, 9> &localForce, std::vector<AtomicBlock3D *> const &args, pluint iBd)
{

    typedef Descriptor<T> D;
    typedef SymmetricTensorImpl<T, Descriptor<T>::d> S;
    Cell<T, Descriptor> &f_cell =
        lattice.get(bdNode.x, bdNode.y, bdNode.z);
    // #ifdef PLB_DEBUG
    int noDynId =
        NoDynamics<T, Descriptor>().getId();
    // #endif
    // It means that solid cell does not collide.

    // 0:rhoVelocityNufield,1:wallDistanceY,2:stencilFlag,3:dynamicID,4:utau,5:stress,6:modifyRhoVelocityField
    PLB_ASSERT((plint)args.size() == 6);
    NTensorField3D<T> const *rhoVelocityNuField = dynamic_cast<NTensorField3D<T> const *>(args[0]);
    TensorField3D<T, 3> const *wallDistanceYField = dynamic_cast<TensorField3D<T, 3> const *>(args[1]);
    TensorField3D<T, 4> const *modifyRhoVelocityField = dynamic_cast<TensorField3D<T, 4> const *>(args[6]);
    TensorField3D<T, 6> const *stressField = dynamic_cast<TensorField3D<T, 6> const *>(args[5]);
    ScalarField3D<int> const *stencilField = dynamic_cast<ScalarField3D<int> const *>(args[2]);
    ScalarField3D<int> const *dynamicIDField = dynamic_cast<ScalarField3D<int> const *>(args[3]);

    const Dot3D offsetrhoVelocityNu = computeRelativeDisplacement(lattice, *rhoVelocityNuField);
    const Dot3D offsetWallDistanceY = computeRelativeDisplacement(lattice, *wallDistanceYField);
    const Dot3D offsetModify = computeRelativeDisplacement(lattice, *modifyRhoVelocityField);
    const Dot3D offsetStress = computeRelativeDisplacement(lattice, *stressField);
    const Dot3D offsetStencil = computeRelativeDisplacement(lattice, *stencilField);
    const Dot3D offsetDynamicID = computeRelativeDisplacement(lattice, *dynamicIDField);
    const Dot3D absOff = lattice.getLocation();
    Dot3D boundaryNode(bdNode.x, bdNode.y, bdNode.z);
    Array<T, 3> const &wallDistanceYNormal = wallDistanceYField->get(boundaryNode.x + offsetWallDistanceY.x, boundaryNode.y + offsetWallDistanceY.y, boundaryNode.z + offsetWallDistanceY.z);
    Array<T, 4> const &modifyRhoVelocity = modifyRhoVelocityField->get(boundaryNode.x + offsetModify.x, boundaryNode.y + offsetModify.y, boundaryNode.z + offsetModify.z);
    Array<T, 6> const &stress = stressField->get(boundaryNode.x + offsetStress.x, boundaryNode.y + offsetStress.y, boundaryNode.z + offsetStress.z);
    const T rho = modifyRhoVelocity[0];

    T distance = std::sqrt(wallDistanceYNormal[0] * wallDistanceYNormal[0] + wallDistanceYNormal[1] * wallDistanceYNormal[1] + wallDistanceYNormal[2] * wallDistanceYNormal[2]);
    Array<T, 3> wNormal;
    wNormal[0] = wallDistanceYNormal[0] / (1.0e-12 + distance);
    wNormal[1] = wallDistanceYNormal[1] / (1.0e-12 + distance);
    wNormal[2] = wallDistanceYNormal[2] / (1.0e-12 + distance);
#ifdef AHMED_ONLY
    if (wNormal[0] > 0.001)
    {
#else
    if(false)
    {
#endif
        Array<T, D::d> deltaJ;
        deltaJ.resetToZero();

        plint numNeumannNodes = 0;
        T neumannDensity = T();
        for (pluint i = 0; i < bdNodeSolidDirections.size(); ++i)
        {
            int iPop = bdNodeSolidDirections[i];
            deltaJ[0] += D::c[iPop][0] * f_cell[iPop];
            deltaJ[1] += D::c[iPop][1] * f_cell[iPop];
            deltaJ[2] += D::c[iPop][2] * f_cell[iPop];
        }

        for (plint i = 0; i < (plint)bdNodeSolidDirections.size(); ++i)
        {
            int iPop = bdNodeSolidDirections[i];
            int oppPop = indexTemplates::opposite<D>(iPop);
            Array<T, 3> wallNode, wall_vel;
            T AC;
            OffBoundary::Type bdType;
            Array<T, 3> wallNormal;
            plint id = bdNodeIds[i];

            bool ok = this->pointOnSurface(
                bdNode + absoluteOffset, Dot3D(D::c[iPop][0], D::c[iPop][1], D::c[iPop][2]),
                wallNode, AC, wallNormal, wall_vel, bdType, id);
            PLB_ASSERT(ok);
            T q = AC * invAB[iPop];
            Cell<T, Descriptor> &iCell = lattice.get(
                bdNode.x + D::c[iPop][0], bdNode.y + D::c[iPop][1],
                bdNode.z + D::c[iPop][2]);
            Cell<T, Descriptor> &jCell = lattice.get(
                bdNode.x - D::c[iPop][0], bdNode.y - D::c[iPop][1],
                bdNode.z - D::c[iPop][2]);
            if (bdType == OffBoundary::dirichlet)
            {
                T u_ci = D::c[iPop][0] * wall_vel[0] + D::c[iPop][1] * wall_vel[1] + D::c[iPop][2] * wall_vel[2];
                plint numUnknown = 0;
                if (q < (T)0.5)
                {
                    if (hasFluidNeighbor[i])
                    {
                        f_cell[oppPop] = 2. * q * iCell[iPop] + (1. - 2. * q) * f_cell[iPop];
                    }
                    else
                    {
                        ++numUnknown;
                        f_cell[oppPop] = iCell[iPop];
                    }
                    f_cell[oppPop] -= 2. * u_ci * D::t[iPop] * D::invCs2;
                }
                else
                {
                    f_cell[oppPop] =
                        1. / (2. * q) * iCell[iPop] + (2. * q - 1) / (2. * q) * jCell[oppPop];
                    f_cell[oppPop] -= 1. / q * u_ci * D::t[iPop] * D::invCs2;
                }
            }
            else if (bdType == OffBoundary::densityNeumann)
            {
                ++numNeumannNodes;
                neumannDensity += wall_vel[0];
                f_cell[oppPop] = jCell[oppPop];
            }
            else
            {
                PLB_ASSERT(false);
            }
        }

        Cell<T, Descriptor> collidedCell(f_cell);
        BlockStatistics statsCopy(lattice.getInternalStatistics());
        collidedCell.collide(statsCopy);

        for (pluint i = 0; i < bdNodeSolidDirections.size(); ++i)
        {
            int iPop = bdNodeSolidDirections[i];
            int oppPop = indexTemplates::opposite<D>(iPop);
            deltaJ[0] -= D::c[oppPop][0] * collidedCell[oppPop];
            deltaJ[1] -= D::c[oppPop][1] * collidedCell[oppPop];
            deltaJ[2] -= D::c[oppPop][2] * collidedCell[oppPop];
        }

        localForce[0] += deltaJ[0];
        localForce[1] += deltaJ[1];
        localForce[2] += deltaJ[2];
        if (numNeumannNodes > 0)
        {
            neumannDensity /= numNeumannNodes;
            T oldRhoBar;
            Array<T, 3> j;
            momentTemplates<T, Descriptor>::get_rhoBar_j(f_cell, oldRhoBar, j);
            T newRhoBar = D::rhoBar(neumannDensity);
            T jSqr = normSqr(j);
            for (plint iPop = 0; iPop < D::q; ++iPop)
            {
                T oldEq = f_cell.getDynamics().computeEquilibrium(iPop, oldRhoBar, j, jSqr);
                T newEq = f_cell.getDynamics().computeEquilibrium(iPop, newRhoBar, j, jSqr);
                f_cell[iPop] += newEq - oldEq;
            }
        }
    }
    else
    {
        // T const * rhoVelocityNu = rhoVelocityNuField->get(boundaryNode.x+offsetModify.x,boundaryNode.y+offsetModify.y,boundaryNode.z+offsetModify.z);
        // const T rho = rhoVelocityNu[0];

        Array<T, 3> vel, j;
        vel[0] = modifyRhoVelocity[1];
        vel[1] = modifyRhoVelocity[2];
        vel[2] = modifyRhoVelocity[3];
        j = rho * vel;
        const T jSqr = VectorTemplate<T, Descriptor>::normSqr(j);
        const T omega = f_cell.getDynamics().getDynamicParameter(dynamicParams::dynamicOmega, f_cell);
        // const T omega = f_cell.getDynamics().getOmega();
        Cell<T, Descriptor> cellProxy(f_cell);
        const T sToPi = -rho / Descriptor<T>::invCs2 / omega;
        Array<T, SymmetricTensor<T, Descriptor>::n> pi;
        pi[S::xx] = (T)2 * stress[S::xx] * sToPi;
        pi[S::yy] = (T)2 * stress[S::yy] * sToPi;
        pi[S::zz] = (T)2 * stress[S::zz] * sToPi;
        pi[S::xy] = (T)2 * stress[S::xy] * sToPi;
        pi[S::xz] = (T)2 * stress[S::xz] * sToPi;
        pi[S::yz] = (T)2 * stress[S::yz] * sToPi;

        f_cell.getDynamics().regularize(cellProxy, Descriptor<T>::rhoBar(rho), j, jSqr, pi);
#ifdef FULL_POP_CONSTRUCTION
        for (int qID = 0; qID < D::q; qID++)
        {
            f_cell[qID] = cellProxy[qID];
        }
#endif
        for (plint iDirection = 0; iDirection < (plint)bdNodeSolidDirections.size(); ++iDirection)
        {
            int iOpp = bdNodeSolidDirections[iDirection];
            int iPop = indexTemplates::opposite<Descriptor<T>>(iOpp);
            Dot3D solidDirection(D::c[iOpp][0], D::c[iOpp][1], D::c[iOpp][2]);
            Dot3D fluidDirection(D::c[iPop][0], D::c[iPop][1], D::c[iPop][2]);
            Dot3D s_node(boundaryNode.x + solidDirection.x, boundaryNode.y + solidDirection.y, boundaryNode.z + solidDirection.z);

            Cell<T, Descriptor> &s_cell = lattice.get(boundaryNode.x + solidDirection.x, boundaryNode.y + solidDirection.y, boundaryNode.z + solidDirection.z);
            // PLB_ASSERT( s_cell.getDynamics().getId() == noDynId
            //&& "Filippova-Haenel BC needs the dynamics to be set to NoDynamics.");

            s_cell[iPop] = cellProxy[iPop];
#ifdef MIN_DISTANCE
            localForce[0] = D::c[iOpp][0] * (s_cell[iOpp] + s_cell[iPop]);
            localForce[1] = D::c[iOpp][1] * (s_cell[iOpp] + s_cell[iPop]);
            localForce[2] = D::c[iOpp][2] * (s_cell[iOpp] + s_cell[iPop]);
#else
            Array<T, 3> wallNormal;
            plint dryNodeId = bdNodeIds[iDirection];
            Array<T, 3> wallNode, wall_vel;
            T wallDistance;
            OffBoundary::Type bdType;
#ifdef PLB_DEBUG
            bool ok =
#endif
                this->pointOnSurface(bdNode + absoluteOffset, solidDirection,
                                     wallNode, wallDistance, wallNormal,
                                     wall_vel, bdType, dryNodeId);
            PLB_ASSERT(ok);

            Array<T, 3> tmpForce, perForce, parForce;
            tmpForce[0] = D::c[iOpp][0] * (s_cell[iOpp] + s_cell[iPop]);
            tmpForce[1] = D::c[iOpp][1] * (s_cell[iOpp] + s_cell[iPop]);
            tmpForce[2] = D::c[iOpp][2] * (s_cell[iOpp] + s_cell[iPop]);

            T perForceAmp = dot(wallNormal, tmpForce);
            perForce = perForceAmp * wallNormal;
            parForce = tmpForce - perForce;

            if ((s_node.z + absoluteOffset.z) * dx >= 0.05)
            {
                if (wallNormal[0] <= 0)
                {
                    localForce[3] += perForce[0];
                }
                else
                {
                    if (wallNormal[2] >= wallNormal[0])
                    {
                        localForce[4] += perForce[0];
                    }
                    else
                    {
                        localForce[5] += perForce[0];
                        if ((fluidDirection.x == 1) && (fluidDirection.y == 0) && (fluidDirection.z == 0))
                        {
                            localForce[7] -= Descriptor<T>::rhoBar(rho) * wallNormal[0];
                        }
                    }
                }

                localForce[6] += parForce[0];
            }
            else
            {
                if (wallNormal[0] <= 0)
                {
                    localForce[8] += tmpForce[0];
                }
                else
                {
                    localForce[1] += tmpForce[0];
                }
            }

            localForce[0] += tmpForce[0];
            localForce[2] += tmpForce[2];
#endif
        }
    }
}
////////////////////////////////////////////////////////////////
// data processor for calculating utau on donor nodes of stencil
template <typename T, template <typename U> class Descriptor>
stencilUtauCompFunctional3D<T, Descriptor>::stencilUtauCompFunctional3D(int numShapeArgs_, int numCompletionArgs_)
    : numShapeArgs(numShapeArgs_),
      numCompletionArgs(numCompletionArgs_)
{
}

template <typename T, template <typename U> class Descriptor>
void stencilUtauCompFunctional3D<T, Descriptor>::processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D *> blocks)
{
    PLB_ASSERT(blocks.size() == 2 + numShapeArgs + numCompletionArgs);
    // numCompletionArgs:0:rhoVelocityNufield,1:wallDistanceY,2:stencilFlag,3:dynamicID,4:utau, 5:stressTensorField
    BlockLattice3D<T, Descriptor> *lattice = dynamic_cast<BlockLattice3D<T, Descriptor> *>(blocks[0]);
    NTensorField3D<T> *rhoVelocityNuField = dynamic_cast<NTensorField3D<T> *>(blocks[2 + numShapeArgs]);
    TensorField3D<T, 3> *wallDistanceY = dynamic_cast<TensorField3D<T, 3> *>(blocks[2 + numShapeArgs + 1]);
    ScalarField3D<int> *stencilFlag = dynamic_cast<ScalarField3D<int> *>(blocks[2 + numShapeArgs + 2]);
    ScalarField3D<T> *utau = dynamic_cast<ScalarField3D<T> *>(blocks[2 + numShapeArgs + 4]);

    PLB_ASSERT(lattice);

    typedef Descriptor<T> D;
    Dot3D offsetrhoVelocityNu = computeRelativeDisplacement(*lattice, *rhoVelocityNuField);
    Dot3D offsetStencil = computeRelativeDisplacement(*lattice, *stencilFlag);
    Dot3D offsetWallDistanceY = computeRelativeDisplacement(*lattice, *wallDistanceY);
    Dot3D offsetUtau = computeRelativeDisplacement(*lattice, *utau);

    Dot3D absOff = lattice->getLocation();
    wallFunctionMethod = WALL_FUNCTION;
    for (plint iX = domain.x0; iX <= domain.x1; ++iX)
    {
        for (plint iY = domain.y0; iY <= domain.y1; ++iY)
        {
            for (plint iZ = domain.z0; iZ <= domain.z1; ++iZ)
            {
                int flag = stencilFlag->get(iX + offsetStencil.x, iY + offsetStencil.y, iZ + offsetStencil.z);
                // donor nodes
                if (2 == flag)
                {
                    Dot3D globalNode(iX + absOff.x, iY + absOff.y, iZ + absOff.z);
                    T &utauDonor = utau->get(iX + offsetUtau.x, iY + offsetUtau.y, iZ + offsetUtau.z);
                    Array<T, 3> distanceNormal = wallDistanceY->get(iX + offsetWallDistanceY.x, iY + offsetWallDistanceY.y, iZ + offsetWallDistanceY.z);
                    T *rhoVelocityNu = rhoVelocityNuField->get(iX + offsetrhoVelocityNu.x, iY + offsetrhoVelocityNu.y, iZ + offsetrhoVelocityNu.z);
                    Array<T, 3> velocity;
                    T nu;
                    nu = rhoVelocityNu[4];
                    velocity.from_cArray(rhoVelocityNu + 1);
                    // computeUtau(utauDonor, distanceNormal, velocity, nu, wallFunctionMethod);

                    T wallDistance = std::sqrt(distanceNormal[0] * distanceNormal[0] + distanceNormal[1] * distanceNormal[1] + distanceNormal[2] * distanceNormal[2]);
                    Array<T, 3> wallNormal;
                    wallNormal[0] = distanceNormal[0] / wallDistance;
                    wallNormal[1] = distanceNormal[1] / wallDistance;
                    wallNormal[2] = distanceNormal[2] / wallDistance;
                    T normalVelocity = wallNormal[0] * velocity[0] + wallNormal[1] * velocity[1] + wallNormal[2] * velocity[2];
                    Array<T, 3> velocityTangential, vtNorm;
                    velocityTangential[0] = velocity[0] - normalVelocity * wallNormal[0];
                    velocityTangential[1] = velocity[1] - normalVelocity * wallNormal[1];
                    velocityTangential[2] = velocity[2] - normalVelocity * wallNormal[2];
                    T tangentialVelocity = std::sqrt(velocityTangential[0] * velocityTangential[0] + velocityTangential[1] * velocityTangential[1] + velocityTangential[2] * velocityTangential[2]);
                    vtNorm[0] = velocityTangential[0] / tangentialVelocity;
                    vtNorm[1] = velocityTangential[1] / tangentialVelocity;
                    vtNorm[2] = velocityTangential[2] / tangentialVelocity;
                    int qmin = 0;
                    int qmax = 0;
                    T mindis = 0.0;
                    T maxdis = 0.0;
                    T dpds = 0.0;
                    for (int qID = 1; qID < D::q; qID++)
                    {
                        const Dot3D neighbor(iX + D::c[qID][0], iY + D::c[qID][1], iZ + D::c[qID][2]);
                        int flag_n = stencilFlag->get(neighbor.x + offsetStencil.x, neighbor.y + offsetStencil.y, neighbor.z + offsetStencil.z);
                        if (2 == flag_n)
                        {
                            T tmp = (D::c[qID][0] * vtNorm[0] + D::c[qID][1] * vtNorm[1] + D::c[qID][2] * vtNorm[2]) / std::sqrt(D::c[qID][0] * D::c[qID][0] + D::c[qID][1] * D::c[qID][1] + D::c[qID][2] * D::c[qID][2]);
                            if (tmp < mindis)
                            {
                                mindis = tmp;
                                qmin = qID;
                            }
                            if (tmp > maxdis)
                            {
                                maxdis = tmp;
                                qmax = qID;
                            }
                        }
                    }
                    if (qmin > 0 && qmax > 0)
                    {
                        const Dot3D neighbor(iX + D::c[qmin][0], iY + D::c[qmin][1], iZ + D::c[qmin][2]);
                        const Dot3D neighbor2(iX + D::c[qmax][0], iY + D::c[qmax][1], iZ + D::c[qmax][2]);
                        T *rhoVelocityNu_n = rhoVelocityNuField->get(neighbor.x + offsetrhoVelocityNu.x, neighbor.y + offsetrhoVelocityNu.y, neighbor.z + offsetrhoVelocityNu.z);
                        T *rhoVelocityNu_n2 = rhoVelocityNuField->get(neighbor2.x + offsetrhoVelocityNu.x, neighbor2.y + offsetrhoVelocityNu.y, neighbor2.z + offsetrhoVelocityNu.z);
                        dpds = D::cs2 * (rhoVelocityNu_n2[0] - rhoVelocityNu_n[0]) / (std::fabs(mindis * std::sqrt(D::c[qmin][0] * D::c[qmin][0] + D::c[qmin][1] * D::c[qmin][1] + D::c[qmin][2] * D::c[qmin][2])) + std::fabs(maxdis * std::sqrt(D::c[qmax][0] * D::c[qmax][0] + D::c[qmax][1] * D::c[qmax][1] + D::c[qmax][2] * D::c[qmax][2])));
                    }
                    bool isforward = vtNorm[0] >= 0? true: false;
                    // computeUtau(globalNode, utauDonor, distanceNormal, velocity, nu, wallFunctionMethod);
                    computeUtau(globalNode, utauDonor, wallDistance, tangentialVelocity, rhoVelocityNu[0], dpds, nu, wallFunctionMethod, isforward);
                }
            }
        }
    }
}

template <typename T, template <typename U> class Descriptor>
// void stencilUtauCompFunctional3D<T,Descriptor>::computeUtau(T & utau, Array<T,3> distanceNormal, Array<T,3> velocity, T nu, int wallMethod){
// void stencilUtauCompFunctional3D<T,Descriptor>::computeUtau(Dot3D const & globalNode, T & utau, Array<T,3> distanceNormal, Array<T,3> velocity, T nu, int wallMethod){
void stencilUtauCompFunctional3D<T, Descriptor>::computeUtau(Dot3D const &globalNode, T &utau, T wallDistance, T tangentialVelocity, T rho, T dpds, T nu, int wallMethod, bool isforward)
{
    T const KK = 0.41;
    T const B = 5.0;
    T D = tangentialVelocity - 7.5789 * std::sqrt(wallDistance * dpds / rho) + 1.4489 * std::pow(nu * dpds / rho, 1.0 / 3.0) * std::log(191.1799 * std::pow(wallDistance, 3.0) * dpds / rho / nu / nu);
    T yplus = 0.0;
    T fy = 0.0;
    // method 1
    T fyAtan;
    T fyLogTop;
    T fyLogDown;
    T fyLog;
    T dfydy = 0.0;
    T utauOld = 0.0;
    T dfyAtandy;
    T dfyLogdy;
    int iter = 0;
    // Initialization
    // when utau is quite close to 0.0
    if (fabs(utau - 0.0) < 1.0e-12)
        utau = std::sqrt(tangentialVelocity * nu / wallDistance);
    T utauInit = utau;
    switch (wallMethod)
    {
    case 0:
        for (iter = 0; iter < 100; iter++)
        {
            yplus = wallDistance * utau / nu;
            fy = std::log(yplus) / KK + B;
            dfydy = 1.0 / KK / yplus;
            utauOld = utau;
            utau = utau - (utau * fy - tangentialVelocity) / (fy + yplus * dfydy);
            if (fabs(utau - utauOld) / fabs(utauOld + 1.0e-12) < 0.01)
                break;
            if (iter > 10)
            {
                utau = std::sqrt(tangentialVelocity * nu / wallDistance);
                break;
            }
        }
        if (iter >= 50)
            printf("Warning: iter is more than 50!\n");
        break;
    case 1:
        for (iter = 0; iter < 100; iter++)
        {
            yplus = wallDistance * utau / nu;
            fyAtan = (2.0 * yplus - 8.15) / 16.7;
            fyLogTop = std::pow(yplus + 10.6, 9.6);
            fyLogDown = std::pow(yplus * yplus - 8.15 * yplus + 86.0, 2.0);
            fyLog = fyLogTop / fyLogDown;
            fy = 5.424 * std::atan(fyAtan) + 0.434 * std::log(fyLog) - 3.507;

            dfyAtandy = 2.0 / 16.7;
            dfyLogdy = (9.6 * pow(yplus + 10.6, 8.6) * fyLogDown - fyLogTop * (2.0 * (yplus * yplus - 8.15 * yplus + 86.0) * (2 * yplus - 8.15))) / (fyLogDown * fyLogDown);
            dfydy = 5.424 / (1 + fyAtan * fyAtan) * dfyAtandy + 0.434 / fyLog * dfyLogdy;
            utauOld = utau;
            utau = utau - (utau * fy - tangentialVelocity) / (fy + yplus * dfydy);
            if (fabs(utau - utauOld) / fabs(utauOld + 1.0e-12) < 0.01)
                break;
        }

        if (iter >= 100)
        {
            utau = std::sqrt(tangentialVelocity * nu / wallDistance);
            yplus = wallDistance * utau / nu;
            if (yplus > 11.81)
                utau = std::pow(tangentialVelocity / 8.3, 7.0 / 8.0) * std::pow(nu / wallDistance, 1.0 / 8.0);
        }

        break;
    case 2:
    {
        /*
        T Re_y = tangentialVelocity*wallDistance/nu;
        if(Re_y<2 || dpds > 0) {
        utau = std::sqrt(tangentialVelocity*nu/wallDistance);
        } else {
        T x = 0.41*7.9*Re_y;
        T W = std::log(x/std::log(x/std::log(x)));
        yplus = std::pow((1.0 - std::tanh(Re_y/180.8)), 0.789)*std::sqrt(Re_y)+(std::pow(std::tanh(Re_y/180.8),0.789)*std::exp(W)/7.9);
        utau = yplus*nu/wallDistance;
        }
        */

        if (dpds <= 0.0)
        {
            // Power law
            /*
            utau = std::sqrt(tangentialVelocity*nu/wallDistance);
            yplus = wallDistance * utau / nu;
            if(yplus > 11.81) utau = std::pow(tangentialVelocity/8.3, 7.0/8.0)*std::pow(nu/wallDistance, 1.0/8.0);
            */
            T Re_y = tangentialVelocity * wallDistance / nu;
            if (Re_y < 2)
            {
                utau = std::sqrt(tangentialVelocity * nu / wallDistance);
            }
            else
            {
                T x = 0.41 * 7.9 * Re_y;
                T W = std::log(x / std::log(x / std::log(x)));
                yplus = std::pow((1.0 - std::tanh(Re_y / 180.8)), 0.789) * std::sqrt(Re_y) + (std::pow(std::tanh(Re_y / 180.8), 0.789) * std::exp(W) / 7.9);
                utau = yplus * nu / wallDistance;
            }
        }
        else
        {
            if (D < 0)
            {
                utau = std::sqrt(tangentialVelocity * nu / wallDistance);
            }
            else
            {
                utau = std::sqrt(tangentialVelocity * nu / wallDistance);
                yplus = wallDistance * utau / nu;
                if (yplus > 11.81)
                    utau = std::pow(1 / 8.3, 7.0 / 8.0) * std::pow(nu / wallDistance, 1.0 / 8.0) * std::pow(D, 7.0 / 8.0);
            }
        }
    }
    break;
    case 3:
    { // SA
        T Re_y = tangentialVelocity * wallDistance / nu;
        if (Re_y < 2)
        {
            utau = std::sqrt(tangentialVelocity * nu / wallDistance);
        }
        else
        {
            T x = 0.41 * 7.9 * Re_y;
            T W = std::log(x / std::log(x / std::log(x)));
            yplus = std::pow((1.0 - std::tanh(Re_y / 180.8)), 0.789) * std::sqrt(Re_y) + (std::pow(std::tanh(Re_y / 180.8), 0.789) * std::exp(W) / 7.9);
            utau = yplus * nu / wallDistance;
        }
    }
    break;
    case 4:
    { // afzal
        // pcout << "dpds: " << dpds << std::endl;
        int iter = 0;
        utauInit = 0.0001;
        T utauOld = utauInit;
        wallDistance *= 0.00976;
        T yplus = wallDistance * utauInit / nu;
        T kplus = nu * dpds / rho / utauInit / utauInit / utauInit;
        T A = 2 / 0.41;
        T C = 10;
        T B = A * (1 - std::log(2));

        T dfydy, dkplus_dutau = -3 * nu * dpds / rho * std::pow(utauInit, -4);
        T dyplus_dutau = wallDistance / nu;
        T dkyplus = kplus * dyplus_dutau + yplus * dkplus_dutau;
        // bool isforward = true;
        T fy;
        T utau;
        T minValue = HUGE_VAL;
        // do {
        if (isforward) {
            do {
                T kyplus = kplus * yplus;
                fy = A / 2 * (std::log(yplus) + 2 * B / A + 0.5 * kyplus);
                dfydy = A / 2 * (0.5 * dkyplus);
                dfydy += A / 2 / yplus * dyplus_dutau;

                // fy = A * std::pow(kyplus, 0.5) + std::pow(kplus, 1.0 / 3.0) * C - A / 2 * std::pow(kyplus, -0.5);
                // dfydy = 0.5 * A * std::pow(kyplus, -0.5) + A / 4 * std::pow(kyplus, -1.5);
                // dfydy *= dkyplus;
                // dfydy += C * std::pow(kplus, -2.0 / 3.0) * dkplus_dutau;

                if (iter == 0) {
                    utau = utauInit;
                }   

                T temp = utau - (utau * fy - tangentialVelocity) / (fy + utau * dfydy);

                if (fabs(minValue) > fabs(utau * fy - tangentialVelocity)) {
                    minValue = utau * fy - tangentialVelocity;
                    utau = temp;
                } else {
                    break;
                }
                utau = temp;

                if (fabs(utauOld - utau) / utauOld < 1e-3) {
                    break;
                } else {
                    utauOld = utau;
                }
                yplus = wallDistance * utau / nu;
                kplus = nu * fabs(dpds) / rho / utau / utau / utau;
                dkplus_dutau = -3 * nu * dpds / rho * std::pow(utau, -4);
                dkyplus = kplus * dyplus_dutau + yplus * dkplus_dutau;
                ++iter;
            } while (iter < 20); 
        } else {
            utauInit = 1e-4;
            utauOld = utauInit;
            do {                
                T kyplus = kplus * yplus;
                T kyplusMinus1 = kyplus - 1;
                if (kyplusMinus1 > 0) {
                    fy = 2.4 * (2 * std::pow(kyplusMinus1, 0.5) - 2 * std::atan(std::pow(kyplusMinus1, 0.5)));
                    dfydy = 2.4 * std::pow(kyplusMinus1, -0.5) - ((2 * kyplusMinus1) + 1) * std::pow(kyplusMinus1, -0.5);
                    dfydy *= dkyplus;
                } else {
                    T Re_y = tangentialVelocity * wallDistance / nu;
                    if (Re_y < 2)
                    {
                        utau = std::sqrt(tangentialVelocity * nu / wallDistance);
                    }
                    else
                    {
                        T x = 0.41 * 7.9 * Re_y;
                        T W = std::log(x / std::log(x / std::log(x)));
                        yplus = std::pow((1.0 - std::tanh(Re_y / 180.8)), 0.789) * std::sqrt(Re_y) + (std::pow(std::tanh(Re_y / 180.8), 0.789) * std::exp(W) / 7.9);
                        utau = yplus * nu / wallDistance;
                        break;
                    }
                }

                if (iter == 0) {
                    utau = utauInit;
                } 

                T temp = utau - (utau * fy - tangentialVelocity) / (fy + utau * dfydy);

                if (fabs(minValue) > fabs(utau * fy - tangentialVelocity)) {
                    minValue = utau * fy - tangentialVelocity;
                    utau = temp;
                } else {
                    break;
                }

                if (fabs(utauOld - utau) / utauOld < 1e-2) {
                    break;
                } else {
                    utauOld = utau;
                }
                yplus = wallDistance * utau / nu;
                kplus = nu * fabs(dpds) / rho / utau / utau / utau;
                dkplus_dutau = -3 * nu * dpds / rho * std::pow(utau, -4);
                dkyplus = kplus * dyplus_dutau + yplus * dkplus_dutau;             
                ++iter;
            } while (iter < 20);
        }
        if (utau < 0) {
            utau = utauInit;
        }
        
    }
    break;
    default:
        printf("Fatal, wallFunctionMethod = %d\n", wallMethod);
        exit(1);
    }
}

template <typename T, template <typename U> class Descriptor>
stencilUtauCompFunctional3D<T, Descriptor> *stencilUtauCompFunctional3D<T, Descriptor>::clone() const
{
    return new stencilUtauCompFunctional3D<T, Descriptor>(*this);
}

template <typename T, template <typename U> class Descriptor>
void stencilUtauCompFunctional3D<T, Descriptor>::getTypeOfModification(std::vector<modif::ModifT> &modified) const
{
    for (int item = 0; item < 2 + numShapeArgs + numCompletionArgs - 1; item++)
    {
        modified[item] = modif::nothing;
    }
    // modified[2+numShapeArgs+numCompletionArgs-1] = modif::staticVariables;
    // utau should be modified
    modified[2 + numShapeArgs + 4] = modif::staticVariables;
}

template <typename T, template <typename U> class Descriptor>
BlockDomain::DomainT stencilUtauCompFunctional3D<T, Descriptor>::appliesTo() const
{
    return BlockDomain::bulk;
}

////////////////////////////////////////////////////////////
// data processor for interpolating utau and density from donor nodes to boundry nodes (fluid neighbor nodes)
template <typename T, template <typename U> class Descriptor>
boundaryUtauDensityInterpFunctional3D<T, Descriptor>::boundaryUtauDensityInterpFunctional3D(int numShapeArgs_, int numCompletionArgs_)
    : numShapeArgs(numShapeArgs_),
      numCompletionArgs(numCompletionArgs_)
{
}

template <typename T, template <typename U> class Descriptor>
void boundaryUtauDensityInterpFunctional3D<T, Descriptor>::processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D *> blocks)
{
    PLB_ASSERT(blocks.size() == 2 + numShapeArgs + numCompletionArgs);
    // numCompletionArgs:0:rhoVelocityNufield,1:wallDistanceY,2:stencilFlag,3:dynamicID,4:utau,5:stressField,6:modifyRhoVelocityField
    BlockLattice3D<T, Descriptor> const *lattice = dynamic_cast<BlockLattice3D<T, Descriptor> const *>(blocks[0]);
    NTensorField3D<T> *rhoVelocityNuField = dynamic_cast<NTensorField3D<T> *>(blocks[2 + numShapeArgs]);
    TensorField3D<T, 3> const *wallDistanceY = dynamic_cast<TensorField3D<T, 3> const *>(blocks[2 + numShapeArgs + 1]);
    ScalarField3D<int> const *stencilFlag = dynamic_cast<ScalarField3D<int> const *>(blocks[2 + numShapeArgs + 2]);
    ScalarField3D<T> *utau = dynamic_cast<ScalarField3D<T> *>(blocks[2 + numShapeArgs + 4]);
    TensorField3D<T, 4> *modifyRhoVelocityField = dynamic_cast<TensorField3D<T, 4> *>(blocks[2 + numShapeArgs + 6]);

    PLB_ASSERT(lattice);

    Dot3D offsetStencil = computeRelativeDisplacement(*lattice, *stencilFlag);

    for (plint iX = domain.x0; iX <= domain.x1; ++iX)
    {
        for (plint iY = domain.y0; iY <= domain.y1; ++iY)
        {
            for (plint iZ = domain.z0; iZ <= domain.z1; ++iZ)
            {
                int flag = stencilFlag->get(iX + offsetStencil.x, iY + offsetStencil.y, iZ + offsetStencil.z);
                // boundary nodes
                if (1 == flag)
                {
                    const Dot3D boundaryNode(iX, iY, iZ);
                    // interpolateUtauDensity(boundaryNode,*lattice,*stencilFlag,*wallDistanceY,*utau, *rhoVelocityNuField);
                    interpolateUtauDensity(boundaryNode, *lattice, *stencilFlag, *wallDistanceY, *utau, *rhoVelocityNuField, *modifyRhoVelocityField);
                }
            }
        }
    }
}

template <typename T, template <typename U> class Descriptor>
void boundaryUtauDensityInterpFunctional3D<T, Descriptor>::interpolateUtauDensity(const Dot3D &boundaryNode, BlockLattice3D<T, Descriptor> const &lattice, ScalarField3D<int> const &stencilFlag, TensorField3D<T, 3> const &wallDistanceY, ScalarField3D<T> &utau, NTensorField3D<T> const &rhoVelocityNuField, TensorField3D<T, 4> &modifyRhoVelocityField)
{

    typedef Descriptor<T> D;
    // Get offset location
    Dot3D offsetModify = computeRelativeDisplacement(lattice, modifyRhoVelocityField);
    Dot3D offsetStencil = computeRelativeDisplacement(lattice, stencilFlag);
    Dot3D offsetWallDistanceY = computeRelativeDisplacement(lattice, wallDistanceY);
    Dot3D offsetUtau = computeRelativeDisplacement(lattice, utau);
    Dot3D offsetrhoVelocityNu = computeRelativeDisplacement(lattice, rhoVelocityNuField);
    // Test boundary node for a special stencil node
    Dot3D absOff = lattice.getLocation();
    Dot3D globalBoundary(boundaryNode.x + absOff.x, boundaryNode.y + absOff.y, boundaryNode.z + absOff.z);
    // Step0:Get reference point of density, refRhoLocation
    const T refHight = 2.5;
    Array<T, 3> refRhoLocation(0.0, 0.0, 0.0);
    Array<T, 3> const wallDistanceYNormal = wallDistanceY.get(boundaryNode.x + offsetWallDistanceY.x, boundaryNode.y + offsetWallDistanceY.y, boundaryNode.z + offsetWallDistanceY.z);
    T distance = std::sqrt(wallDistanceYNormal[0] * wallDistanceYNormal[0] + wallDistanceYNormal[1] * wallDistanceYNormal[1] + wallDistanceYNormal[2] * wallDistanceYNormal[2]);
    Array<T, 3> wallNormal;
    wallNormal[0] = wallDistanceYNormal[0] / (1.0e-12 + distance);
    wallNormal[1] = wallDistanceYNormal[1] / (1.0e-12 + distance);
    wallNormal[2] = wallDistanceYNormal[2] / (1.0e-12 + distance);
    T bounaryRefDistance = refHight - distance;
    refRhoLocation[0] = boundaryNode.x + bounaryRefDistance * wallNormal[0];
    refRhoLocation[1] = boundaryNode.y + bounaryRefDistance * wallNormal[1];
    refRhoLocation[2] = boundaryNode.z + bounaryRefDistance * wallNormal[2];

    // Step1:Get donor nodes location, utauDonor, densityDonor
    // Step2:Compute weight
    // Step3:Compute utauDonor*weight and densityDonor*weight
    // Step5:Update sumUtau, sumDensity, sumWeight
    // Step6:Get the final interpolation value
    // Initialize as zero
    T sumWeightUtau = 0.0;
    T sumWeightUtauXUtau = 0.0;
    T sumWeightRho = 0.0;
    T sumWeightU = 0.0;
    Array<T, 3> sumWeightVelocity(0.0, 0.0, 0.0);
    T sumWeightRhoXRho = 0.0;
    int counter = 0;
    // layer one
    for (int qID = 0; qID < D::q; qID++)
    {
        const Dot3D neighbor(boundaryNode.x + D::c[qID][0], boundaryNode.y + D::c[qID][1], boundaryNode.z + D::c[qID][2]);
        int flag = stencilFlag.get(neighbor.x + offsetStencil.x, neighbor.y + offsetStencil.y, neighbor.z + offsetStencil.z);

        // it is a donor node on the stencil.
        if (2 == flag)
        {
            // compute weightUtau and weightUtau*utau
            Array<T, 3> const wallDistanceYNormal = wallDistanceY.get(neighbor.x + offsetWallDistanceY.x, neighbor.y + offsetWallDistanceY.y, neighbor.z + offsetWallDistanceY.z);
            T const utauDonor = utau.get(neighbor.x + offsetUtau.x, neighbor.y + offsetUtau.y, neighbor.z + offsetUtau.z);
            updateSumWeightAndUtauDonor(boundaryNode, neighbor, wallDistanceYNormal, utauDonor, sumWeightUtau, sumWeightUtauXUtau);
            // compute weightRho and weightRho*rho
            T const *rhoVelocityNu = rhoVelocityNuField.get(neighbor.x + offsetrhoVelocityNu.x, neighbor.y + offsetrhoVelocityNu.y, neighbor.z + offsetrhoVelocityNu.z);
            T const &rhoDonor = rhoVelocityNu[0];
            Array<T, 3> velocity;
            velocity.from_cArray(rhoVelocityNu + 1);
            updateSumWeightAndRhoDonor(refRhoLocation, neighbor, wallDistanceYNormal, rhoDonor, velocity, sumWeightRho, sumWeightU, sumWeightVelocity, sumWeightRhoXRho);
            counter++;
        }
    }
    // layer two
    for (int qID = 0; qID < D::q; qID++)
    {
        const Dot3D neighbor(boundaryNode.x + 2 * D::c[qID][0], boundaryNode.y + 2 * D::c[qID][1], boundaryNode.z + 2 * D::c[qID][2]);
        int flag = stencilFlag.get(neighbor.x + offsetStencil.x, neighbor.y + offsetStencil.y, neighbor.z + offsetStencil.z);

        // it is a donor node.
        if (2 == flag)
        {
            Array<T, 3> const wallDistanceYNormal = wallDistanceY.get(neighbor.x + offsetWallDistanceY.x, neighbor.y + offsetWallDistanceY.y, neighbor.z + offsetWallDistanceY.z);
            T const utauDonor = utau.get(neighbor.x + offsetUtau.x, neighbor.y + offsetUtau.y, neighbor.z + offsetUtau.z);
            // TODO: recover this
            updateSumWeightAndUtauDonor(boundaryNode, neighbor, wallDistanceYNormal, utauDonor, sumWeightUtau, sumWeightUtauXUtau);
            // compute weightRho and weightRho*rho
            T const *rhoVelocityNu = rhoVelocityNuField.get(neighbor.x + offsetrhoVelocityNu.x, neighbor.y + offsetrhoVelocityNu.y, neighbor.z + offsetrhoVelocityNu.z);
            T const &rhoDonor = rhoVelocityNu[0];
            Array<T, 3> velocity;
            velocity.from_cArray(rhoVelocityNu + 1);
            // updateSumWeightAndRhoDonor(refRhoLocation, neighbor, rhoDonor, sumWeightRho, sumWeightRhoXRho);
            updateSumWeightAndRhoDonor(refRhoLocation, neighbor, wallDistanceYNormal, rhoDonor, velocity, sumWeightRho, sumWeightU, sumWeightVelocity, sumWeightRhoXRho);
            counter++;
        }
    }

    // compute final utau on boundary node (neighbor fluid node)
    if (0 == counter)
    { // TODO: no interp is made.
        utau.get(boundaryNode.x + offsetUtau.x, boundaryNode.y + offsetUtau.y, boundaryNode.z + offsetUtau.z) = 0.0;
    }
    else
    {
        utau.get(boundaryNode.x + offsetUtau.x, boundaryNode.y + offsetUtau.y, boundaryNode.z + offsetUtau.z) = sumWeightUtauXUtau / (sumWeightUtau + 1.0e-12);
    }
    // T * rhoVelocityNu = rhoVelocityNuField.get(boundaryNode.x+offsetrhoVelocityNu.x,boundaryNode.y+offsetrhoVelocityNu.y,boundaryNode.z+offsetrhoVelocityNu.z);
    // T & rhoBoundary = rhoVelocityNu[0];
    Array<T, 4> &modifyRhoVelocity = modifyRhoVelocityField.get(boundaryNode.x + offsetModify.x, boundaryNode.y + offsetModify.y, boundaryNode.z + offsetModify.z);
    // T & rhoBoundary = modifyRhoVelocity[0];
    // rhoBoundary = sumWeightRhoXRho/(sumWeightRho+1.0e-12);
    modifyRhoVelocity[0] = sumWeightRhoXRho / (sumWeightRho + 1.0e-12);
    modifyRhoVelocity[1] = sumWeightVelocity[0] / (sumWeightU + 1.0e-12);
    modifyRhoVelocity[2] = sumWeightVelocity[1] / (sumWeightU + 1.0e-12);
    modifyRhoVelocity[3] = sumWeightVelocity[2] / (sumWeightU + 1.0e-12);
}

template <typename T, template <typename U> class Descriptor>
void boundaryUtauDensityInterpFunctional3D<T, Descriptor>::updateSumWeightAndUtauDonor(const Dot3D &boundaryNode, const Dot3D &neighbor, Array<T, 3> const &wallDistanceYNormal, T const &utauDonor, T &sumWeightUtau, T &sumWeightUtauXUtau)
{
    Array<T, 3> donorBoundary(neighbor.x - boundaryNode.x, neighbor.y - boundaryNode.y, neighbor.z - boundaryNode.z);
    T boundaryDonorDistance2 = donorBoundary[0] * donorBoundary[0] + donorBoundary[1] * donorBoundary[1] + donorBoundary[2] * donorBoundary[2];
    T distance = std::sqrt(wallDistanceYNormal[0] * wallDistanceYNormal[0] + wallDistanceYNormal[1] * wallDistanceYNormal[1] + wallDistanceYNormal[2] * wallDistanceYNormal[2]);
    Array<T, 3> wallNormal;
    wallNormal[0] = wallDistanceYNormal[0] / (1.0e-12 + distance);
    wallNormal[1] = wallDistanceYNormal[1] / (1.0e-12 + distance);
    wallNormal[2] = wallDistanceYNormal[2] / (1.0e-12 + distance);
    T donorBoundaryNormal = donorBoundary[0] * wallNormal[0] + donorBoundary[1] * wallNormal[1] + donorBoundary[2] * wallNormal[2];
    T boundaryDonorDistanceTangential2 = boundaryDonorDistance2 - donorBoundaryNormal * donorBoundaryNormal;
    T weightUtau = 1.0 / boundaryDonorDistanceTangential2;

    sumWeightUtau += weightUtau;
    sumWeightUtauXUtau += weightUtau * utauDonor;
}

template <typename T, template <typename U> class Descriptor>
void boundaryUtauDensityInterpFunctional3D<T, Descriptor>::interpolateUtauDensity(const Dot3D &boundaryNode, BlockLattice3D<T, Descriptor> const &lattice, ScalarField3D<int> const &stencilFlag, TensorField3D<T, 3> const &wallDistanceY, ScalarField3D<T> &utau, NTensorField3D<T> &rhoVelocityNuField)
{
    /*
        typedef Descriptor<T> D;
        //Get offset location
        Dot3D offsetrhoVelocityNu = computeRelativeDisplacement(lattice, rhoVelocityNuField);
        Dot3D offsetStencil = computeRelativeDisplacement(lattice, stencilFlag);
        Dot3D offsetWallDistanceY = computeRelativeDisplacement(lattice, wallDistanceY);
        Dot3D offsetUtau = computeRelativeDisplacement(lattice, utau);
        //Step0:Get reference point of density, refRhoLocation
        const T refHight = 2.5;
        Array<T,3> refRhoLocation(0.0, 0.0, 0.0);
        Array<T,3> const wallDistanceYNormal =  wallDistanceY.get(boundaryNode.x+offsetWallDistanceY.x,boundaryNode.y+offsetWallDistanceY.y,boundaryNode.z+offsetWallDistanceY.z);
        T distance = std::sqrt(wallDistanceYNormal[0]*wallDistanceYNormal[0]+wallDistanceYNormal[1]*wallDistanceYNormal[1]+wallDistanceYNormal[2]*wallDistanceYNormal[2]);
        Array<T,3> wallNormal;
        wallNormal[0] = wallDistanceYNormal[0]/(1.0e-12+distance);
        wallNormal[1] = wallDistanceYNormal[1]/(1.0e-12+distance);
        wallNormal[2] = wallDistanceYNormal[2]/(1.0e-12+distance);
        T bounaryRefDistance = refHight - distance;
        refRhoLocation[0] = boundaryNode.x + bounaryRefDistance*wallNormal[0];
        refRhoLocation[1] = boundaryNode.y + bounaryRefDistance*wallNormal[1];
        refRhoLocation[2] = boundaryNode.z + bounaryRefDistance*wallNormal[2];

        //Step1:Get donor nodes location, utauDonor, densityDonor
        //Step2:Compute weight
        //Step3:Compute utauDonor*weight and densityDonor*weight
        //Step5:Update sumUtau, sumDensity, sumWeight
        //Step6:Get the final interpolation value
        //Initialize as zero
        T sumWeightUtau = 0.0;
        T sumWeightUtauXUtau = 0.0;
        T sumWeightRho = 0.0;
        T sumWeightRhoXRho = 0.0;
        //layer one
        for (int qID = 0; qID < D::q; qID++){
                const Dot3D neighbor(boundaryNode.x+D::c[qID][0], boundaryNode.y+D::c[qID][1], boundaryNode.z+D::c[qID][2]);
            int flag = stencilFlag.get(neighbor.x+offsetStencil.x,neighbor.y+offsetStencil.y,neighbor.z+offsetStencil.z);

                //it is a donor node on the stencil.
            if (2 == flag) {
            //compute weightUtau and weightUtau*utau
                Array<T,3> const wallDistanceYNormal =  wallDistanceY.get(neighbor.x+offsetWallDistanceY.x,neighbor.y+offsetWallDistanceY.y,neighbor.z+offsetWallDistanceY.z);
                    T const utauDonor = utau.get(neighbor.x+offsetUtau.x,neighbor.y+offsetUtau.y,neighbor.z+offsetUtau.z);
                updateSumWeightAndUtauDonor(boundaryNode, neighbor, wallDistanceYNormal, utauDonor, sumWeightUtau, sumWeightUtauXUtau);
            //compute weightRho and weightRho*rho
            T const * rhoVelocityNu = rhoVelocityNuField.get(neighbor.x+offsetrhoVelocityNu.x,neighbor.y+offsetrhoVelocityNu.y,neighbor.z+offsetrhoVelocityNu.z);
            T const & rhoDonor = rhoVelocityNu[0];
            updateSumWeightAndRhoDonor(refRhoLocation, neighbor, rhoDonor, sumWeightRho, sumWeightRhoXRho);
            updateSumWeightAndRhoDonor(refRhoLocation, neighbor, rhoDonor, velocity, sumWeightRho, sumWeightVelocity, sumWeightRhoXRho);


            }
        }

        //layer two
        for (int qID = 0; qID < D::q; qID++){
                const Dot3D neighbor(boundaryNode.x+2*D::c[qID][0], boundaryNode.y+2*D::c[qID][1], boundaryNode.z+2*D::c[qID][2]);
            int flag = stencilFlag.get(neighbor.x+offsetStencil.x,neighbor.y+offsetStencil.y,neighbor.z+offsetStencil.z);

                //it is a donor node.
            if (2 == flag) {
                Array<T,3> const wallDistanceYNormal =  wallDistanceY.get(neighbor.x+offsetWallDistanceY.x,neighbor.y+offsetWallDistanceY.y,neighbor.z+offsetWallDistanceY.z);
                    T const utauDonor = utau.get(neighbor.x+offsetUtau.x,neighbor.y+offsetUtau.y,neighbor.z+offsetUtau.z);
                //updateSumWeightAndUtauDonor(boundaryNode, neighbor, wallDistanceYNormal, utauDonor, sumWeightUtau, sumWeightUtauXUtau);
            //compute weightRho and weightRho*rho
            T const * rhoVelocityNu = rhoVelocityNuField.get(neighbor.x+offsetrhoVelocityNu.x,neighbor.y+offsetrhoVelocityNu.y,neighbor.z+offsetrhoVelocityNu.z);
            T const & rhoDonor = rhoVelocityNu[0];
            updateSumWeightAndRhoDonor(refRhoLocation, neighbor, rhoDonor, sumWeightRho, sumWeightRhoXRho);
            }
        }

        //compute final utau on boundary node (neighbor fluid node)
        utau.get(boundaryNode.x+offsetUtau.x,boundaryNode.y+offsetUtau.y,boundaryNode.z+offsetUtau.z) = sumWeightUtauXUtau/(sumWeightUtau+1.0e-12);

        T * rhoVelocityNu = rhoVelocityNuField.get(boundaryNode.x+offsetrhoVelocityNu.x,boundaryNode.y+offsetrhoVelocityNu.y,boundaryNode.z+offsetrhoVelocityNu.z);
        T & rhoBoundary = rhoVelocityNu[0];
        rhoBoundary = sumWeightRhoXRho/(sumWeightRho+1.0e-12);
    */
}

template <typename T, template <typename U> class Descriptor>
void boundaryUtauDensityInterpFunctional3D<T, Descriptor>::updateSumWeightAndRhoDonor(Array<T, 3> const &boundaryNode, const Dot3D &neighbor, Array<T, 3> const &wallDistanceYNormal, T const &rhoDonor, Array<T, 3> const &velocity, T &sumWeightRho, T &sumWeightU, Array<T, 3> &sumWeightVelocity, T &sumWeightRhoXRho)
{
    Array<T, 3> donorBoundary(neighbor.x - boundaryNode[0], neighbor.y - boundaryNode[1], neighbor.z - boundaryNode[2]);
    T boundaryDonorDistance2 = donorBoundary[0] * donorBoundary[0] + donorBoundary[1] * donorBoundary[1] + donorBoundary[2] * donorBoundary[2];
    //        T weightRho = 1.0/boundaryDonorDistance2;
    T distance = std::sqrt(wallDistanceYNormal[0] * wallDistanceYNormal[0] + wallDistanceYNormal[1] * wallDistanceYNormal[1] + wallDistanceYNormal[2] * wallDistanceYNormal[2]);
    Array<T, 3> wallNormal;
    wallNormal[0] = wallDistanceYNormal[0] / (1.0e-12 + distance);
    wallNormal[1] = wallDistanceYNormal[1] / (1.0e-12 + distance);
    wallNormal[2] = wallDistanceYNormal[2] / (1.0e-12 + distance);
    T donorBoundaryNormal = donorBoundary[0] * wallNormal[0] + donorBoundary[1] * wallNormal[1] + donorBoundary[2] * wallNormal[2];
    T boundaryDonorDistanceTangential2 = boundaryDonorDistance2 - donorBoundaryNormal * donorBoundaryNormal;
    T weightRho = 1.0 / boundaryDonorDistanceTangential2;
    T weightU = 1.0 / (donorBoundaryNormal * donorBoundaryNormal);
    sumWeightRho += weightRho;
    sumWeightU += weightU;
    sumWeightRhoXRho += weightRho * rhoDonor;
    sumWeightVelocity[0] += weightU * velocity[0];
    sumWeightVelocity[1] += weightU * velocity[1];
    sumWeightVelocity[2] += weightU * velocity[2];
}

template <typename T, template <typename U> class Descriptor>
boundaryUtauDensityInterpFunctional3D<T, Descriptor> *boundaryUtauDensityInterpFunctional3D<T, Descriptor>::clone() const
{
    return new boundaryUtauDensityInterpFunctional3D<T, Descriptor>(*this);
}

template <typename T, template <typename U> class Descriptor>
void boundaryUtauDensityInterpFunctional3D<T, Descriptor>::getTypeOfModification(std::vector<modif::ModifT> &modified) const
{
    for (int item = 0; item < 2 + numShapeArgs + numCompletionArgs; item++)
    {
        modified[item] = modif::nothing;
    }
    // modifyRhoVelocityNu is updated.
    // modified[2+numShapeArgs] = modif::staticVariables;
    modified[2 + numShapeArgs + 6] = modif::staticVariables;
    // utau is updated.
    modified[2 + numShapeArgs + 4] = modif::staticVariables;
}

template <typename T, template <typename U> class Descriptor>
BlockDomain::DomainT boundaryUtauDensityInterpFunctional3D<T, Descriptor>::appliesTo() const
{
    return BlockDomain::bulk;
}
/////////////////////////////////////////////////////
// data processor for updating velocity on boudnary nodes (neighbor fluid cells) with the help of utau
template <typename T, template <typename U> class Descriptor>
velocityOnBoundaryUpdateFunctional3D<T, Descriptor>::velocityOnBoundaryUpdateFunctional3D(int numShapeArgs_, int numCompletionArgs_)
    : numShapeArgs(numShapeArgs_),
      numCompletionArgs(numCompletionArgs_)
{
}

template <typename T, template <typename U> class Descriptor>
void velocityOnBoundaryUpdateFunctional3D<T, Descriptor>::processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D *> blocks)
{
    PLB_ASSERT(blocks.size() == 2 + numShapeArgs + numCompletionArgs);
    // numCompletionArgs:0:rhoVelocityNufield,1:wallDistanceY,2:stencilFlag,3:dynamicID,4:utau,5:stressField,6:modifyRhoVelocity
    BlockLattice3D<T, Descriptor> *lattice = dynamic_cast<BlockLattice3D<T, Descriptor> *>(blocks[0]);
    NTensorField3D<T> const *rhoVelocityNuField = dynamic_cast<NTensorField3D<T> const *>(blocks[2 + numShapeArgs]);
    TensorField3D<T, 3> const *wallDistanceY = dynamic_cast<TensorField3D<T, 3> const *>(blocks[2 + numShapeArgs + 1]);
    ScalarField3D<int> const *stencilFlag = dynamic_cast<ScalarField3D<int> const *>(blocks[2 + numShapeArgs + 2]);
    ScalarField3D<T> const *utau = dynamic_cast<ScalarField3D<T> const *>(blocks[2 + numShapeArgs + 4]);
    TensorField3D<T, 4> *modifyRhoVelocityField = dynamic_cast<TensorField3D<T, 4> *>(blocks[2 + numShapeArgs + 6]);

    PLB_ASSERT(lattice);
    typedef Descriptor<T> D;

    Dot3D offsetrhoVelocityNu = computeRelativeDisplacement(*lattice, *rhoVelocityNuField);
    Dot3D offsetStencil = computeRelativeDisplacement(*lattice, *stencilFlag);
    Dot3D offsetWallDistanceY = computeRelativeDisplacement(*lattice, *wallDistanceY);
    Dot3D offsetUtau = computeRelativeDisplacement(*lattice, *utau);
    Dot3D offsetModify = computeRelativeDisplacement(*lattice, *modifyRhoVelocityField);

    int counter = 0;
    T ypavg = 0.0;
    T utavg = 0.0;
    T uavg = 0.0;
    T upavg = 0.0;

    for (plint iX = domain.x0; iX <= domain.x1; ++iX)
    {
        for (plint iY = domain.y0; iY <= domain.y1; ++iY)
        {
            for (plint iZ = domain.z0; iZ <= domain.z1; ++iZ)
            {
                int flag = stencilFlag->get(iX + offsetStencil.x, iY + offsetStencil.y, iZ + offsetStencil.z);
                // boundary nodes (neighbor fluid cells)
                if (1 == flag)
                {
                    T const &utauBoundary = utau->get(iX + offsetUtau.x, iY + offsetUtau.y, iZ + offsetUtau.z);
                    // if(utauBoundary > 1.0e-10) {
                    Array<T, 3> const &distanceNormal = wallDistanceY->get(iX + offsetWallDistanceY.x, iY + offsetWallDistanceY.y, iZ + offsetWallDistanceY.z);
                    Array<T, 4> &modifyRhoVelocity = modifyRhoVelocityField->get(iX + offsetModify.x, iY + offsetModify.y, iZ + offsetModify.z);
                    T const *rhoVelocityNu = rhoVelocityNuField->get(iX + offsetrhoVelocityNu.x, iY + offsetrhoVelocityNu.y, iZ + offsetrhoVelocityNu.z);
                    Array<T, 3> velocity;
                    // velocity.from_cArray(rhoVelocityNu+1);
                    velocity[0] = modifyRhoVelocity[1];
                    velocity[1] = modifyRhoVelocity[2];
                    velocity[2] = modifyRhoVelocity[3];
                    T const nuBoundary = rhoVelocityNu[4];
                    T const rho = modifyRhoVelocity[0];

                    // compute wall normal
                    Array<T, 3> wallNormal;
                    T distance = std::sqrt(distanceNormal[0] * distanceNormal[0] + distanceNormal[1] * distanceNormal[1] + distanceNormal[2] * distanceNormal[2]);
                    wallNormal[0] = distanceNormal[0] / (1.0e-12 + distance);
                    wallNormal[1] = distanceNormal[1] / (1.0e-12 + distance);
                    wallNormal[2] = distanceNormal[2] / (1.0e-12 + distance);

                    // compute normal component of velocity
                    T velocityNormal = velocity[0] * wallNormal[0] + velocity[1] * wallNormal[1] + velocity[2] * wallNormal[2];
                    // compute tangential of velocity
                    Array<T, 3> vtNorm;
                    velocity[0] = velocity[0] - velocityNormal * wallNormal[0];
                    velocity[1] = velocity[1] - velocityNormal * wallNormal[1];
                    velocity[2] = velocity[2] - velocityNormal * wallNormal[2];
                    T velocityTangential = std::sqrt(velocity[0] * velocity[0] + velocity[1] * velocity[1] + velocity[2] * velocity[2]);
                    vtNorm[0] = velocity[0] / velocityTangential;
                    vtNorm[1] = velocity[1] / velocityTangential;
                    vtNorm[2] = velocity[2] / velocityTangential;
                    int qmin = 0;
                    int qmax = 0;
                    T mindis = 0.0;
                    T maxdis = 0.0;
                    T dpds = 0.0;
                    for (int qID = 1; qID < D::q; qID++)
                    {
                        const Dot3D neighbor(iX + D::c[qID][0], iY + D::c[qID][1], iZ + D::c[qID][2]);
                        int flag_n = stencilFlag->get(neighbor.x + offsetStencil.x, neighbor.y + offsetStencil.y, neighbor.z + offsetStencil.z);
                        if (2 == flag_n || 1 == flag_n)
                        {
                            T tmp = (D::c[qID][0] * vtNorm[0] + D::c[qID][1] * vtNorm[1] + D::c[qID][2] * vtNorm[2]) / std::sqrt(D::c[qID][0] * D::c[qID][0] + D::c[qID][1] * D::c[qID][1] + D::c[qID][2] * D::c[qID][2]);
                            if (tmp < mindis)
                            {
                                mindis = tmp;
                                qmin = qID;
                            }
                            if (tmp > maxdis)
                            {
                                maxdis = tmp;
                                qmax = qID;
                            }
                        }
                    }
                    if (qmin > 0 && qmax > 0)
                    {
                        const Dot3D neighbor(iX + D::c[qmin][0], iY + D::c[qmin][1], iZ + D::c[qmin][2]);
                        const Dot3D neighbor2(iX + D::c[qmax][0], iY + D::c[qmax][1], iZ + D::c[qmax][2]);
                        // Array<T,4>& rhoVelocityNu_n = modifyRhoVelocityField->get(neighbor.x+offsetModify.x,neighbor.y+offsetModify.y,neighbor.z+offsetModify.z);
                        T const *rhoVelocityNu_n = rhoVelocityNuField->get(neighbor.x + offsetrhoVelocityNu.x, neighbor.y + offsetrhoVelocityNu.y, neighbor.z + offsetrhoVelocityNu.z);
                        // Array<T,4>& rhoVelocityNu_n2 = modifyRhoVelocityField->get(neighbor2.x+offsetModify.x,neighbor2.y+offsetModify.y,neighbor2.z+offsetModify.z);
                        T const *rhoVelocityNu_n2 = rhoVelocityNuField->get(neighbor2.x + offsetrhoVelocityNu.x, neighbor2.y + offsetrhoVelocityNu.y, neighbor2.z + offsetrhoVelocityNu.z);
                        dpds = D::cs2 * (rhoVelocityNu_n2[0] - rhoVelocityNu_n[0]) / (std::fabs(mindis * std::sqrt(D::c[qmin][0] * D::c[qmin][0] + D::c[qmin][1] * D::c[qmin][1] + D::c[qmin][2] * D::c[qmin][2])) + std::fabs(maxdis * std::sqrt(D::c[qmax][0] * D::c[qmax][0] + D::c[qmax][1] * D::c[qmax][1] + D::c[qmax][2] * D::c[qmax][2])));
                    }
                    // rhoVelocityNu[1] = wallTangential[0] * utauBoundary;
                    // rhoVelocityNu[2] = wallTangential[1] * utauBoundary;
                    // rhoVelocityNu[3] = wallTangential[2] * utauBoundary;
                    T yPlus = distance * utauBoundary / nuBoundary;
                    T pPlus = nuBoundary / rho / std::pow(utauBoundary, 3.0) * dpds;
                    T kPlus = pPlus;
                    bool isforward = vtNorm[0] >= 0? true: false;
                    wallFunctionMethod = WALL_FUNCTION;
                    T U_t; // tangential velocity
                    T uPlus = 0.0;
                    T D = velocityTangential - 7.5789 * std::sqrt(distance * dpds / rho) + 1.4489 * std::pow(nuBoundary * dpds / rho, 1.0 / 3.0) * std::log(191.1799 * std::pow(distance, 3.0) * dpds / rho / nuBoundary / nuBoundary);
                    // T D = 8.3*std::pow(utauBoundary,8.0/7.0)*std::pow(distance/nuBoundary, 1.0/7.0);
                    switch (wallFunctionMethod)
                    {
                    case 0:
                        yPlus = 1.0 * utauBoundary / nuBoundary;
                        uPlus = uPlusOrgLog(yPlus);
                        U_t = uPlus * utauBoundary * distance;
                        break;
                    case 1:
                        uPlus = uPlusMusker(yPlus);
                        U_t = uPlus * utauBoundary;
                        break;
                    case 2:
                        uPlus = uPlusAPG(yPlus);

                        // uPlus = yPlus;
                        // if(yPlus > 11.81) uPlus = 8.3*std::pow(yPlus, 1.0/7.0);

                        if (dpds > 0.0 && D >= 0 && yPlus > 11.81)
                        {
                            uPlus = 8.3 * std::pow(yPlus, 1.0 / 7.0) + 7.5789 * std::sqrt(yPlus * pPlus) - 1.4489 * std::pow(pPlus, 1.0 / 3.0) * std::log(191.1799 * std::pow(yPlus, 3.0) * pPlus);
                        }

                        U_t = uPlus * utauBoundary;
                        break;
                    case 3:
                        uPlus = uPlusSA(yPlus);
                        U_t = uPlus * utauBoundary;
                        break;
                    case 4:
                        // if (yPlus * kPlus > 1) {
                        //     uPlus = uPlusAfzal(yPlus, kPlus, isforward);
                        // } else {
                        //     uPlus = uPlusSA(yPlus);
                        // }
                        uPlus = uPlusSA(yPlus);
                        U_t = uPlus * utauBoundary;
                        break;
                    default:
                        U_t = uPlus * utauBoundary;
                        break;
                    }
                    ypavg += yPlus;
                    uavg += U_t;
                    upavg += distance;
                    utavg += utauBoundary;
                    counter++;
                    modifyRhoVelocity[1] = vtNorm[0] * U_t;
                    modifyRhoVelocity[2] = vtNorm[1] * U_t;
                    modifyRhoVelocity[3] = vtNorm[2] * U_t;
                    /*
                        } else {
                                    Array<T,4> & modifyRhoVelocity = modifyRhoVelocityField->get(iX+offsetModify.x,iY+offsetModify.y,iZ+offsetModify.z);
                                    modifyRhoVelocity[0] = 1.0;
                                    modifyRhoVelocity[1] = 0.0;
                                    modifyRhoVelocity[2] = 0.0;
                                    modifyRhoVelocity[3] = 0.0;
                        }
                    */
                }
            }
        }
    }
    // if(counter>0) {std::cout<<"yplus avg = "<<ypavg/counter<<" ; utangential avg = "<<utavg/counter <<std::endl;}
    // if(counter>0) {std::cout<<"uplus avg = "<<uavg/counter<<" ; distance avg = "<<upavg/counter <<std::endl;}
}

template <typename T, template <typename U> class Descriptor>
T velocityOnBoundaryUpdateFunctional3D<T, Descriptor>::uPlusOrgLog(const T yPlus)
{
    const T KK = 0.41;
    const T BB = 5.0;
    const T uPlus = 1.0 / KK * std::log(yPlus) + BB;
    return uPlus;
}

template <typename T, template <typename U> class Descriptor>
T velocityOnBoundaryUpdateFunctional3D<T, Descriptor>::uPlusMusker(const T yPlus)
{

    const T fyAtan = (2.0 * yPlus - 8.15) / 16.7;
    const T fyLogTop = std::pow(yPlus + 10.6, 9.6);
    const T fyLogDown = std::pow(yPlus * yPlus - 8.15 * yPlus + 86.0, 2.0);
    const T fyLog = fyLogTop / fyLogDown;
    const T uPlus = 5.424 * std::atan(fyAtan) + 0.434 * std::log(fyLog) - 3.507;
}

template <typename T, template <typename U> class Descriptor>
T velocityOnBoundaryUpdateFunctional3D<T, Descriptor>::uPlusAPG(const T yPlus)
{
    const T B = 5.0333908790505579;
    const T a1 = 8.148221580024245;
    const T a2 = -6.9287093849022945;
    const T b1 = 7.4600876082527945;
    const T b2 = 7.468145790401841;
    const T c1 = 2.5496773539754747;
    const T c2 = 1.3301651588535228;
    const T c3 = 3.599459109332379;
    const T c4 = 3.6397531868684494;
    T uPlus = B + c1 * std::log((yPlus + a1) * (yPlus + a1) + b1 * b1) - c2 * std::log((yPlus + a2) * (yPlus + a2) + b2 * b2) - c3 * std::atan2(b1, (yPlus + a1)) - c4 * std::atan2(b2, (yPlus + a2));
    return uPlus;
}

template <typename T, template <typename U> class Descriptor>
T velocityOnBoundaryUpdateFunctional3D<T, Descriptor>::uPlusSA(const T yPlus)
{
    const T B = 5.0333908790505579;
    const T a1 = 8.148221580024245;
    const T a2 = -6.9287093849022945;
    const T b1 = 7.4600876082527945;
    const T b2 = 7.468145790401841;
    const T c1 = 2.5496773539754747;
    const T c2 = 1.3301651588535228;
    const T c3 = 3.599459109332379;
    const T c4 = 3.6397531868684494;
    T uPlus = B + c1 * std::log((yPlus + a1) * (yPlus + a1) + b1 * b1) - c2 * std::log((yPlus + a2) * (yPlus + a2) + b2 * b2) - c3 * std::atan2(b1, (yPlus + a1)) - c4 * std::atan2(b2, (yPlus + a2));
    return uPlus;
}
template <typename T, template <typename U> class Descriptor>
velocityOnBoundaryUpdateFunctional3D<T, Descriptor> *velocityOnBoundaryUpdateFunctional3D<T, Descriptor>::clone() const
{
    return new velocityOnBoundaryUpdateFunctional3D<T, Descriptor>(*this);
}

template <typename T, template <typename U> class Descriptor>
void velocityOnBoundaryUpdateFunctional3D<T, Descriptor>::getTypeOfModification(std::vector<modif::ModifT> &modified) const
{
    for (int item = 0; item < 2 + numShapeArgs + numCompletionArgs - 1; item++)
    {
        modified[item] = modif::nothing;
    }
    // rhoVelocityNuField should be updated
    // modified[2+numShapeArgs] = modif::staticVariables;
    modified[2 + numShapeArgs + 6] = modif::staticVariables;
}

template <typename T, template <typename U> class Descriptor>
BlockDomain::DomainT velocityOnBoundaryUpdateFunctional3D<T, Descriptor>::appliesTo() const
{
    return BlockDomain::bulk;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// data processor for computing stress on boundary nodes

template <typename T, template <typename U> class Descriptor>
stressOnBoundaryCompFunctional3D<T, Descriptor>::stressOnBoundaryCompFunctional3D(int numShapeArgs_, int numCompletionArgs_)
    : numShapeArgs(numShapeArgs_),
      numCompletionArgs(numCompletionArgs_)
{
}

template <typename T, template <typename U> class Descriptor>
void stressOnBoundaryCompFunctional3D<T, Descriptor>::processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D *> blocks)
{
    PLB_ASSERT(blocks.size() == 2 + numShapeArgs + numCompletionArgs);
    // numCompletionArgs:0:rhoVelocityNufield,1:wallDistanceY,2:stencilFlag,3:dynamicID,4:utau, 5:stressTensorField,6:modifRhoVelocity
    BlockLattice3D<T, Descriptor> *lattice = dynamic_cast<BlockLattice3D<T, Descriptor> *>(blocks[0]);
    NTensorField3D<T> *rhoVelocityNuField = dynamic_cast<NTensorField3D<T> *>(blocks[2 + numShapeArgs]);
    TensorField3D<T, 3> *wallDistanceY = dynamic_cast<TensorField3D<T, 3> *>(blocks[2 + numShapeArgs + 1]);
    ScalarField3D<int> *stencilFlag = dynamic_cast<ScalarField3D<int> *>(blocks[2 + numShapeArgs + 2]);
    ScalarField3D<T> *utau = dynamic_cast<ScalarField3D<T> *>(blocks[2 + numShapeArgs + 4]);
    TensorField3D<T, 6> *stressField = dynamic_cast<TensorField3D<T, 6> *>(blocks[2 + numShapeArgs + 5]);

    PLB_ASSERT(lattice);

    Dot3D offsetStencil = computeRelativeDisplacement(*lattice, *stencilFlag);
    Dot3D offsetStress = computeRelativeDisplacement(*lattice, *stressField);
    /*
        Dot3D offsetrhoVelocityNu = computeRelativeDisplacement(*lattice, *rhoVelocityNuField);
        Dot3D offsetWallDistanceY = computeRelativeDisplacement(*lattice, *wallDistanceY);
        Dot3D offsetUtau = computeRelativeDisplacement(*lattice, *utau);
        Dot3D offsetStress = computeRelativeDisplacement(*lattice, *stressField);
    */

    Array<T, 3> dUdx; // dU_xdx, dU_ydx, dU_zdx
    Array<T, 3> dUdy; // dU_xdy, dU_ydy, dU_zdy
    Array<T, 3> dUdz; // dU_xdz, dU_ydz, dU_zdz
    T dU_tdx;
    T dU_tdy;
    T dU_tdz;
    Dot3D directionX(1, 0, 0);
    Dot3D directionY(0, 1, 0);
    Dot3D directionZ(0, 0, 1);
    T dU_tdn;
    for (plint iX = domain.x0; iX <= domain.x1; ++iX)
    {
        for (plint iY = domain.y0; iY <= domain.y1; ++iY)
        {
            for (plint iZ = domain.z0; iZ <= domain.z1; ++iZ)
            {
                int flag = stencilFlag->get(iX + offsetStencil.x, iY + offsetStencil.y, iZ + offsetStencil.z);
                // boundary nodes (neighbor fluid cells)
                if (1 == flag)
                {
                    Dot3D boundaryNode(iX, iY, iZ);
                    bool isGap = false;
                    if (computeVelocityDeriveDirection(boundaryNode, directionX, *lattice, *rhoVelocityNuField, *wallDistanceY, dUdx, dU_tdx) < 0)
                        isGap = true;
                    if (computeVelocityDeriveDirection(boundaryNode, directionY, *lattice, *rhoVelocityNuField, *wallDistanceY, dUdy, dU_tdy) < 0)
                        isGap = true;
                    if (computeVelocityDeriveDirection(boundaryNode, directionZ, *lattice, *rhoVelocityNuField, *wallDistanceY, dUdz, dU_tdz) < 0)
                        isGap = true;
                    if (isGap)
                    {
                        Array<T, 6> &stress = stressField->get(boundaryNode.x + offsetStress.x, boundaryNode.y + offsetStress.y, boundaryNode.z + offsetStress.z);
                        for (int i = 0; i < 6; i++)
                            stress[i] = 0.0;
                        return;
                    }
                    computeVelocityTangentialDeriveNormal(boundaryNode, *lattice, *utau, *rhoVelocityNuField, *wallDistanceY, dU_tdn);
                    updateVelocityDeriveByWallFunction(boundaryNode, *lattice, *rhoVelocityNuField, *wallDistanceY, dU_tdx, dU_tdy, dU_tdz, dU_tdn, dUdx, dUdy, dUdz);

                    computeStress(boundaryNode, *lattice, dUdx, dUdy, dUdz, *stressField);
                }
            }
        }
    }
}

template <typename T, template <typename U> class Descriptor>
void stressOnBoundaryCompFunctional3D<T, Descriptor>::computeStress(Dot3D const &boundaryNode, BlockLattice3D<T, Descriptor> const &lattice, Array<T, 3> const &dUdx, Array<T, 3> const &dUdy, Array<T, 3> const &dUdz, TensorField3D<T, 6> &stressField)
{
    typedef SymmetricTensorImpl<T, Descriptor<T>::d> S;
    Dot3D offsetStress = computeRelativeDisplacement(lattice, stressField);
    Array<T, 6> &stress = stressField.get(boundaryNode.x + offsetStress.x, boundaryNode.y + offsetStress.y, boundaryNode.z + offsetStress.z);
    // Compute stress by velocity derivative
    // S::xx, dudx
    stress[S::xx] = dUdx[0];
    // S::yy, dvdy
    stress[S::yy] = dUdy[1];
    // S::zz, dwdz
    stress[S::zz] = dUdz[2];
    // S::xy, 0.5 * (dudy+dvdx)
    stress[S::xy] = 0.5 * (dUdx[1] + dUdy[0]);
    // S::xz, 0.5 * (dudz+dwdx)
    stress[S::xz] = 0.5 * (dUdz[0] + dUdx[2]);
    // S::yz, 0.5 * (dvdz+dwdy)
    stress[S::yz] = 0.5 * (dUdz[1] + dUdy[2]);
}

template <typename T, template <typename U> class Descriptor>
void stressOnBoundaryCompFunctional3D<T, Descriptor>::updateVelocityDeriveByWallFunction(Dot3D const &boundaryNode, BlockLattice3D<T, Descriptor> const &lattice, NTensorField3D<T> const &rhoVelocityNuField, TensorField3D<T, 3> const &wallDistanceY, T const &dU_tdx, T const &dU_tdy, T const &dU_tdz, T const &dU_tdn, Array<T, 3> &dUdx, Array<T, 3> &dUdy, Array<T, 3> &dUdz)
{
    // Compute absolute and relative offset
    Dot3D absOff = lattice.getLocation();
    Dot3D offsetrhoVelocityNu = computeRelativeDisplacement(lattice, rhoVelocityNuField);
    Dot3D offsetWallDistanceY = computeRelativeDisplacement(lattice, wallDistanceY);

    Array<T, 3> const &wallDistanceNormal = wallDistanceY.get(boundaryNode.x + offsetWallDistanceY.x, boundaryNode.y + offsetWallDistanceY.y, boundaryNode.z + offsetWallDistanceY.z);
    T const *rhoVelocityNu = rhoVelocityNuField.get(boundaryNode.x + offsetrhoVelocityNu.x, boundaryNode.y + offsetrhoVelocityNu.y, boundaryNode.z + offsetrhoVelocityNu.z);
    // Get normal and tangential direction on the boundary node
    Array<T, 3> velocity, tangential, normal;
    velocity.from_cArray(rhoVelocityNu + 1);
    // Velocity on boundary nodes has been updated that only tangential part is reserved.
    const T velocityMagnitude = std::sqrt(velocity[0] * velocity[0] + velocity[1] * velocity[1] + velocity[2] * velocity[2]);
    const T distance = std::sqrt(wallDistanceNormal[0] * wallDistanceNormal[0] + wallDistanceNormal[1] * wallDistanceNormal[1] + wallDistanceNormal[2] * wallDistanceNormal[2]);
    for (int dim = 0; dim < 3; dim++)
    {
        tangential[dim] = velocity[dim] / velocityMagnitude;
        normal[dim] = wallDistanceNormal[dim] / distance;
    }
    // Get new dU_tdx, dU_tdy, dU_tdz by dU_tdn
    const T dU_tdxNew = normal[0] * dU_tdn;
    const T dU_tdyNew = normal[1] * dU_tdn;
    const T dU_tdzNew = normal[2] * dU_tdn;
    // Update dUdx containing dudx, dvdx, dwdx
    // dUdx[0] = dUdx[0] + tangential[0] * (dU_tdxNew - dU_tdx);
    // dUdx[1] = dUdx[1] + tangential[1] * (dU_tdxNew - dU_tdx);
    // dUdx[2] = dUdx[2] + tangential[2] * (dU_tdxNew - dU_tdx);
    for (int dim = 0; dim < 3; dim++)
    {
        dUdx[dim] = dUdx[dim] + tangential[dim] * (dU_tdxNew - dU_tdx);
    }
    // Update dUdy containing dudy, dvdy, dwdy
    // dUdy[0] = dUdy[0] + tangential[0] * (dU_tdyNew - dU_tdy);
    // dUdy[1] = dUdy[1] + tangential[1] * (dU_tdyNew - dU_tdy);
    // dUdy[2] = dUdy[2] + tangential[2] * (dU_tdyNew - dU_tdy);
    for (int dim = 0; dim < 3; dim++)
    {
        dUdy[dim] = dUdy[dim] + tangential[dim] * (dU_tdyNew - dU_tdy);
    }
    // Update dUdz containing dudz, dvdz, dwdz
    // dUdz[0] = dUdz[0] + tangential[0] * (dU_tdzNew - dU_tdz);
    // dUdz[1] = dUdz[1] + tangential[1] * (dU_tdzNew - dU_tdz);
    // dUdz[2] = dUdz[2] + tangential[2] * (dU_tdzNew - dU_tdz);
    for (int dim = 0; dim < 3; dim++)
    {
        dUdz[dim] = dUdz[dim] + tangential[dim] * (dU_tdzNew - dU_tdz);
    }
}

template <typename T, template <typename U> class Descriptor>
void stressOnBoundaryCompFunctional3D<T, Descriptor>::computeVelocityTangentialDeriveNormal(Dot3D const &boundaryNode, BlockLattice3D<T, Descriptor> const &lattice, ScalarField3D<T> const &utau, NTensorField3D<T> const &rhoVelocityNuField, TensorField3D<T, 3> const &wallDistanceY, T &dU_tdn)
{
    // Compute absolute and relative offset
    Dot3D absOff = lattice.getLocation();
    Dot3D offsetUtau = computeRelativeDisplacement(lattice, utau);
    Dot3D offsetrhoVelocityNu = computeRelativeDisplacement(lattice, rhoVelocityNuField);
    Dot3D offsetWallDistanceY = computeRelativeDisplacement(lattice, wallDistanceY);
    // Get utau, distance, and nu
    T const &Utau = utau.get(boundaryNode.x + offsetUtau.x, boundaryNode.y + offsetUtau.y, boundaryNode.z + offsetUtau.z);
    Array<T, 3> const &wallDistanceNormal = wallDistanceY.get(boundaryNode.x + offsetWallDistanceY.x, boundaryNode.y + offsetWallDistanceY.y, boundaryNode.z + offsetWallDistanceY.z);
    T const *rhoVelocityNu = rhoVelocityNuField.get(boundaryNode.x + offsetrhoVelocityNu.x, boundaryNode.y + offsetrhoVelocityNu.y, boundaryNode.z + offsetrhoVelocityNu.z);
    T const distance = std::sqrt(wallDistanceNormal[0] * wallDistanceNormal[0] + wallDistanceNormal[1] * wallDistanceNormal[1] + wallDistanceNormal[2] * wallDistanceNormal[2]);
    T const nu = rhoVelocityNu[4];
    // Compute hPlus and nbPlus
    T const hPlus = 1.0 * Utau / nu;
    T const nbPlus = distance * Utau / nu;
    T const coeff0 = -11.0 / 6.0;
    T const coeff1 = 3.0;
    T const coeff2 = -3.0 / 2.0;
    T const coeff3 = 1.0 / 3.0;
    // Compute dU_tdn
    wallFunctionMethod = WALL_FUNCTION;
    switch (wallFunctionMethod)
    {
    case 0:
        dU_tdn = Utau * (coeff0 * uPlusOrgLog(nbPlus) + coeff1 * uPlusOrgLog(nbPlus + hPlus) + coeff2 * uPlusOrgLog(nbPlus + 2.0 * hPlus) + coeff3 * uPlusOrgLog(nbPlus + 3.0 * hPlus));
        break;
    case 1:
        dU_tdn = Utau * (coeff0 * uPlusMusker(nbPlus) + coeff1 * uPlusMusker(nbPlus + hPlus) + coeff2 * uPlusMusker(nbPlus + 2.0 * hPlus) + coeff3 * uPlusMusker(nbPlus + 3.0 * hPlus));
        break;
    case 2:
        // TODO: Replace it with APG
        dU_tdn = Utau * (coeff0 * uPlusAPG(nbPlus) + coeff1 * uPlusAPG(nbPlus + hPlus) + coeff2 * uPlusAPG(nbPlus + 2.0 * hPlus) + coeff3 * uPlusAPG(nbPlus + 3.0 * hPlus));
        /*
        if(nbPlus < 11.81) {
            //dU_tdn = 8.3*Utau*std::pow(Utau/nu, 1.0/7.0)*std::pow(distance, 1.0/7.0-1);
            dU_tdn = Utau*Utau/nu;
        } else {
            //dU_tdn = 8.3*Utau*std::pow(Utau/nu, 1.0/7.0)*std::pow(distance, 1.0/7.0-1);
            dU_tdn = Utau*Utau/nu;
        }
        */
        break;
    case 3:
        dU_tdn = Utau * (coeff0 * uPlusSA(nbPlus) + coeff1 * uPlusSA(nbPlus + hPlus) + coeff2 * uPlusSA(nbPlus + 2.0 * hPlus) + coeff3 * uPlusSA(nbPlus + 3.0 * hPlus));
        break;
    case 4:
        dU_tdn = Utau * (coeff0 * uPlusSA(nbPlus) + coeff1 * uPlusSA(nbPlus + hPlus) + coeff2 * uPlusSA(nbPlus + 2.0 * hPlus) + coeff3 * uPlusSA(nbPlus + 3.0 * hPlus));
        break; 
    default:
        printf("Fatal, wallFunctionMethod = %d\n", wallFunctionMethod);
        exit(1);
    }
}

template <typename T, template <typename U> class Descriptor>
T stressOnBoundaryCompFunctional3D<T, Descriptor>::uPlusOrgLog(const T yPlus)
{
    const T KK = 0.41;
    const T BB = 5.0;
    const T uPlus = 1.0 / KK * std::log(yPlus) + BB;
    return uPlus;
}

template <typename T, template <typename U> class Descriptor>
T stressOnBoundaryCompFunctional3D<T, Descriptor>::uPlusMusker(const T yPlus)
{
    const T fyAtan = (2.0 * yPlus - 8.15) / 16.7;
    const T fyLogTop = std::pow(yPlus + 10.6, 9.6);
    const T fyLogDown = std::pow(yPlus * yPlus - 8.15 * yPlus + 86.0, 2.0);
    const T fyLog = fyLogTop / fyLogDown;
    const T uPlus = 5.424 * std::atan(fyAtan) + 0.434 * std::log(fyLog) - 3.507;
    return uPlus;
}

template <typename T, template <typename U> class Descriptor>
T stressOnBoundaryCompFunctional3D<T, Descriptor>::uPlusAPG(const T yPlus)
{
    const T B = 5.0333908790505579;
    const T a1 = 8.148221580024245;
    const T a2 = -6.9287093849022945;
    const T b1 = 7.4600876082527945;
    const T b2 = 7.468145790401841;
    const T c1 = 2.5496773539754747;
    const T c2 = 1.3301651588535228;
    const T c3 = 3.599459109332379;
    const T c4 = 3.6397531868684494;
    T uPlus = B + c1 * std::log((yPlus + a1) * (yPlus + a1) + b1 * b1) - c2 * std::log((yPlus + a2) * (yPlus + a2) + b2 * b2) - c3 * std::atan2(b1, (yPlus + a1)) - c4 * std::atan2(b2, (yPlus + a2));
    return uPlus;
}

template <typename T, template <typename U> class Descriptor>
T stressOnBoundaryCompFunctional3D<T, Descriptor>::uPlusSA(const T yPlus)
{
    const T B = 5.0333908790505579;
    const T a1 = 8.148221580024245;
    const T a2 = -6.9287093849022945;
    const T b1 = 7.4600876082527945;
    const T b2 = 7.468145790401841;
    const T c1 = 2.5496773539754747;
    const T c2 = 1.3301651588535228;
    const T c3 = 3.599459109332379;
    const T c4 = 3.6397531868684494;
    T uPlus = B + c1 * std::log((yPlus + a1) * (yPlus + a1) + b1 * b1) - c2 * std::log((yPlus + a2) * (yPlus + a2) + b2 * b2) - c3 * std::atan2(b1, (yPlus + a1)) - c4 * std::atan2(b2, (yPlus + a2));
    return uPlus;
}

template <typename T, template <typename U> class Descriptor>
T velocityOnBoundaryUpdateFunctional3D<T, Descriptor>::uPlusAfzal(const T yPlus, const T kPlus, bool isforward)
{
    const T A = 2 / 0.41;
    const T C = 10;
    T B = A * (1 - std::log(2));
    T kyplus = yPlus * kPlus;
    T kyplusMinus1 = kyplus - 1;
    T uPlus;

    if (isforward) {
        // if (kPlus > 1) {
        //     uPlus = A * std::pow(kyplus, 0.5) + std::pow(kPlus, 1.0 / 3.0) * C + A / 2 * (-1 * std::pow(kyplus, -0.5) + 1.0 / 12.0 * std::pow(kyplus, -1.5) - 1.0 / 40.0 * std::pow(kyplus, -2.5));
        // } else {
        //     uPlus = A / 2 * (std::log(yPlus) + 2 * B / A + 0.5 * kyplus - 1.0 / 16 * std::pow(kyplus, 2) + 1.0 / 48 * std::pow(kyplus, 3));
        // }
        uPlus = A / 2 * (std::log(yPlus) + 2 * B / A + 0.5 * kyplus - 1.0 / 16 * std::pow(kyplus, 2) + 1.0 / 48 * std::pow(kyplus, 3));
    } else {
        uPlus = 2.4 * (2 * std::pow(kyplusMinus1, 0.5) - 2 * std::atan(std::pow(kyplusMinus1, 0.5)));
    }  
    if (uPlus < 0) {
        uPlus = uPlusSA(yPlus);
    }    
     
    return uPlus;
}

template <typename T, template <typename U> class Descriptor>
int stressOnBoundaryCompFunctional3D<T, Descriptor>::computeVelocityDeriveDirection(Dot3D const &boundaryNode, Dot3D const &direction, BlockLattice3D<T, Descriptor> const &lattice, NTensorField3D<T> const &rhoVelocityNuField, TensorField3D<T, 3> const &wallDistanceY, Array<T, 3> &velocityDerive, T &U_tDerive)
{
    // Compute absolute and relative offset
    Dot3D absOff = lattice.getLocation();
    Dot3D offsetrhoVelocityNu = computeRelativeDisplacement(lattice, rhoVelocityNuField);
    Dot3D offsetWallDistanceY = computeRelativeDisplacement(lattice, wallDistanceY);
    // Get positive and negative cell property, solid or liquid?
    bool fluidNeg, fluidPos;

    Cell<T, Descriptor> const &cellPos = lattice.get(boundaryNode.x + direction.x, boundaryNode.y + direction.y, boundaryNode.z + direction.z);
    Cell<T, Descriptor> const &cellNeg = lattice.get(boundaryNode.x - direction.x, boundaryNode.y - direction.y, boundaryNode.z - direction.z);
    int noDynId = NoDynamics<T, Descriptor>().getId(); // solid node

    if (noDynId == cellPos.getDynamics().getId())
        fluidPos = false;
    else
        fluidPos = true;

    if (noDynId == cellNeg.getDynamics().getId())
        fluidNeg = false;
    else
        fluidNeg = true;
    // Something wrong in grid as both fluidPos and fluidNeg false
    if ((false == fluidPos) && (false == fluidNeg))
    {
        //		printf("Fatal, in  direction (%d, %d, %d), fluid node (%d, %d, %d) neighbors are both solid\n", direction.x, direction.y, direction.z, boundaryNode.x+absOff.x, boundaryNode.y+absOff.y, boundaryNode.z+absOff.z);
        //   		exit(1);

        velocityDerive[0] = 0.0;
        velocityDerive[1] = 0.0;
        velocityDerive[2] = 0.0;
        U_tDerive = 0.0;
        return -1;
    }
    // Step1 Compute original derivative of velocity in direction
    // Get velocity on positive, negative, and boundary nodes
    Array<T, 3> velocityPos;
    Array<T, 3> velocityNeg;
    Array<T, 3> velocityBoundaryNode;
    T const *rhoVelocityNuPos = rhoVelocityNuField.get(boundaryNode.x + offsetrhoVelocityNu.x + direction.x, boundaryNode.y + offsetrhoVelocityNu.y + direction.y, boundaryNode.z + offsetrhoVelocityNu.z + direction.z);
    T const *rhoVelocityNuNeg = rhoVelocityNuField.get(boundaryNode.x + offsetrhoVelocityNu.x - direction.x, boundaryNode.y + offsetrhoVelocityNu.y - direction.y, boundaryNode.z + offsetrhoVelocityNu.z - direction.z);
    T const *rhoVelocityNuBoundaryNode = rhoVelocityNuField.get(boundaryNode.x + offsetrhoVelocityNu.x, boundaryNode.y + offsetrhoVelocityNu.y, boundaryNode.z + offsetrhoVelocityNu.z);

    velocityPos.from_cArray(rhoVelocityNuPos + 1);
    velocityNeg.from_cArray(rhoVelocityNuNeg + 1);
    velocityBoundaryNode.from_cArray(rhoVelocityNuBoundaryNode + 1);
    // Compute velocityDerivative on direction by velocity on positive, negative and boudnary nodes
    velocityDeriveComp(velocityDerive, fluidPos, fluidNeg, velocityPos, velocityBoundaryNode, velocityNeg);

    // Step2 Compute derivative of tangential velocity in direction
    // Get tangential component of velocity on positive, negative, and boundary nodes.
    Array<T, 3> const &distanceNormalPos = wallDistanceY.get(boundaryNode.x + offsetWallDistanceY.x + direction.x, boundaryNode.y + offsetWallDistanceY.y + direction.y, boundaryNode.z + offsetWallDistanceY.z + direction.z);
    Array<T, 3> const &distanceNormalNeg = wallDistanceY.get(boundaryNode.x + offsetWallDistanceY.x - direction.x, boundaryNode.y + offsetWallDistanceY.y - direction.y, boundaryNode.z + offsetWallDistanceY.z - direction.z);
    Array<T, 3> const &distanceNormalBoundaryNode = wallDistanceY.get(boundaryNode.x + offsetWallDistanceY.x, boundaryNode.y + offsetWallDistanceY.y, boundaryNode.z + offsetWallDistanceY.z);

    T U_tPos = getVelocityTangentialMagnitude(velocityPos, distanceNormalPos);
    T U_tNeg = getVelocityTangentialMagnitude(velocityNeg, distanceNormalNeg);
    T U_tBoundaryNode = getVelocityTangentialMagnitude(velocityBoundaryNode, distanceNormalBoundaryNode);
    /*
    //Check U_tBoundaryNode: due to update of velocity on boudnary nodes, velocityBoundaryNodeMagnitude=U_tBoundaryNode
    T velocityBoundaryNodeMagnitude = std::sqrt(velocityBoundaryNode[0]*velocityBoundaryNode[0]+velocityBoundaryNode[1]*velocityBoundaryNode[1]+velocityBoundaryNode[2]*velocityBoundaryNode[2]) ;
    if (fabs(U_tBoundaryNode - velocityBoundaryNodeMagnitude) > 1.0e-12) {
        printf("Error: boundary node (%d, %d, %d) owns wrong tangential part of velocity\n", boundaryNode.x+absOff.x, boundaryNode.y+absOff.y, boundaryNode.z+absOff.z);
        exit(1);
    }
*/
    // Compute derivative of tangential magnitude of velocity on positive, negative, and boundary nodes
    U_tDeriveComp(U_tDerive, fluidPos, fluidNeg, U_tPos, U_tBoundaryNode, U_tNeg);
    return 1;
}

template <typename T, template <typename U> class Descriptor>
T stressOnBoundaryCompFunctional3D<T, Descriptor>::getVelocityTangentialMagnitude(Array<T, 3> const &velocity, Array<T, 3> const &distanceNormal)
{

    // compute wall normal
    Array<T, 3> wallNormal;
    T distance = std::sqrt(distanceNormal[0] * distanceNormal[0] + distanceNormal[1] * distanceNormal[1] + distanceNormal[2] * distanceNormal[2]);
    wallNormal[0] = distanceNormal[0] / (1.0e-12 + distance);
    wallNormal[1] = distanceNormal[1] / (1.0e-12 + distance);
    wallNormal[2] = distanceNormal[2] / (1.0e-12 + distance);

    // compute normal component of velocity
    T velocityNormal = velocity[0] * wallNormal[0] + velocity[1] * wallNormal[1] + velocity[2] * wallNormal[2];
    // compute tangential of velocity
    Array<T, 3> velocityTangential;
    velocityTangential[0] = velocity[0] - velocityNormal * wallNormal[0];
    velocityTangential[1] = velocity[1] - velocityNormal * wallNormal[1];
    velocityTangential[2] = velocity[2] - velocityNormal * wallNormal[2];
    T velocityTangentialMagnitude = std::sqrt(velocityTangential[0] * velocityTangential[0] + velocityTangential[1] * velocityTangential[1] + velocityTangential[2] * velocityTangential[2]);

    return velocityTangentialMagnitude;
}

template <typename T, template <typename U> class Descriptor>
void stressOnBoundaryCompFunctional3D<T, Descriptor>::U_tDeriveComp(T &U_tDerive, const bool fluidPos, const bool fluidNeg, T const &U_tPos, T const &U_tBoundaryNode, T const &U_tNeg)
{
    // The other three conditions
    // central scheme
    if ((true == fluidPos) && (true == fluidNeg))
    {
        U_tDerive = fd::ctl_diff(U_tPos, U_tNeg);
    }
    // one-sided scheme
    if ((true == fluidPos) && (false == fluidNeg))
    {
        U_tDerive = fd::o1_fwd_diff(U_tBoundaryNode, U_tPos);
    }
    if ((false == fluidPos) && (true == fluidNeg))
    {
        U_tDerive = fd::o1_fwd_diff(U_tNeg, U_tBoundaryNode);
    }
}

template <typename T, template <typename U> class Descriptor>
void stressOnBoundaryCompFunctional3D<T, Descriptor>::velocityDeriveComp(Array<T, 3> &velocityDerive, const bool fluidPos, const bool fluidNeg, Array<T, 3> const &velocityPos, Array<T, 3> const &velocityBoundaryNode, Array<T, 3> const &velocityNeg)
{
    // The other three conditions
    // central scheme
    if ((true == fluidPos) && (true == fluidNeg))
    {
        for (int dim = 0; dim < 3; dim++)
        {
            velocityDerive[dim] = fd::ctl_diff(velocityPos[dim], velocityNeg[dim]);
        }
    }
    // one-sided scheme
    if ((true == fluidPos) && (false == fluidNeg))
    {
        for (int dim = 0; dim < 3; dim++)
        {
            velocityDerive[dim] = fd::o1_fwd_diff(velocityBoundaryNode[dim], velocityPos[dim]);
        }
    }
    if ((false == fluidPos) && (true == fluidNeg))
    {
        for (int dim = 0; dim < 3; dim++)
        {
            velocityDerive[dim] = fd::o1_fwd_diff(velocityNeg[dim], velocityBoundaryNode[dim]);
        }
    }
}

template <typename T, template <typename U> class Descriptor>
stressOnBoundaryCompFunctional3D<T, Descriptor> *stressOnBoundaryCompFunctional3D<T, Descriptor>::clone() const
{
    return new stressOnBoundaryCompFunctional3D<T, Descriptor>(*this);
}

template <typename T, template <typename U> class Descriptor>
void stressOnBoundaryCompFunctional3D<T, Descriptor>::getTypeOfModification(std::vector<modif::ModifT> &modified) const
{
    for (int item = 0; item < 2 + numShapeArgs + numCompletionArgs - 1; item++)
    {
        modified[item] = modif::nothing;
    }
    // stressField should be updated
    modified[2 + numShapeArgs + 5] = modif::staticVariables;
}

template <typename T, template <typename U> class Descriptor>
BlockDomain::DomainT stressOnBoundaryCompFunctional3D<T, Descriptor>::appliesTo() const
{
    return BlockDomain::bulk;
}

////////////////////////////////////////////////////////////////
// data processor for finding and marking boundary nodes of stencil
template <typename T, template <typename U> class Descriptor>
boundaryNodeStencilCompFunctional3D<T, Descriptor>::boundaryNodeStencilCompFunctional3D(int numShapeArgs_, int numCompletionArgs_)
    : numShapeArgs(numShapeArgs_),
      numCompletionArgs(numCompletionArgs_)
{
}

template <typename T, template <typename U> class Descriptor>
void boundaryNodeStencilCompFunctional3D<T, Descriptor>::processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D *> blocks)
{
    PLB_ASSERT(blocks.size() == 2 + numShapeArgs + numCompletionArgs);
    typedef Descriptor<T> D;
    // numCompletionArgs:0:rhoVelocityNufield,1:wallDistanceY,2:stencilFlag,3:dynamicID,4:utau, 5:stressTensorField,6:modifRhoVelocityField
    BlockLattice3D<T, Descriptor> *lattice = dynamic_cast<BlockLattice3D<T, Descriptor> *>(blocks[0]);
    ScalarField3D<int> *stencilField = dynamic_cast<ScalarField3D<int> *>(blocks[2 + numShapeArgs + 2]);
    ScalarField3D<int> *dynamicIDField = dynamic_cast<ScalarField3D<int> *>(blocks[2 + numShapeArgs + 3]);

    PLB_ASSERT(lattice);

    Dot3D offsetStencil = computeRelativeDisplacement(*lattice, *stencilField);
    Dot3D offsetDynamicID = computeRelativeDisplacement(*lattice, *dynamicIDField);

    Dot3D absOff = lattice->getLocation();
    int noDynId = NoDynamics<T, Descriptor>().getId();
    for (plint iX = domain.x0; iX <= domain.x1; ++iX)
    {
        for (plint iY = domain.y0; iY <= domain.y1; ++iY)
        {
            for (plint iZ = domain.z0; iZ <= domain.z1; ++iZ)
            {
                const int dynamicID = dynamicIDField->get(iX + offsetDynamicID.x, iY + offsetDynamicID.y, iZ + offsetDynamicID.z);
                // only consider fluid node
                if (noDynId == dynamicID || TOO_CLOSE == dynamicID)
                {
                    //stencilField->get(iX + offsetStencil.x, iY + offsetStencil.y, iZ + offsetStencil.z) = -1;
                    continue;
                }
                if (NO_DISTANCE == dynamicID)
                {
                    //stencilField->get(iX + offsetStencil.x, iY + offsetStencil.y, iZ + offsetStencil.z) = -1;
                    continue;
                }

                for (int qID = 0; qID < D::q; qID++)
                {
                    const Dot3D neighbor(iX + D::c[qID][0], iY + D::c[qID][1], iZ + D::c[qID][2]);
                    const int dynamicIDNgb = dynamicIDField->get(neighbor.x + offsetDynamicID.x, neighbor.y + offsetDynamicID.y, neighbor.z + offsetDynamicID.z);
                    // fluid node owns solid nodes or too close nodes, it is a boundary node
                    if (noDynId == dynamicIDNgb || TOO_CLOSE == dynamicIDNgb)
                        stencilField->get(iX + offsetStencil.x, iY + offsetStencil.y, iZ + offsetStencil.z) = 1;
                }
            }
        }
    }
}

template <typename T, template <typename U> class Descriptor>
boundaryNodeStencilCompFunctional3D<T, Descriptor> *boundaryNodeStencilCompFunctional3D<T, Descriptor>::clone() const
{
    return new boundaryNodeStencilCompFunctional3D<T, Descriptor>(*this);
}

template <typename T, template <typename U> class Descriptor>
void boundaryNodeStencilCompFunctional3D<T, Descriptor>::getTypeOfModification(std::vector<modif::ModifT> &modified) const
{
    for (int item = 0; item < 2 + numShapeArgs + numCompletionArgs - 1; item++)
    {
        modified[item] = modif::nothing;
    }
    // stencilField should be modified
    modified[2 + numShapeArgs + 2] = modif::staticVariables;
}

template <typename T, template <typename U> class Descriptor>
BlockDomain::DomainT boundaryNodeStencilCompFunctional3D<T, Descriptor>::appliesTo() const
{
    return BlockDomain::bulk;
}
////////////////////////////////////////////////////////////////////
// data processor for finding and marking donor nodes of stencil
// It should be used after boundary nodes treatment
template <typename T, template <typename U> class Descriptor>
donorNodeStencilCompFunctional3D<T, Descriptor>::donorNodeStencilCompFunctional3D(int numShapeArgs_, int numCompletionArgs_)
    : numShapeArgs(numShapeArgs_),
      numCompletionArgs(numCompletionArgs_)
{
}

template <typename T, template <typename U> class Descriptor>
void donorNodeStencilCompFunctional3D<T, Descriptor>::processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D *> blocks)
{
    PLB_ASSERT(blocks.size() == 2 + numShapeArgs + numCompletionArgs);
    typedef Descriptor<T> D;
    // numCompletionArgs:0:rhoVelocityNufield,1:wallDistanceY,2:stencilFlag,3:dynamicID,4:utau, 5:stressTensorField,6:modifRhoVelocityField
    BlockLattice3D<T, Descriptor> *lattice = dynamic_cast<BlockLattice3D<T, Descriptor> *>(blocks[0]);
    ScalarField3D<int> *stencilField = dynamic_cast<ScalarField3D<int> *>(blocks[2 + numShapeArgs + 2]);
    ScalarField3D<int> *dynamicIDField = dynamic_cast<ScalarField3D<int> *>(blocks[2 + numShapeArgs + 3]);

    PLB_ASSERT(lattice);

    Dot3D offsetStencil = computeRelativeDisplacement(*lattice, *stencilField);
    Dot3D offsetDynamicID = computeRelativeDisplacement(*lattice, *dynamicIDField);

    Dot3D absOff = lattice->getLocation();
    int noDynId = NoDynamics<T, Descriptor>().getId();
    for (plint iX = domain.x0; iX <= domain.x1; ++iX)
    {
        for (plint iY = domain.y0; iY <= domain.y1; ++iY)
        {
            for (plint iZ = domain.z0; iZ <= domain.z1; ++iZ)
            {
                const int dynamicID = dynamicIDField->get(iX + offsetDynamicID.x, iY + offsetDynamicID.y, iZ + offsetDynamicID.z);
                // only consider fluid node
                if (noDynId == dynamicID || TOO_CLOSE == dynamicID || NO_DISTANCE == dynamicID)
                    continue;
                const int stencilFlag = stencilField->get(iX + offsetStencil.x, iY + offsetStencil.y, iZ + offsetStencil.z);
                // boundary node is not donor node
                if (1 == stencilFlag)
                    continue;

                bool isDonorNode = false;
                bool isBoundaryNode = false;
                // The first layer
                for (int qID = 0; qID < D::q; qID++)
                {
                    const Dot3D neighbor(iX + D::c[qID][0], iY + D::c[qID][1], iZ + D::c[qID][2]);
                    const int dynamicIDNgb = dynamicIDField->get(neighbor.x + offsetDynamicID.x, neighbor.y + offsetDynamicID.y, neighbor.z + offsetDynamicID.z);
                    // just for warning: fluid node owns solid nodes, it is a boundary node
                    if (noDynId == dynamicIDNgb || TOO_CLOSE == dynamicIDNgb)
                        isBoundaryNode = true;
                    const int stencilFlagNgb = stencilField->get(neighbor.x + offsetStencil.x, neighbor.y + offsetStencil.y, neighbor.z + offsetStencil.z);
                    // fluid node owns boundary node on the first layer, it is the boundary node's donor node
                    if (1 == stencilFlagNgb)
                        isDonorNode = true;
                }
                // The second layer
                for (int qID = 0; qID < D::q; qID++)
                {
                    const Dot3D neighbor(iX + 2 * D::c[qID][0], iY + 2 * D::c[qID][1], iZ + 2 * D::c[qID][2]);
                    const int dynamicIDNgb = dynamicIDField->get(neighbor.x + offsetDynamicID.x, neighbor.y + offsetDynamicID.y, neighbor.z + offsetDynamicID.z);
                    const int stencilFlagNgb = stencilField->get(neighbor.x + offsetStencil.x, neighbor.y + offsetStencil.y, neighbor.z + offsetStencil.z);
                    // fluid node owns boundary node on the second layer, it is the boundary node's donor node
                    if (1 == stencilFlagNgb)
                        isDonorNode = true;
                }
                if (isBoundaryNode)
                {
                    printf("Fatal: some boundary nodes are not recorded\n");
                    exit(1);
                }
                if (isDonorNode)
                    stencilField->get(iX + offsetStencil.x, iY + offsetStencil.y, iZ + offsetStencil.z) = 2;
            }
        }
    }
}

template <typename T, template <typename U> class Descriptor>
donorNodeStencilCompFunctional3D<T, Descriptor> *donorNodeStencilCompFunctional3D<T, Descriptor>::clone() const
{
    return new donorNodeStencilCompFunctional3D<T, Descriptor>(*this);
}

template <typename T, template <typename U> class Descriptor>
void donorNodeStencilCompFunctional3D<T, Descriptor>::getTypeOfModification(std::vector<modif::ModifT> &modified) const
{
    for (int item = 0; item < 2 + numShapeArgs + numCompletionArgs - 1; item++)
    {
        modified[item] = modif::nothing;
    }
    // stencilField should be modified
    modified[2 + numShapeArgs + 2] = modif::staticVariables;
}

template <typename T, template <typename U> class Descriptor>
BlockDomain::DomainT donorNodeStencilCompFunctional3D<T, Descriptor>::appliesTo() const
{
    return BlockDomain::bulk;
}

// data analysis processor for setting Csmago in LES model
template <typename T, template <typename U> class Descriptor>
SetLESCsmagoJohanFunctional3D<T, Descriptor>::SetLESCsmagoJohanFunctional3D(int numShapeArgs_, int numCompletionArgs_, T cSmago_)
    : numShapeArgs(numShapeArgs_),
      numCompletionArgs(numCompletionArgs_),
      cSmago(cSmago_)
{
}

template <typename T, template <typename U> class Descriptor>
void SetLESCsmagoJohanFunctional3D<T, Descriptor>::processGenericBlocks(
    Box3D domain, std::vector<AtomicBlock3D *> blocks)
{

    PLB_ASSERT(blocks.size() == 2 + numShapeArgs + numCompletionArgs);
    // numCompletionArgs:0:rhoVelocityNufield,1:wallDistanceY,2:stencilFlag,3:dynamicID,4:utau, 5:stressTensorField

    BlockLattice3D<T, Descriptor> *lattice = dynamic_cast<BlockLattice3D<T, Descriptor> *>(blocks[0]);
    NTensorField3D<T> const *rhoVelocityNuField = dynamic_cast<NTensorField3D<T> const *>(blocks[2 + numShapeArgs]);
    TensorField3D<T, 3> const *wallDistanceY = dynamic_cast<TensorField3D<T, 3> const *>(blocks[2 + numShapeArgs + 1]);
    ScalarField3D<int> const *stencilField = dynamic_cast<ScalarField3D<int> const *>(blocks[2 + numShapeArgs + 2]);
    ScalarField3D<T> const *utauField = dynamic_cast<ScalarField3D<T> const *>(blocks[2 + numShapeArgs + 4]);
    Dot3D offsetStencil = computeRelativeDisplacement(*lattice, *stencilField);
    Dot3D offsetrhoVelocityNu = computeRelativeDisplacement(*lattice, *rhoVelocityNuField);
    Dot3D offsetWallDistanceY = computeRelativeDisplacement(*lattice, *wallDistanceY);
    Dot3D offsettauField = computeRelativeDisplacement(*lattice, *utauField);

    int noDynId = NoDynamics<T, Descriptor>().getId();
    // Set distanceIP, KK, alphaDistance for different Reynolds number and simulations.
    T distanceIP = 2.5;
    // T distanceIP = 2.5;

    // T KK = 0.41;
    T KK = 2.0;
    // T KK = 0.30;
    T alphaDistance = 0.4;
    // T alphaDistance = 0.4;
    T distanceCRT = alphaDistance * distanceIP;

    for (plint iX = domain.x0; iX <= domain.x1; ++iX)
    {
        for (plint iY = domain.y0; iY <= domain.y1; ++iY)
        {
            for (plint iZ = domain.z0; iZ <= domain.z1; ++iZ)
            {
                int stencilFlag = stencilField->get(iX + offsetStencil.x, iY + offsetStencil.y, iZ + offsetStencil.z);
                // only modify csmago of stencil nodes including boundary nodes and donor nodes
                // if (0 == stencilFlag) continue;

                Cell<T, Descriptor> &cell = lattice->get(iX, iY, iZ);
                Dynamics<T, Descriptor> &dynamic = cell.getDynamics();
                // Nodynamics, no turbulence model (solid node)
                if (dynamic.getId() == noDynId)
                    continue;
                Array<T, 3> distanceNormal = wallDistanceY->get(iX + offsetWallDistanceY.x, iY + offsetWallDistanceY.y, iZ + offsetWallDistanceY.z);
                T distanceY = std::sqrt(distanceNormal[0] * distanceNormal[0] + distanceNormal[1] * distanceNormal[1] + distanceNormal[2] * distanceNormal[2]);

                // only consider cells close to body
                if (distanceY > distanceIP) 
                    continue;

                // distanceY > distanceIP: LES model
                // nutLES = csmago*csmago*||S||

                T cSmagoRANS = 0.0;
                // RANS model
                // nutRANS = (ka*d_crt)*(ka*d_crt)*||S||
                if ((0 < distanceY) && (distanceY <= distanceCRT))
                    cSmagoRANS = KK * distanceCRT;
                // Hybrid model: RANS+LES
                // nutHybrid = sqrt((ka*Y)*(ka*Y)*K^gamma + cSmago*cSmago*(1.0-K^gamma))*sqrt((ka*Y)*(ka*Y)*K^gamma + cSmago*cSmago*(1.0-K^gamma))*||S||

                if ((distanceCRT < distanceY) && (distanceY <= distanceIP))
                    cSmagoRANS = KK * distanceY;

                T ratio = (distanceIP - distanceY) / (distanceIP - distanceCRT);
                T coefK = (std::max)((std::min)(ratio, (T)1.0), (T)0.0);
                T gamma = 2.0;
                T cSmagoNew = std::sqrt(cSmagoRANS * cSmagoRANS * std::pow(coefK, gamma) + (1.0 - pow(coefK, gamma)));

                /*
                                //Van Driest damping function
                                                T utauNode = utauField->get(iX+offsettauField.x, iY+offsettauField.y, iZ+offsettauField.z);
                                                const T * rhoVelocityNu = rhoVelocityNuField->get(iX+offsetrhoVelocityNu.x,iY+offsetrhoVelocityNu.y,iZ+offsetrhoVelocityNu.z);
                                                T nuNode = rhoVelocityNu[4];
                                                T yPlus = distanceY * utauNode / nuNode;
                                                T ePart = 1.0 - std::exp(-yPlus/26.0);
                                                T vanDriest = KK*distanceY/0.158*ePart;
                                                if (fabs(vanDriest-1.0)<1.0e-12) cSmagoNew = cSmagoNew * vanDriest;
                */
                dynamic.setParameter(dynamicParams::smagorinskyConstant, cSmagoNew);
                //dynamic.setDynamicSmago(cell, cSmagoNew);
            }
        }
    }
}

template <typename T, template <typename U> class Descriptor>
SetLESCsmagoJohanFunctional3D<T, Descriptor> *SetLESCsmagoJohanFunctional3D<T, Descriptor>::clone() const
{
    return new SetLESCsmagoJohanFunctional3D<T, Descriptor>(*this);
}

template <typename T, template <typename U> class Descriptor>
void SetLESCsmagoJohanFunctional3D<T, Descriptor>::getTypeOfModification(std::vector<modif::ModifT> &modified) const
{
    modified[0] = modif::staticVariables;
    modified[1] = modif::nothing;
}
// #include "../../src/offLatticeNearWallBoundaryCondition3D.hh"
template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::
    OffLatticeNearWallBoundaryCondition3D(
        OffLatticeNearWallModel3D<T, BoundaryType> *offLatticeModel_,
        VoxelizedDomain3D<T> &voxelizedDomain_,
        MultiBlockLattice3D<T, Descriptor> &lattice_)
    : voxelizedDomain(voxelizedDomain_),
      lattice(lattice_),
      boundaryShapeArg(lattice_),
      offLatticeModel(offLatticeModel_),
      offLatticePattern(lattice)
{
    // It is very important that the "offLatticePattern" container block
    // has the same multi-block management as the lattice used in the
    // simulation.
    std::vector<MultiBlock3D *> offLatticeIniArg;
    // First argument for compute-off-lattice-pattern.
    offLatticeIniArg.push_back(&offLatticePattern);
    // Remaining arguments for inner-flow-shape.
    offLatticeIniArg.push_back(&voxelizedDomain.getVoxelMatrix());
    offLatticeIniArg.push_back(&voxelizedDomain.getTriangleHash());
    offLatticeIniArg.push_back(&boundaryShapeArg);
    applyProcessingFunctional(
        new OffLatticeNearWallPatternFunctional3D<T, BoundaryType>(
            offLatticeModel->clone()),
        offLatticePattern.getBoundingBox(), offLatticeIniArg);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::
    OffLatticeNearWallBoundaryCondition3D(
        OffLatticeNearWallModel3D<T, BoundaryType> *offLatticeModel_,
        VoxelizedDomain3D<T> &voxelizedDomain_,
        MultiBlockLattice3D<T, Descriptor> &lattice_,
        MultiParticleField3D<DenseParticleField3D<T, Descriptor>> &particleField_)
    : offLatticeModel(offLatticeModel_),
      voxelizedDomain(voxelizedDomain_),
      lattice(lattice_),
      boundaryShapeArg(particleField_),
      offLatticePattern(lattice)
{
    // It is very important that the "offLatticePattern" container block
    // has the same multi-block management as the lattice used in the
    // simulation.
    std::vector<MultiBlock3D *> offLatticeIniArg;
    // First argument for compute-off-lattice-pattern.
    offLatticeIniArg.push_back(&offLatticePattern);
    // Remaining arguments for inner-flow-shape.
    offLatticeIniArg.push_back(&voxelizedDomain.getVoxelMatrix());
    offLatticeIniArg.push_back(&voxelizedDomain.getTriangleHash());
    offLatticeIniArg.push_back(&boundaryShapeArg);
    applyProcessingFunctional(
        new OffLatticeNearWallPatternFunctional3D<T, BoundaryType>(
            offLatticeModel->clone()),
        offLatticePattern.getBoundingBox(), offLatticeIniArg);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::OffLatticeNearWallBoundaryCondition3D(
    OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType> const &rhs)
    : offLatticeModel(rhs.offLatticeModel.clone()),
      voxelizedDomain(rhs.voxelizedDomain),
      lattice(rhs.lattice),
      boundaryShapeArg(rhs.boundaryShapeArg),
      offLatticePattern(rhs.offLatticePattern)
{
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::~OffLatticeNearWallBoundaryCondition3D()
{
    delete offLatticeModel;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
void OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::insert(plint processorLevel)
{
    // It is very important that the "offLatticePattern" container block
    // has the same multi-block management as the lattice used in the
    // simulation.
    std::vector<MultiBlock3D *> offLatticeArg;
    // First two arguments for Guo algorithm.
    offLatticeArg.push_back(&lattice);
    offLatticeArg.push_back(&offLatticePattern);
    // Remaining arguments for inner-flow-shape.
    offLatticeArg.push_back(&voxelizedDomain.getVoxelMatrix());
    offLatticeArg.push_back(&voxelizedDomain.getTriangleHash());
    offLatticeArg.push_back(&boundaryShapeArg);
    plint numShapeArgs = 3;
    plint numCompletionArgs = 0;
    integrateProcessingFunctional(
        new OffLatticeNearWallCompletionFunctional3D<T, Descriptor, BoundaryType>(
            offLatticeModel->clone(), numShapeArgs, numCompletionArgs),
        boundaryShapeArg.getBoundingBox(), offLatticeArg, processorLevel);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
void OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::insert(
    std::vector<MultiBlock3D *> const &completionArg, plint processorLevel)
{
    // It is very important that the "offLatticePattern" container block
    // has the same multi-block management as the lattice used in the
    // simulation.
    std::vector<MultiBlock3D *> offLatticeArg;
    // First three arguments for Guo algorithm.
    offLatticeArg.push_back(&lattice);
    offLatticeArg.push_back(&offLatticePattern);
    // Next arguments for inner-flow-shape.
    offLatticeArg.push_back(&voxelizedDomain.getVoxelMatrix());
    offLatticeArg.push_back(&voxelizedDomain.getTriangleHash());
    offLatticeArg.push_back(&boundaryShapeArg);
    // Remaining are optional arguments for completion algorithm.
    // 0:rhoVelocityNufield,1:wallDistanceY,2:stencilFlag,3:dynamicID,4:utau,5:stress,6:modifRhoVelocityField
    plint numCompletionArgs = (plint)completionArg.size();
    for (plint i = 0; i < numCompletionArgs; ++i)
    {
        offLatticeArg.push_back(completionArg[i]);
    }
    plint numShapeArgs = 3;
    // compute stencilFlag

    pcout << "compute wall distance and normal on stencil nodes..." << std::endl;
    applyProcessingFunctional(
        new OffLatticeWallDistanceYNormalFunctional3D<T, Descriptor, BoundaryType>(
            offLatticeModel->clone(), numShapeArgs, numCompletionArgs),
        lattice.getBoundingBox(), offLatticeArg);
    pcout << "compute stencilFlag ..." << std::endl;
    applyProcessingFunctional(
        new boundaryNodeStencilCompFunctional3D<T, Descriptor>(
            numShapeArgs, numCompletionArgs),
        lattice.getBoundingBox(), offLatticeArg);

    applyProcessingFunctional(
        new donorNodeStencilCompFunctional3D<T, Descriptor>(
            numShapeArgs, numCompletionArgs),
        lattice.getBoundingBox(), offLatticeArg);
    // compute velocity on field
    pcout << "insert local calculation of velocity ..." << std::endl;
    MultiNTensorField3D<T> *rhoVelocityNufield =
        dynamic_cast<MultiNTensorField3D<T> *>(completionArg[0]);
    integrateProcessingFunctional(new PackedRhoVelocityNufunctional3D<T, Descriptor>(),
                                  lattice.getBoundingBox(), lattice, *rhoVelocityNufield, 0);
    pcout << "finish insert calculation of velocity ..." << std::endl;

    pcout << "insert local calculation of U_tau on donor nodes..." << std::endl;
    integrateProcessingFunctional(
        new stencilUtauCompFunctional3D<T, Descriptor>(
            numShapeArgs, numCompletionArgs),
        lattice.getBoundingBox(), offLatticeArg, 1);
    pcout << "finish insert calculation of U_tau ..." << std::endl;
    pcout << "insert non-local interpolation of U_tau and density on boundary nodes..." << std::endl;
    integrateProcessingFunctional(
        new boundaryUtauDensityInterpFunctional3D<T, Descriptor>(
            numShapeArgs, numCompletionArgs),
        lattice.getBoundingBox(), offLatticeArg, 2);
    pcout << "finish insert interpolation of U_tau and density ..." << std::endl;

    pcout << "insert local update velocity on boudnary nodes..." << std::endl;
    integrateProcessingFunctional(
        new velocityOnBoundaryUpdateFunctional3D<T, Descriptor>(
            numShapeArgs, numCompletionArgs),
        lattice.getBoundingBox(), offLatticeArg, 3);
    pcout << "finish insert velocity update on boundary nodes" << std::endl;
    pcout << "insert non-local stress computing on boundary nodes..." << std::endl;
    integrateProcessingFunctional(
        new stressOnBoundaryCompFunctional3D<T, Descriptor>(
            numShapeArgs, numCompletionArgs),
        lattice.getBoundingBox(), offLatticeArg, 4);

    pcout << "insert non-local completion of distribution functions for boundary nodes" << std::endl;
    integrateProcessingFunctional(
        new OffLatticeNearWallCompletionFunctional3D<T, Descriptor, BoundaryType>(
            offLatticeModel->clone(), numShapeArgs, numCompletionArgs),
        boundaryShapeArg.getBoundingBox(), offLatticeArg, 5);
    /*
        pcout<<"insert local computing of turbulence viscosity"<<endl;
        integrateProcessingFunctional (
                new SetLESCsmagoJohanFunctional3D<T,Descriptor> (
                    numShapeArgs, numCompletionArgs, 0.1),
                lattice.getBoundingBox(), offLatticeArg, 5);
    */
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
void OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::apply()
{
    // It is very important that the "offLatticePattern" container block
    // has the same multi-block management as the lattice used in the
    // simulation.
    std::vector<MultiBlock3D *> offLatticeArg;
    // First three arguments for Guo algorithm.
    offLatticeArg.push_back(&lattice);
    offLatticeArg.push_back(&offLatticePattern);
    // Remaining arguments for inner-flow-shape.
    offLatticeArg.push_back(&voxelizedDomain.getVoxelMatrix());
    offLatticeArg.push_back(&voxelizedDomain.getTriangleHash());
    offLatticeArg.push_back(&boundaryShapeArg);
    plint numShapeArgs = 3;
    plint numCompletionArgs = 0;
    applyProcessingFunctional(
        new OffLatticeNearWallCompletionFunctional3D<T, Descriptor, BoundaryType>(
            offLatticeModel->clone(), numShapeArgs, numCompletionArgs),
        boundaryShapeArg.getBoundingBox(), offLatticeArg);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
void OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::apply(
    std::vector<MultiBlock3D *> const &completionArg)
{
    // It is very important that the "offLatticePattern" container block
    // has the same multi-block management as the lattice used in the
    // simulation.
    std::vector<MultiBlock3D *> offLatticeArg;
    // First three arguments for Guo algorithm.
    offLatticeArg.push_back(&lattice);
    offLatticeArg.push_back(&offLatticePattern);
    // Next arguments for inner-flow-shape.
    offLatticeArg.push_back(&voxelizedDomain.getVoxelMatrix());
    offLatticeArg.push_back(&voxelizedDomain.getTriangleHash());
    offLatticeArg.push_back(&boundaryShapeArg);
    // Remaining are optional arguments for completion algorithm.
    plint numCompletionArgs = (plint)completionArg.size();
    for (plint i = 0; i < numCompletionArgs; ++i)
    {
        offLatticeArg.push_back(completionArg[i]);
    }
    plint numShapeArgs = 3;
    applyProcessingFunctional(
        new OffLatticeNearWallCompletionFunctional3D<T, Descriptor, BoundaryType>(
            offLatticeModel->clone(), numShapeArgs, numCompletionArgs),
        boundaryShapeArg.getBoundingBox(), offLatticeArg);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
Array<T, 9> OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::getForceOnObject()
{
    std::vector<MultiBlock3D *> arg;
    arg.push_back(&offLatticePattern);
    GetForceOnObjectNearWallFunctional3D<T, BoundaryType> functional(offLatticeModel->clone());
    applyProcessingFunctional(
        functional, boundaryShapeArg.getBoundingBox(), arg);
    return functional.getForce();
}

template <typename T, template <typename U> class Descriptor, class BoundaryType>
Array<T, 9> OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::getForceOnMesh(
    RawConnectedTriangleMesh<T>* triangleMesh, plint propertyID, std::vector<MultiBlock3D *>& completionArg, T fScale)
{
    std::vector<MultiBlock3D *> blocks(completionArg);
    blocks.push_back(&lattice);
    blocks.push_back(&offLatticePattern);

    applyProcessingFunctional(new CalculateDataOnTriangleMesh3D<T, Descriptor, BoundaryType>(
        triangleMesh, propertyID, completionArg.size(), fScale, offLatticeModel), lattice.getBoundingBox(), blocks);

    std::vector<MultiBlock3D *> arg;
    arg.push_back(&offLatticePattern);
    GetForceOnMeshNearWallFunctional3D<T, BoundaryType> functional(offLatticeModel->clone());
    applyProcessingFunctional(
        functional, boundaryShapeArg.getBoundingBox(), arg);
    return functional.getForce();
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiTensorField3D<T, 3>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVelocity(Box3D domain)
{
    std::unique_ptr<MultiTensorField3D<T, 3>> velocity(plb::computeVelocity(lattice, domain));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    setToConstant<T, 3>(*velocity, voxelizedDomain.getVoxelMatrix(), solidFlag,
                        domain, Array<T, 3>(T(), T(), T()));
    setToConstant<T, 3>(*velocity, voxelizedDomain.getVoxelMatrix(), solidBorderFlag,
                        domain, Array<T, 3>(T(), T(), T()));
    return velocity;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiTensorField3D<T, 3>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVelocity()
{
    return computeVelocity(lattice.getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiTensorField3D<T, 3>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVorticity(Box3D domain)
{
    std::unique_ptr<MultiTensorField3D<T, 3>> vorticity(
        plb::computeBulkVorticity(*plb::computeVelocity(lattice, domain), domain));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    setToConstant<T, 3>(*vorticity, voxelizedDomain.getVoxelMatrix(), solidFlag,
                        domain, Array<T, 3>(T(), T(), T()));
    setToConstant<T, 3>(*vorticity, voxelizedDomain.getVoxelMatrix(), solidBorderFlag,
                        domain, Array<T, 3>(T(), T(), T()));
    return vorticity;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiTensorField3D<T, 3>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVorticity()
{
    return computeVorticity(lattice.getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVelocityNorm(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>> velocityNorm(plb::computeVelocityNorm(lattice, domain));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    setToConstant(*velocityNorm, voxelizedDomain.getVoxelMatrix(),
                  solidFlag, domain, (T)0);
    setToConstant(*velocityNorm, voxelizedDomain.getVoxelMatrix(),
                  solidBorderFlag, domain, (T)0);
    return velocityNorm;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVelocityNorm()
{
    return computeVelocityNorm(lattice.getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVorticityNorm(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>> vorticityNorm(
        plb::computeNorm(
            *plb::computeBulkVorticity(*plb::computeVelocity(lattice, domain), domain), domain));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    setToConstant<T>(*vorticityNorm, voxelizedDomain.getVoxelMatrix(), solidFlag, domain, T());
    setToConstant<T>(*vorticityNorm, voxelizedDomain.getVoxelMatrix(), solidBorderFlag, domain, T());
    return vorticityNorm;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVorticityNorm()
{
    return computeVorticityNorm(lattice.getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVelocityComponent(
    Box3D domain, plint iComp)
{
    std::unique_ptr<MultiScalarField3D<T>> velocityComponent(
        plb::computeVelocityComponent(lattice, domain, iComp));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    setToConstant(*velocityComponent, voxelizedDomain.getVoxelMatrix(),
                  solidFlag, domain, (T)0);
    setToConstant(*velocityComponent, voxelizedDomain.getVoxelMatrix(),
                  solidBorderFlag, domain, (T)0);
    return velocityComponent;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVelocityComponent(plint iComp)
{
    return computeVelocityComponent(lattice.getBoundingBox(), iComp);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computePressure(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>> pressure(plb::computeDensity(lattice, domain));
    T averageDensity = computeAverageDensity(domain);
    subtractInPlace(*pressure, averageDensity, domain);
    multiplyInPlace(*pressure, Descriptor<T>::cs2, domain);
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    setToConstant(*pressure, voxelizedDomain.getVoxelMatrix(),
                  solidFlag, domain, (T)0);
    setToConstant(*pressure, voxelizedDomain.getVoxelMatrix(),
                  solidBorderFlag, domain, (T)0);
    return pressure;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computePressure()
{
    return computePressure(lattice.getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeDensity(Box3D domain, T solidDensity)
{
    std::unique_ptr<MultiScalarField3D<T>> density(plb::computeDensity(lattice, domain));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    setToConstant(*density, voxelizedDomain.getVoxelMatrix(),
                  solidFlag, domain, solidDensity);
    setToConstant(*density, voxelizedDomain.getVoxelMatrix(),
                  solidBorderFlag, domain, solidDensity);
    return density;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeDensity(T solidDensity)
{
    return computeDensity(lattice.getBoundingBox(), solidDensity);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeStrainRateNorm(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>>
        strainRateNorm(computeSymmetricTensorNorm(*plb::computeStrainRateFromStress(lattice, domain)));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    setToConstant(*strainRateNorm, voxelizedDomain.getVoxelMatrix(),
                  solidFlag, domain, (T)0);
    setToConstant(*strainRateNorm, voxelizedDomain.getVoxelMatrix(),
                  solidBorderFlag, domain, (T)0);
    return strainRateNorm;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeStrainRateNorm()
{
    return computeStrainRateNorm(lattice.getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiTensorField3D<T, SymmetricTensor<T, Descriptor>::n>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeStrainRate(Box3D domain)
{
    std::unique_ptr<MultiTensorField3D<T, SymmetricTensor<T, Descriptor>::n>>
        strainRate(plb::computeStrainRateFromStress(lattice, domain));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    Array<T, SymmetricTensor<T, Descriptor>::n> zeros;
    zeros.resetToZero();
    setToConstant<T, SymmetricTensor<T, Descriptor>::n>(*strainRate, voxelizedDomain.getVoxelMatrix(),
                                                        solidFlag, domain, zeros);
    setToConstant<T, SymmetricTensor<T, Descriptor>::n>(*strainRate, voxelizedDomain.getVoxelMatrix(),
                                                        solidBorderFlag, domain, zeros);
    return strainRate;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiTensorField3D<T, SymmetricTensor<T, Descriptor>::n>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeStrainRate()
{
    return computeStrainRate(lattice.getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeShearStressNorm(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>>
        shearStressNorm(computeSymmetricTensorNorm(*plb::computeShearStress(lattice, domain)));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    setToConstant(*shearStressNorm, voxelizedDomain.getVoxelMatrix(),
                  solidFlag, domain, (T)0);
    setToConstant(*shearStressNorm, voxelizedDomain.getVoxelMatrix(),
                  solidBorderFlag, domain, (T)0);
    return shearStressNorm;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeShearStressNorm()
{
    return computeShearStressNorm(lattice.getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiTensorField3D<T, SymmetricTensor<T, Descriptor>::n>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeShearStress(Box3D domain)
{
    std::unique_ptr<MultiTensorField3D<T, SymmetricTensor<T, Descriptor>::n>>
        shearStress(plb::computeShearStress(lattice, domain));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    Array<T, SymmetricTensor<T, Descriptor>::n> zeros;
    zeros.resetToZero();
    setToConstant<T, SymmetricTensor<T, Descriptor>::n>(*shearStress, voxelizedDomain.getVoxelMatrix(),
                                                        solidFlag, domain, zeros);
    setToConstant<T, SymmetricTensor<T, Descriptor>::n>(*shearStress, voxelizedDomain.getVoxelMatrix(),
                                                        solidBorderFlag, domain, zeros);
    return shearStress;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiTensorField3D<T, SymmetricTensor<T, Descriptor>::n>>
OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeShearStress()
{
    return computeShearStress(lattice.getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeAverageVelocityComponent(Box3D domain, plint iComponent)
{
    std::unique_ptr<MultiScalarField3D<T>> density(
        plb::computeVelocityComponent(lattice, domain, iComponent));
    MultiScalarField3D<int> flagMatrix((MultiBlock3D &)voxelizedDomain.getVoxelMatrix());
    int flowType = voxelizedDomain.getFlowType();
    int fluidBorderFlag = voxelFlag::borderFlag(flowType);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  flowType, domain, 1);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  fluidBorderFlag, domain, 1);
    return computeAverage(*density, flagMatrix, 1, domain);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
Array<T, 3> OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeAverageVelocity(Box3D domain)
{
    std::unique_ptr<MultiTensorField3D<T, 3>> velocity(
        plb::computeVelocity(lattice, domain));
    MultiScalarField3D<int> flagMatrix((MultiBlock3D &)voxelizedDomain.getVoxelMatrix());
    int flowType = voxelizedDomain.getFlowType();
    int fluidBorderFlag = voxelFlag::borderFlag(flowType);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  flowType, domain, 1);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  fluidBorderFlag, domain, 1);
    return computeAverage<T, 3>(*velocity, flagMatrix, 1, domain);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeAverageDensity()
{
    return computeAverageDensity(voxelizedDomain.getVoxelMatrix().getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeAverageDensity(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>> density(
        plb::computeDensity(lattice, domain));
    std::unique_ptr<MultiScalarField3D<T>> density2(
        plb::computeDensity(lattice, domain));
    MultiScalarField3D<int> flagMatrix((MultiBlock3D &)voxelizedDomain.getVoxelMatrix());
    int flowType = voxelizedDomain.getFlowType();
    int fluidBorderFlag = voxelFlag::borderFlag(flowType);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  flowType, domain, 1);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  fluidBorderFlag, domain, 1);
    return computeAverage(*density, flagMatrix, 1, domain);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeAverageEnergy()
{
    return computeAverageEnergy(voxelizedDomain.getVoxelMatrix().getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeAverageEnergy(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>> energy(
        plb::computeKineticEnergy(lattice, domain));
    MultiScalarField3D<int> flagMatrix((MultiBlock3D &)voxelizedDomain.getVoxelMatrix());
    int flowType = voxelizedDomain.getFlowType();
    int fluidBorderFlag = voxelFlag::borderFlag(flowType);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  flowType, domain, 1);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  fluidBorderFlag, domain, 1);
    return computeAverage(*energy, flagMatrix, 1, domain);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeRMSvorticity()
{
    return computeRMSvorticity(voxelizedDomain.getVoxelMatrix().getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeRMSvorticity(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>> vorticityNormSqr(
        plb::computeNormSqr(*plb::computeBulkVorticity(*plb::computeVelocity(lattice, domain))));
    MultiScalarField3D<int> flagMatrix((MultiBlock3D &)voxelizedDomain.getVoxelMatrix());
    int flowType = voxelizedDomain.getFlowType();
    int fluidBorderFlag = voxelFlag::borderFlag(flowType);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  flowType, domain, 1);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  fluidBorderFlag, domain, 1);
    return std::sqrt(computeAverage(*vorticityNormSqr, flagMatrix, 1, domain));
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeAverageShearStressNorm()
{
    return computeAverageShearStressNorm(voxelizedDomain.getVoxelMatrix().getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeAverageShearStressNorm(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>> shearStress(
        plb::computeSymmetricTensorNorm(*plb::computeShearStress(lattice, domain)));
    MultiScalarField3D<int> flagMatrix((MultiBlock3D &)voxelizedDomain.getVoxelMatrix());
    int flowType = voxelizedDomain.getFlowType();
    int fluidBorderFlag = voxelFlag::borderFlag(flowType);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  flowType, domain, 1);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  fluidBorderFlag, domain, 1);
    return computeAverage(*shearStress, flagMatrix, 1, domain);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeRMSshearStressNorm()
{
    return computeRMSshearStressNorm(voxelizedDomain.getVoxelMatrix().getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeNearWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeRMSshearStressNorm(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>> shearStressNorm(
        plb::computeSymmetricTensorNorm(*plb::computeShearStress(lattice, domain)));
    T avgShearStress = computeAverageShearStressNorm(domain);

    MultiScalarField3D<int> flagMatrix((MultiBlock3D &)voxelizedDomain.getVoxelMatrix());
    int flowType = voxelizedDomain.getFlowType();
    int fluidBorderFlag = voxelFlag::borderFlag(flowType);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  flowType, domain, 1);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  fluidBorderFlag, domain, 1);
    shearStressNorm = subtract(*shearStressNorm, avgShearStress);
    return std::sqrt(computeAverage(*multiply(*shearStressNorm, *shearStressNorm), flagMatrix, 1, domain));
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::
    OffLatticeImplicitWallBoundaryCondition3D(
        OffLatticeNearWallModel3D<T, BoundaryType> *offLatticeModel_,
        VoxelizedDomain3D<T> &voxelizedDomain_,
        MultiBlockLattice3D<T, Descriptor> &lattice_)
    : voxelizedDomain(voxelizedDomain_),
      lattice(lattice_),
      boundaryShapeArg(lattice_),
      offLatticeModel(offLatticeModel_),
      offLatticePattern(lattice)
{
    // It is very important that the "offLatticePattern" container block
    // has the same multi-block management as the lattice used in the
    // simulation.
    std::vector<MultiBlock3D *> offLatticeIniArg;
    // First argument for compute-off-lattice-pattern.
    offLatticeIniArg.push_back(&offLatticePattern);
    // Remaining arguments for inner-flow-shape.
    offLatticeIniArg.push_back(&voxelizedDomain.getVoxelMatrix());
    offLatticeIniArg.push_back(&voxelizedDomain.getTriangleHash());
    offLatticeIniArg.push_back(&boundaryShapeArg);
    applyProcessingFunctional(
        new OffLatticeImplicitWallPatternFunctional3D<T, BoundaryType>(
            offLatticeModel->clone()),
        offLatticePattern.getBoundingBox(), offLatticeIniArg);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::
    OffLatticeImplicitWallBoundaryCondition3D(
        OffLatticeNearWallModel3D<T, BoundaryType> *offLatticeModel_,
        VoxelizedDomain3D<T> &voxelizedDomain_,
        MultiBlockLattice3D<T, Descriptor> &lattice_,
        MultiParticleField3D<DenseParticleField3D<T, Descriptor>> &particleField_)
    : offLatticeModel(offLatticeModel_),
      voxelizedDomain(voxelizedDomain_),
      lattice(lattice_),
      boundaryShapeArg(particleField_),
      offLatticePattern(lattice)
{
    // It is very important that the "offLatticePattern" container block
    // has the same multi-block management as the lattice used in the
    // simulation.
    std::vector<MultiBlock3D *> offLatticeIniArg;
    // First argument for compute-off-lattice-pattern.
    offLatticeIniArg.push_back(&offLatticePattern);
    // Remaining arguments for inner-flow-shape.
    offLatticeIniArg.push_back(&voxelizedDomain.getVoxelMatrix());
    offLatticeIniArg.push_back(&voxelizedDomain.getTriangleHash());
    offLatticeIniArg.push_back(&boundaryShapeArg);
    applyProcessingFunctional(
        new OffLatticeImplicitWallPatternFunctional3D<T, BoundaryType>(
            offLatticeModel->clone()),
        offLatticePattern.getBoundingBox(), offLatticeIniArg);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::OffLatticeImplicitWallBoundaryCondition3D(
    OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType> const &rhs)
    : offLatticeModel(rhs.offLatticeModel.clone()),
      voxelizedDomain(rhs.voxelizedDomain),
      lattice(rhs.lattice),
      boundaryShapeArg(rhs.boundaryShapeArg),
      offLatticePattern(rhs.offLatticePattern)
{
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::~OffLatticeImplicitWallBoundaryCondition3D()
{
    delete offLatticeModel;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
void OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::insert(plint processorLevel)
{
    // It is very important that the "offLatticePattern" container block
    // has the same multi-block management as the lattice used in the
    // simulation.
    std::vector<MultiBlock3D *> offLatticeArg;
    // First two arguments for Guo algorithm.
    offLatticeArg.push_back(&lattice);
    offLatticeArg.push_back(&offLatticePattern);
    // Remaining arguments for inner-flow-shape.
    offLatticeArg.push_back(&voxelizedDomain.getVoxelMatrix());
    offLatticeArg.push_back(&voxelizedDomain.getTriangleHash());
    offLatticeArg.push_back(&boundaryShapeArg);
    plint numShapeArgs = 3;
    plint numCompletionArgs = 0;
    integrateProcessingFunctional(
        new OffLatticeNearWallCompletionFunctional3D<T, Descriptor, BoundaryType>(
            offLatticeModel->clone(), numShapeArgs, numCompletionArgs),
        boundaryShapeArg.getBoundingBox(), offLatticeArg, processorLevel);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
void OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::insert(
    std::vector<MultiBlock3D *> const &completionArg, plint processorLevel)
{
    // It is very important that the "offLatticePattern" container block
    // has the same multi-block management as the lattice used in the
    // simulation.
    std::vector<MultiBlock3D *> offLatticeArg;
    // First three arguments for Guo algorithm.
    offLatticeArg.push_back(&lattice);
    offLatticeArg.push_back(&offLatticePattern);
    // Next arguments for inner-flow-shape.
    offLatticeArg.push_back(&voxelizedDomain.getVoxelMatrix());
    offLatticeArg.push_back(&voxelizedDomain.getTriangleHash());
    offLatticeArg.push_back(&boundaryShapeArg);
    // Remaining are optional arguments for completion algorithm.
    // 0:rhoVelocityNufield,1:wallDistanceY,2:stencilFlag,3:dynamicID,4:utau,5:stress,6:modifRhoVelocityField
    plint numCompletionArgs = (plint)completionArg.size();
    for (plint i = 0; i < numCompletionArgs; ++i)
    {
        offLatticeArg.push_back(completionArg[i]);
    }
    plint numShapeArgs = 3;
    // compute stencilFlag
    applyProcessingFunctional(
        new boundaryNodeStencilFunctional3D<T, Descriptor>(
            numShapeArgs, numCompletionArgs),
        lattice.getBoundingBox(), offLatticeArg);

    pcout << "compute wall distance and normal on stencil nodes..." << std::endl;
    applyProcessingFunctional(
        new OffLatticeWallDistanceYFunctional3D<T, Descriptor, BoundaryType>(
            offLatticeModel->clone(), numShapeArgs, numCompletionArgs),
        lattice.getBoundingBox(), offLatticeArg);

    pcout << "finish wall distance and normal ..." << std::endl;
    /*
        integrateProcessingFunctional (
                new SetLESCsmagoFunctional3D<T,Descriptor> (
                    numShapeArgs, numCompletionArgs, 0.14),
                lattice.getBoundingBox(), offLatticeArg, 5);
    */

    applyProcessingFunctional(
        new SetLESCsmagoFunctional3D<T, Descriptor>(
            numShapeArgs, numCompletionArgs, 1.0),
        lattice.getBoundingBox(), offLatticeArg);

    integrateProcessingFunctional(
        new OffLatticeNearWallCompletionFunctional3D<T, Descriptor, BoundaryType>(
            offLatticeModel->clone(), numShapeArgs, numCompletionArgs),
        boundaryShapeArg.getBoundingBox(), offLatticeArg, processorLevel);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
void OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::apply()
{
    // It is very important that the "offLatticePattern" container block
    // has the same multi-block management as the lattice used in the
    // simulation.
    std::vector<MultiBlock3D *> offLatticeArg;
    // First three arguments for Guo algorithm.
    offLatticeArg.push_back(&lattice);
    offLatticeArg.push_back(&offLatticePattern);
    // Remaining arguments for inner-flow-shape.
    offLatticeArg.push_back(&voxelizedDomain.getVoxelMatrix());
    offLatticeArg.push_back(&voxelizedDomain.getTriangleHash());
    offLatticeArg.push_back(&boundaryShapeArg);
    plint numShapeArgs = 3;
    plint numCompletionArgs = 0;
    applyProcessingFunctional(
        new OffLatticeNearWallCompletionFunctional3D<T, Descriptor, BoundaryType>(
            offLatticeModel->clone(), numShapeArgs, numCompletionArgs),
        boundaryShapeArg.getBoundingBox(), offLatticeArg);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
void OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::apply(
    std::vector<MultiBlock3D *> const &completionArg)
{
    // It is very important that the "offLatticePattern" container block
    // has the same multi-block management as the lattice used in the
    // simulation.
    std::vector<MultiBlock3D *> offLatticeArg;
    // First three arguments for Guo algorithm.
    offLatticeArg.push_back(&lattice);
    offLatticeArg.push_back(&offLatticePattern);
    // Next arguments for inner-flow-shape.
    offLatticeArg.push_back(&voxelizedDomain.getVoxelMatrix());
    offLatticeArg.push_back(&voxelizedDomain.getTriangleHash());
    offLatticeArg.push_back(&boundaryShapeArg);
    // Remaining are optional arguments for completion algorithm.
    plint numCompletionArgs = (plint)completionArg.size();
    for (plint i = 0; i < numCompletionArgs; ++i)
    {
        offLatticeArg.push_back(completionArg[i]);
    }
    plint numShapeArgs = 3;
    applyProcessingFunctional(
        new OffLatticeNearWallCompletionFunctional3D<T, Descriptor, BoundaryType>(
            offLatticeModel->clone(), numShapeArgs, numCompletionArgs),
        boundaryShapeArg.getBoundingBox(), offLatticeArg);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
Array<T, 9> OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::getForceOnObject()
{
    std::vector<MultiBlock3D *> arg;
    arg.push_back(&offLatticePattern);
    GetForceOnObjectNearWallFunctional3D<T, BoundaryType> functional(offLatticeModel->clone());
    applyProcessingFunctional(
        functional, boundaryShapeArg.getBoundingBox(), arg);
    return functional.getForce();
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiTensorField3D<T, 3>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVelocity(Box3D domain)
{
    std::unique_ptr<MultiTensorField3D<T, 3>> velocity(plb::computeVelocity(lattice, domain));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    setToConstant<T, 3>(*velocity, voxelizedDomain.getVoxelMatrix(), solidFlag,
                        domain, Array<T, 3>(T(), T(), T()));
    setToConstant<T, 3>(*velocity, voxelizedDomain.getVoxelMatrix(), solidBorderFlag,
                        domain, Array<T, 3>(T(), T(), T()));
    return velocity;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiTensorField3D<T, 3>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVelocity()
{
    return computeVelocity(lattice.getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiTensorField3D<T, 3>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVorticity(Box3D domain)
{
    std::unique_ptr<MultiTensorField3D<T, 3>> vorticity(
        plb::computeBulkVorticity(*plb::computeVelocity(lattice, domain), domain));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    setToConstant<T, 3>(*vorticity, voxelizedDomain.getVoxelMatrix(), solidFlag,
                        domain, Array<T, 3>(T(), T(), T()));
    setToConstant<T, 3>(*vorticity, voxelizedDomain.getVoxelMatrix(), solidBorderFlag,
                        domain, Array<T, 3>(T(), T(), T()));
    return vorticity;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiTensorField3D<T, 3>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVorticity()
{
    return computeVorticity(lattice.getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVelocityNorm(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>> velocityNorm(plb::computeVelocityNorm(lattice, domain));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    setToConstant(*velocityNorm, voxelizedDomain.getVoxelMatrix(),
                  solidFlag, domain, (T)0);
    setToConstant(*velocityNorm, voxelizedDomain.getVoxelMatrix(),
                  solidBorderFlag, domain, (T)0);
    return velocityNorm;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVelocityNorm()
{
    return computeVelocityNorm(lattice.getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVorticityNorm(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>> vorticityNorm(
        plb::computeNorm(
            *plb::computeBulkVorticity(*plb::computeVelocity(lattice, domain), domain), domain));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    setToConstant<T>(*vorticityNorm, voxelizedDomain.getVoxelMatrix(), solidFlag, domain, T());
    setToConstant<T>(*vorticityNorm, voxelizedDomain.getVoxelMatrix(), solidBorderFlag, domain, T());
    return vorticityNorm;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVorticityNorm()
{
    return computeVorticityNorm(lattice.getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVelocityComponent(
    Box3D domain, plint iComp)
{
    std::unique_ptr<MultiScalarField3D<T>> velocityComponent(
        plb::computeVelocityComponent(lattice, domain, iComp));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    setToConstant(*velocityComponent, voxelizedDomain.getVoxelMatrix(),
                  solidFlag, domain, (T)0);
    setToConstant(*velocityComponent, voxelizedDomain.getVoxelMatrix(),
                  solidBorderFlag, domain, (T)0);
    return velocityComponent;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeVelocityComponent(plint iComp)
{
    return computeVelocityComponent(lattice.getBoundingBox(), iComp);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computePressure(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>> pressure(plb::computeDensity(lattice, domain));
    T averageDensity = computeAverageDensity(domain);
    subtractInPlace(*pressure, averageDensity, domain);
    multiplyInPlace(*pressure, Descriptor<T>::cs2, domain);
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    setToConstant(*pressure, voxelizedDomain.getVoxelMatrix(),
                  solidFlag, domain, (T)0);
    setToConstant(*pressure, voxelizedDomain.getVoxelMatrix(),
                  solidBorderFlag, domain, (T)0);
    return pressure;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computePressure()
{
    return computePressure(lattice.getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeDensity(Box3D domain, T solidDensity)
{
    std::unique_ptr<MultiScalarField3D<T>> density(plb::computeDensity(lattice, domain));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    setToConstant(*density, voxelizedDomain.getVoxelMatrix(),
                  solidFlag, domain, solidDensity);
    setToConstant(*density, voxelizedDomain.getVoxelMatrix(),
                  solidBorderFlag, domain, solidDensity);
    return density;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeDensity(T solidDensity)
{
    return computeDensity(lattice.getBoundingBox(), solidDensity);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeStrainRateNorm(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>>
        strainRateNorm(computeSymmetricTensorNorm(*plb::computeStrainRateFromStress(lattice, domain)));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    setToConstant(*strainRateNorm, voxelizedDomain.getVoxelMatrix(),
                  solidFlag, domain, (T)0);
    setToConstant(*strainRateNorm, voxelizedDomain.getVoxelMatrix(),
                  solidBorderFlag, domain, (T)0);
    return strainRateNorm;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeStrainRateNorm()
{
    return computeStrainRateNorm(lattice.getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiTensorField3D<T, SymmetricTensor<T, Descriptor>::n>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeStrainRate(Box3D domain)
{
    std::unique_ptr<MultiTensorField3D<T, SymmetricTensor<T, Descriptor>::n>>
        strainRate(plb::computeStrainRateFromStress(lattice, domain));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    Array<T, SymmetricTensor<T, Descriptor>::n> zeros;
    zeros.resetToZero();
    setToConstant<T, SymmetricTensor<T, Descriptor>::n>(*strainRate, voxelizedDomain.getVoxelMatrix(),
                                                        solidFlag, domain, zeros);
    setToConstant<T, SymmetricTensor<T, Descriptor>::n>(*strainRate, voxelizedDomain.getVoxelMatrix(),
                                                        solidBorderFlag, domain, zeros);
    return strainRate;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiTensorField3D<T, SymmetricTensor<T, Descriptor>::n>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeStrainRate()
{
    return computeStrainRate(lattice.getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeShearStressNorm(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>>
        shearStressNorm(computeSymmetricTensorNorm(*plb::computeShearStress(lattice, domain)));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    setToConstant(*shearStressNorm, voxelizedDomain.getVoxelMatrix(),
                  solidFlag, domain, (T)0);
    setToConstant(*shearStressNorm, voxelizedDomain.getVoxelMatrix(),
                  solidBorderFlag, domain, (T)0);
    return shearStressNorm;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiScalarField3D<T>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeShearStressNorm()
{
    return computeShearStressNorm(lattice.getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiTensorField3D<T, SymmetricTensor<T, Descriptor>::n>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeShearStress(Box3D domain)
{
    std::unique_ptr<MultiTensorField3D<T, SymmetricTensor<T, Descriptor>::n>>
        shearStress(plb::computeShearStress(lattice, domain));
    int flowType = voxelizedDomain.getFlowType();
    int solidFlag = voxelFlag::invert(flowType);
    int solidBorderFlag = voxelFlag::borderFlag(solidFlag);
    Array<T, SymmetricTensor<T, Descriptor>::n> zeros;
    zeros.resetToZero();
    setToConstant<T, SymmetricTensor<T, Descriptor>::n>(*shearStress, voxelizedDomain.getVoxelMatrix(),
                                                        solidFlag, domain, zeros);
    setToConstant<T, SymmetricTensor<T, Descriptor>::n>(*shearStress, voxelizedDomain.getVoxelMatrix(),
                                                        solidBorderFlag, domain, zeros);
    return shearStress;
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
std::unique_ptr<MultiTensorField3D<T, SymmetricTensor<T, Descriptor>::n>>
OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeShearStress()
{
    return computeShearStress(lattice.getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeAverageVelocityComponent(Box3D domain, plint iComponent)
{
    std::unique_ptr<MultiScalarField3D<T>> density(
        plb::computeVelocityComponent(lattice, domain, iComponent));
    MultiScalarField3D<int> flagMatrix((MultiBlock3D &)voxelizedDomain.getVoxelMatrix());
    int flowType = voxelizedDomain.getFlowType();
    int fluidBorderFlag = voxelFlag::borderFlag(flowType);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  flowType, domain, 1);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  fluidBorderFlag, domain, 1);
    return computeAverage(*density, flagMatrix, 1, domain);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
Array<T, 3> OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeAverageVelocity(Box3D domain)
{
    std::unique_ptr<MultiTensorField3D<T, 3>> velocity(
        plb::computeVelocity(lattice, domain));
    MultiScalarField3D<int> flagMatrix((MultiBlock3D &)voxelizedDomain.getVoxelMatrix());
    int flowType = voxelizedDomain.getFlowType();
    int fluidBorderFlag = voxelFlag::borderFlag(flowType);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  flowType, domain, 1);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  fluidBorderFlag, domain, 1);
    return computeAverage<T, 3>(*velocity, flagMatrix, 1, domain);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeAverageDensity()
{
    return computeAverageDensity(voxelizedDomain.getVoxelMatrix().getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeAverageDensity(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>> density(
        plb::computeDensity(lattice, domain));
    std::unique_ptr<MultiScalarField3D<T>> density2(
        plb::computeDensity(lattice, domain));
    MultiScalarField3D<int> flagMatrix((MultiBlock3D &)voxelizedDomain.getVoxelMatrix());
    int flowType = voxelizedDomain.getFlowType();
    int fluidBorderFlag = voxelFlag::borderFlag(flowType);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  flowType, domain, 1);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  fluidBorderFlag, domain, 1);
    return computeAverage(*density, flagMatrix, 1, domain);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeAverageEnergy()
{
    return computeAverageEnergy(voxelizedDomain.getVoxelMatrix().getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeAverageEnergy(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>> energy(
        plb::computeKineticEnergy(lattice, domain));
    MultiScalarField3D<int> flagMatrix((MultiBlock3D &)voxelizedDomain.getVoxelMatrix());
    int flowType = voxelizedDomain.getFlowType();
    int fluidBorderFlag = voxelFlag::borderFlag(flowType);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  flowType, domain, 1);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  fluidBorderFlag, domain, 1);
    return computeAverage(*energy, flagMatrix, 1, domain);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeRMSvorticity()
{
    return computeRMSvorticity(voxelizedDomain.getVoxelMatrix().getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeRMSvorticity(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>> vorticityNormSqr(
        plb::computeNormSqr(*plb::computeBulkVorticity(*plb::computeVelocity(lattice, domain))));
    MultiScalarField3D<int> flagMatrix((MultiBlock3D &)voxelizedDomain.getVoxelMatrix());
    int flowType = voxelizedDomain.getFlowType();
    int fluidBorderFlag = voxelFlag::borderFlag(flowType);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  flowType, domain, 1);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  fluidBorderFlag, domain, 1);
    return std::sqrt(computeAverage(*vorticityNormSqr, flagMatrix, 1, domain));
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeAverageShearStressNorm()
{
    return computeAverageShearStressNorm(voxelizedDomain.getVoxelMatrix().getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeAverageShearStressNorm(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>> shearStress(
        plb::computeSymmetricTensorNorm(*plb::computeShearStress(lattice, domain)));
    MultiScalarField3D<int> flagMatrix((MultiBlock3D &)voxelizedDomain.getVoxelMatrix());
    int flowType = voxelizedDomain.getFlowType();
    int fluidBorderFlag = voxelFlag::borderFlag(flowType);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  flowType, domain, 1);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  fluidBorderFlag, domain, 1);
    return computeAverage(*shearStress, flagMatrix, 1, domain);
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeRMSshearStressNorm()
{
    return computeRMSshearStressNorm(voxelizedDomain.getVoxelMatrix().getBoundingBox());
}

template <typename T,
          template <typename U> class Descriptor,
          class BoundaryType>
T OffLatticeImplicitWallBoundaryCondition3D<T, Descriptor, BoundaryType>::computeRMSshearStressNorm(Box3D domain)
{
    std::unique_ptr<MultiScalarField3D<T>> shearStressNorm(
        plb::computeSymmetricTensorNorm(*plb::computeShearStress(lattice, domain)));
    T avgShearStress = computeAverageShearStressNorm(domain);

    MultiScalarField3D<int> flagMatrix((MultiBlock3D &)voxelizedDomain.getVoxelMatrix());
    int flowType = voxelizedDomain.getFlowType();
    int fluidBorderFlag = voxelFlag::borderFlag(flowType);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  flowType, domain, 1);
    setToConstant(flagMatrix, voxelizedDomain.getVoxelMatrix(),
                  fluidBorderFlag, domain, 1);
    shearStressNorm = subtract(*shearStressNorm, avgShearStress);
    return std::sqrt(computeAverage(*multiply(*shearStressNorm, *shearStressNorm), flagMatrix, 1, domain));
}

template <typename T, class SurfaceData>
OffLatticeImplicitWallPatternFunctional3D<T, SurfaceData>::
    OffLatticeImplicitWallPatternFunctional3D(
        OffLatticeNearWallModel3D<T, SurfaceData> *offLatticeModel_)
    : offLatticeModel(offLatticeModel_)
{
}

template <typename T, class SurfaceData>
OffLatticeImplicitWallPatternFunctional3D<T, SurfaceData>::~OffLatticeImplicitWallPatternFunctional3D()
{
    delete offLatticeModel;
}

template <typename T, class SurfaceData>
OffLatticeImplicitWallPatternFunctional3D<T, SurfaceData>::
    OffLatticeImplicitWallPatternFunctional3D(
        OffLatticeImplicitWallPatternFunctional3D<T, SurfaceData> const &rhs)
    : offLatticeModel(rhs.offLatticeModel->clone())
{
}

template <typename T, class SurfaceData>
OffLatticeImplicitWallPatternFunctional3D<T, SurfaceData> &
OffLatticeImplicitWallPatternFunctional3D<T, SurfaceData>::operator=(
    OffLatticeImplicitWallPatternFunctional3D<T, SurfaceData> const &rhs)
{
    OffLatticeImplicitWallPatternFunctional3D<T, SurfaceData>(rhs).swap(*this);
    return *this;
}

template <typename T, class SurfaceData>
void OffLatticeImplicitWallPatternFunctional3D<T, SurfaceData>::swap(
    OffLatticeImplicitWallPatternFunctional3D<T, SurfaceData> &rhs)
{
    std::swap(offLatticeModel, rhs.offLatticeModel);
}

template <typename T, class SurfaceData>
OffLatticeImplicitWallPatternFunctional3D<T, SurfaceData> *
OffLatticeImplicitWallPatternFunctional3D<T, SurfaceData>::clone() const
{
    return new OffLatticeImplicitWallPatternFunctional3D<T, SurfaceData>(*this);
}

template <typename T, class SurfaceData>
void OffLatticeImplicitWallPatternFunctional3D<T, SurfaceData>::getTypeOfModification(
    std::vector<modif::ModifT> &modified) const
{
    // It is very important that the "offLatticePattern" container block
    // (the first atomic-block passed) has the same multi-block management
    // as the lattice used in the simulation.
    modified[0] = modif::staticVariables; // Container.
    // Possible additional parameters for the shape function are read-only.
    for (pluint i = 1; i < modified.size(); ++i)
    {
        modified[i] = modif::nothing;
    }
}

template <typename T, class SurfaceData>
BlockDomain::DomainT OffLatticeImplicitWallPatternFunctional3D<T, SurfaceData>::appliesTo() const
{
    return BlockDomain::bulk;
}

template <typename T, class SurfaceData>
void OffLatticeImplicitWallPatternFunctional3D<T, SurfaceData>::processGenericBlocks(
    Box3D domain, std::vector<AtomicBlock3D *> fields)
{
    PLB_PRECONDITION(fields.size() >= 1);
    AtomicContainerBlock3D *container =
        dynamic_cast<AtomicContainerBlock3D *>(fields[0]);
    PLB_ASSERT(container);
    ContainerBlockData *storeInfo =
        offLatticeModel->generateOffLatticeInfo();
    container->setData(storeInfo);

    if (fields.size() > 1)
    {
        std::vector<AtomicBlock3D *> shapeParameters(fields.size() - 1);
        for (pluint i = 0; i < shapeParameters.size(); ++i)
        {
            shapeParameters[i] = fields[i + 1];
        }
        offLatticeModel->provideShapeArguments(shapeParameters);
    }

    for (plint iX = domain.x0; iX <= domain.x1; ++iX)
    {
        for (plint iY = domain.y0; iY <= domain.y1; ++iY)
        {
            for (plint iZ = domain.z0; iZ <= domain.z1; ++iZ)
            {
                offLatticeModel->prepareCell(Dot3D(iX, iY, iZ), *container);
            }
        }
    }
}

template <typename T, class SurfaceData>
GetForceOnAhmedFunctional3D<T, SurfaceData>::GetForceOnAhmedFunctional3D(
    OffLatticeNearWallModel3D<T, SurfaceData> *offLatticeModel_)
    : offLatticeModel(offLatticeModel_),
      forceId(
          this->getStatistics().subscribeSum(),
          this->getStatistics().subscribeSum(),
          this->getStatistics().subscribeSum(),
          this->getStatistics().subscribeSum(),
          this->getStatistics().subscribeSum(),
          this->getStatistics().subscribeSum(),
          this->getStatistics().subscribeSum(),
          this->getStatistics().subscribeSum(),
          this->getStatistics().subscribeSum())
{
}

template <typename T, class SurfaceData>
GetForceOnAhmedFunctional3D<T, SurfaceData>::~GetForceOnAhmedFunctional3D()
{
    delete offLatticeModel;
}

template <typename T, class SurfaceData>
GetForceOnAhmedFunctional3D<T, SurfaceData>::GetForceOnAhmedFunctional3D(
    GetForceOnAhmedFunctional3D<T, SurfaceData> const &rhs)
    : PlainReductiveBoxProcessingFunctional3D(rhs),
      offLatticeModel(rhs.offLatticeModel->clone()),
      forceId(rhs.forceId)
{
}

template <typename T, class SurfaceData>
GetForceOnAhmedFunctional3D<T, SurfaceData> &
GetForceOnAhmedFunctional3D<T, SurfaceData>::operator=(
    GetForceOnAhmedFunctional3D<T, SurfaceData> const &rhs)
{
    delete offLatticeModel;
    offLatticeModel = rhs.offLatticeModel->clone();
    forceId = rhs.forceId;
    PlainReductiveBoxProcessingFunctional3D::operator=(rhs);
    return *this;
}

template <typename T, class SurfaceData>
void GetForceOnAhmedFunctional3D<T, SurfaceData>::processGenericBlocks(
    Box3D domain, std::vector<AtomicBlock3D *> fields)
{
    PLB_PRECONDITION(fields.size() == 1);
    AtomicContainerBlock3D *offLatticeInfo =
        dynamic_cast<AtomicContainerBlock3D *>(fields[0]);
    PLB_ASSERT(offLatticeInfo);

    Array<T, 9> force = offLatticeModel->getLocalForce(*offLatticeInfo);
    for (plint i = 0; i < 9; i++)
    {
        this->getStatistics().gatherSum(forceId[i], force[i]);
    }
}

template <typename T, class SurfaceData>
GetForceOnAhmedFunctional3D<T, SurfaceData> *
GetForceOnAhmedFunctional3D<T, SurfaceData>::clone() const
{
    return new GetForceOnAhmedFunctional3D<T, SurfaceData>(*this);
}

template <typename T, class SurfaceData>
void GetForceOnAhmedFunctional3D<T, SurfaceData>::getTypeOfModification(std::vector<modif::ModifT> &modified) const
{
    modified[0] = modif::nothing; // Off-lattice info.
}

template <typename T, class SurfaceData>
BlockDomain::DomainT GetForceOnAhmedFunctional3D<T, SurfaceData>::appliesTo() const
{
    return BlockDomain::bulk;
}

template <typename T, class SurfaceData>
Array<T, 9> GetForceOnAhmedFunctional3D<T, SurfaceData>::getForce() const
{
    Array<T, 9> AForce;
    for (plint i = 0; i < 9; i++)
    {
        AForce[i] = this->getStatistics().getSum(forceId[i]);
    }
    return AForce;
}

template <typename T, class BoundaryType>
Array<T, 9> getForceOnAhmed(
    MultiBlock3D &offLatticePattern,
    OffLatticeNearWallModel3D<T, BoundaryType> const &offLatticeModel)
{
    std::vector<MultiBlock3D *> arg;
    arg.push_back(&offLatticePattern);
    GetForceOnAhmedFunctional3D<T, BoundaryType> functional(offLatticeModel.clone());
    applyProcessingFunctional(
        functional, offLatticePattern.getBoundingBox(), arg);
    return functional.getForce();
}

template <typename T, template <typename U> class Descriptor>
boundaryNodeStencilFunctional3D<T, Descriptor>::boundaryNodeStencilFunctional3D(int numShapeArgs_, int numCompletionArgs_)
    : numShapeArgs(numShapeArgs_),
      numCompletionArgs(numCompletionArgs_)
{
}

template <typename T, template <typename U> class Descriptor>
void boundaryNodeStencilFunctional3D<T, Descriptor>::processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D *> blocks)
{
    PLB_ASSERT(blocks.size() == 2 + numShapeArgs + numCompletionArgs);
    typedef Descriptor<T> D;
    // numCompletionArgs:0:rhoVelocityNufield,1:wallDistanceY,2:stencilFlag,3:dynamicID,4:utau, 5:stressTensorField,6:modifRhoVelocityField
    BlockLattice3D<T, Descriptor> *lattice = dynamic_cast<BlockLattice3D<T, Descriptor> *>(blocks[0]);
    ScalarField3D<int> *stencilField = dynamic_cast<ScalarField3D<int> *>(blocks[2 + numShapeArgs + 2]);
    ScalarField3D<int> *dynamicIDField = dynamic_cast<ScalarField3D<int> *>(blocks[2 + numShapeArgs + 3]);

    PLB_ASSERT(lattice);

    Dot3D offsetStencil = computeRelativeDisplacement(*lattice, *stencilField);
    Dot3D offsetDynamicID = computeRelativeDisplacement(*lattice, *dynamicIDField);

    Dot3D absOff = lattice->getLocation();
    int noDynId = NoDynamics<T, Descriptor>().getId();
    for (plint iX = domain.x0; iX <= domain.x1; ++iX)
    {
        for (plint iY = domain.y0; iY <= domain.y1; ++iY)
        {
            for (plint iZ = domain.z0; iZ <= domain.z1; ++iZ)
            {
                const int dynamicID = dynamicIDField->get(iX + offsetDynamicID.x, iY + offsetDynamicID.y, iZ + offsetDynamicID.z);
/*
#ifdef GEN_GRID_DAT
                //Just for generating grid.dat
                stencilField->get(iX + offsetStencil.x, iY + offsetStencil.y, iZ + offsetStencil.z) = 1;
#endif                
*/
                // only consider fluid node
                if (noDynId == dynamicID)
                {
#ifdef GEN_GRID_DAT                    
                    // This section is for generating grid.dat
                    bool allNeighborSolid = true;
                    for (int qID = 0; qID < D::q; qID++)
                    {
                        const Dot3D neighbor(iX + D::c[qID][0], iY + D::c[qID][1], iZ + D::c[qID][2]);
                        const int dynamicIDNgb = dynamicIDField->get(neighbor.x + offsetDynamicID.x, neighbor.y + offsetDynamicID.y, neighbor.z + offsetDynamicID.z);
                        // fluid node owns solid nodes, it is a boundary node
                        if (noDynId != dynamicIDNgb)
                        {    
                            allNeighborSolid = false;
                            break;
                        }
                    }
                    if(allNeighborSolid) stencilField->get(iX + offsetStencil.x, iY + offsetStencil.y, iZ + offsetStencil.z) = -1;
#endif
                    continue;
                }

                bool isBoundaryNode = false;
                for (int qID = 0; qID < D::q; qID++)
                {
                    const Dot3D neighbor(iX + D::c[qID][0], iY + D::c[qID][1], iZ + D::c[qID][2]);
                    const int dynamicIDNgb = dynamicIDField->get(neighbor.x + offsetDynamicID.x, neighbor.y + offsetDynamicID.y, neighbor.z + offsetDynamicID.z);
                    // fluid node owns solid nodes, it is a boundary node
                    if (noDynId == dynamicIDNgb) {
                        stencilField->get(iX + offsetStencil.x, iY + offsetStencil.y, iZ + offsetStencil.z) = 1;
                        isBoundaryNode = true;
#ifdef GEN_GRID_DAT
                        for (int i = 0; i < D::q; i++)
                        {
                            stencilField->get(iX + D::c[i][0] + offsetStencil.x, iY + D::c[i][1] + offsetStencil.y, iZ + D::c[i][2] + offsetStencil.z) = 1;
                        }
#endif
                        break;
                    }
                }
            }
        }
    }
}

template <typename T, template <typename U> class Descriptor>
boundaryNodeStencilFunctional3D<T, Descriptor> *boundaryNodeStencilFunctional3D<T, Descriptor>::clone() const
{
    return new boundaryNodeStencilFunctional3D<T, Descriptor>(*this);
}

template <typename T, template <typename U> class Descriptor>
void boundaryNodeStencilFunctional3D<T, Descriptor>::getTypeOfModification(std::vector<modif::ModifT> &modified) const
{
    for (int item = 0; item < 2 + numShapeArgs + numCompletionArgs - 1; item++)
    {
        modified[item] = modif::nothing;
    }
    // stencilField should be modified
    modified[2 + numShapeArgs + 2] = modif::staticVariables;
}

template <typename T, template <typename U> class Descriptor>
BlockDomain::DomainT boundaryNodeStencilFunctional3D<T, Descriptor>::appliesTo() const
{
    return BlockDomain::bulk;
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
OffLatticeWallDistanceYFunctional3D<T, Descriptor, SurfaceData>::OffLatticeWallDistanceYFunctional3D(
    OffLatticeNearWallModel3D<T, SurfaceData> *offLatticeModel_,
    plint numShapeArgs_, plint numCompletionArgs_)
    : offLatticeModel(offLatticeModel_),
      numShapeArgs(numShapeArgs_),
      numCompletionArgs(numCompletionArgs_)
{
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
OffLatticeWallDistanceYFunctional3D<T, Descriptor, SurfaceData>::~OffLatticeWallDistanceYFunctional3D()
{
    delete offLatticeModel;
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
OffLatticeWallDistanceYFunctional3D<T, Descriptor, SurfaceData>::
    OffLatticeWallDistanceYFunctional3D(
        OffLatticeWallDistanceYFunctional3D<T, Descriptor, SurfaceData> const &rhs)
    : offLatticeModel(rhs.offLatticeModel->clone()),
      numShapeArgs(rhs.numShapeArgs),
      numCompletionArgs(rhs.numCompletionArgs)
{
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
OffLatticeWallDistanceYFunctional3D<T, Descriptor, SurfaceData> &
OffLatticeWallDistanceYFunctional3D<T, Descriptor, SurfaceData>::operator=(
    OffLatticeWallDistanceYFunctional3D<T, Descriptor, SurfaceData> const &rhs)
{
    OffLatticeWallDistanceYFunctional3D<T, Descriptor, SurfaceData>(rhs).swap(*this);
    return *this;
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
void OffLatticeWallDistanceYFunctional3D<T, Descriptor, SurfaceData>::swap(
    OffLatticeWallDistanceYFunctional3D<T, Descriptor, SurfaceData> &rhs)
{
    std::swap(offLatticeModel, rhs.offLatticeModel);
    std::swap(numShapeArgs, rhs.numShapeArgs);
    std::swap(numCompletionArgs, rhs.numCompletionArgs);
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
OffLatticeWallDistanceYFunctional3D<T, Descriptor, SurfaceData> *
OffLatticeWallDistanceYFunctional3D<T, Descriptor, SurfaceData>::clone() const
{
    return new OffLatticeWallDistanceYFunctional3D<T, Descriptor, SurfaceData>(*this);
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
void OffLatticeWallDistanceYFunctional3D<T, Descriptor, SurfaceData>::getTypeOfModification(
    std::vector<modif::ModifT> &modified) const
{
    PLB_ASSERT((plint)modified.size() == 2 + numShapeArgs + numCompletionArgs);
    modified[0] = modif::nothing; // Lattice.
    // It is very important that the "offLatticePattern" container block
    // which is passed as the second atomic-block with the off-lattice info
    // has the same multi-block management as the lattice used in the simulation.
    modified[1] = modif::nothing; // Container for wet/dry nodes.
    // Possible additional parameters for the shape function and
    //   for the completion algorithm are read-only.
    for (pluint i = 2; i < modified.size(); ++i)
    {
        modified[i] = modif::nothing;
    }
    // wallDistanceY is updated
    modified[2 + numShapeArgs + 1] = modif::staticVariables;
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
BlockDomain::DomainT OffLatticeWallDistanceYFunctional3D<T, Descriptor, SurfaceData>::appliesTo() const
{
    return BlockDomain::bulk;
}

template <typename T, template <typename U> class Descriptor, class SurfaceData>
void OffLatticeWallDistanceYFunctional3D<T, Descriptor, SurfaceData>::processGenericBlocks(
    Box3D domain, std::vector<AtomicBlock3D *> fields)
{
    PLB_PRECONDITION((plint)fields.size() == 2 + numShapeArgs + numCompletionArgs);
    AtomicBlock3D *lattice = fields[0];
    PLB_ASSERT(lattice);

    // It is very important that the "offLatticePattern" container block
    // which is passed as the second atomic-block with the off-lattice info
    // has the same multi-block management as the lattice used in the simulation.
    AtomicContainerBlock3D *container = // Container for wet/dry nodes.
        dynamic_cast<AtomicContainerBlock3D *>(fields[1]);
    PLB_ASSERT(container);

    if (numShapeArgs > 0)
    {
        std::vector<AtomicBlock3D *> shapeParameters(numShapeArgs);
        for (plint i = 0; i < numShapeArgs; ++i)
        {
            shapeParameters[i] = fields[i + 2];
        }
        // It is necessary, otherwise, offLatticeModel does not own HashContainer, etc.
        offLatticeModel->provideShapeArguments(shapeParameters);
    }
    // 0:rhoVelocityNufield,1:wallDistanceY,2:stencilFlag,3:dynamicID,4:utau
    std::vector<AtomicBlock3D *> completionParameters(numCompletionArgs);
    for (plint i = 0; i < numCompletionArgs; ++i)
    {
        completionParameters[i] = fields[i + 2 + numShapeArgs];
    }

    BlockLattice3D<T, Descriptor> &lattice2 =
        dynamic_cast<BlockLattice3D<T, Descriptor> &>(*lattice);
    TensorField3D<T, 3> *wallDistanceY = dynamic_cast<TensorField3D<T, 3> *>(completionParameters[1]);
    Dot3D offsetWallDistanceY = computeRelativeDisplacement(lattice2, *wallDistanceY);
    ScalarField3D<int> *dynamicIDField = dynamic_cast<ScalarField3D<int> *>(completionParameters[3]);
    Dot3D offsetDynamicID = computeRelativeDisplacement(lattice2, *dynamicIDField);
    int noDynId = NoDynamics<T, Descriptor>().getId();

    Dot3D absOffset = lattice2.getLocation();
    T minut = 5.0;
    T maxut = 0.0;
    for (plint iX = domain.x0; iX <= domain.x1; ++iX)
    {
        for (plint iY = domain.y0; iY <= domain.y1; ++iY)
        {
            for (plint iZ = domain.z0; iZ <= domain.z1; ++iZ)
            {
                offLatticeModel->distanceYNormalCell(Dot3D(iX, iY, iZ), minut, maxut, *lattice, completionParameters);

                /*
                int dynamicID = dynamicIDField->get(iX + offsetDynamicID.x, iY + offsetDynamicID.y, iZ + offsetDynamicID.z);
                if (noDynId == dynamicID)
                    continue;
                Dot3D cellLocation = Dot3D(iX, iY, iZ);
                T distance;
                bool isBehind;
                bool ok = offLatticeModel->distanceToSurface(cellLocation + absOffset, distance, isBehind);
                if (!ok || !std::isfinite(distance))
                    distance = 4.01;
                Array<T, 3> &wallDistance = wallDistanceY->get(cellLocation.x + offsetWallDistanceY.x, cellLocation.y + offsetWallDistanceY.y, cellLocation.z + offsetWallDistanceY.z);
                wallDistance[0] = distance;
                wallDistance[1] = 0.0;
                wallDistance[2] = 0.0;
                */
            }
        }
    }
}

template <typename T, template <typename U> class Descriptor>
SetLESCsmagoFunctional3D<T, Descriptor>::SetLESCsmagoFunctional3D(int numShapeArgs_, int numCompletionArgs_, T cSmago_)
    : numShapeArgs(numShapeArgs_),
      numCompletionArgs(numCompletionArgs_),
      cSmago(cSmago_)
{
}

template <typename T, template <typename U> class Descriptor>
void SetLESCsmagoFunctional3D<T, Descriptor>::processGenericBlocks(
    Box3D domain, std::vector<AtomicBlock3D *> blocks)
{

    PLB_ASSERT(blocks.size() == 2 + numShapeArgs + numCompletionArgs);
    // numCompletionArgs:0:rhoVelocityNufield,1:wallDistanceY,2:stencilFlag,3:dynamicID,4:utau, 5:stressTensorField

    BlockLattice3D<T, Descriptor> *lattice = dynamic_cast<BlockLattice3D<T, Descriptor> *>(blocks[0]);
    TensorField3D<T, 3> const *wallDistanceY = dynamic_cast<TensorField3D<T, 3> const *>(blocks[2 + numShapeArgs + 1]);
    ScalarField3D<int> const *stencilField = dynamic_cast<ScalarField3D<int> const *>(blocks[2 + numShapeArgs + 2]);
    Dot3D offsetStencil = computeRelativeDisplacement(*lattice, *stencilField);
    Dot3D offsetWallDistanceY = computeRelativeDisplacement(*lattice, *wallDistanceY);

    int noDynId = NoDynamics<T, Descriptor>().getId();
    // Set distanceIP, KK, alphaDistance for different Reynolds number and simulations.
    T distanceIP = 3.5;
    // T distanceIP = 2.5;

    // T KK = 0.41;
    T KK = 0.0;
    // T KK = 0.30;
    T alphaDistance = 0.4;
    // T alphaDistance = 0.4;
    T distanceCRT = alphaDistance * distanceIP;
    T distanceCRT2 = 2.0 * alphaDistance * distanceIP;

    for (plint iX = domain.x0; iX <= domain.x1; ++iX)
    {
        for (plint iY = domain.y0; iY <= domain.y1; ++iY)
        {
            for (plint iZ = domain.z0; iZ <= domain.z1; ++iZ)
            {
                int stencilFlag = stencilField->get(iX + offsetStencil.x, iY + offsetStencil.y, iZ + offsetStencil.z);
                // only modify csmago of stencil nodes including boundary nodes and donor nodes
                // if (0 == stencilFlag) continue;

                Cell<T, Descriptor> &cell = lattice->get(iX, iY, iZ);
                Dynamics<T, Descriptor> &dynamic = cell.getDynamics();
                // Nodynamics, no turbulence model (solid node)
                if (dynamic.getId() == noDynId)
                    continue;
                Array<T, 3> distanceNormal = wallDistanceY->get(iX + offsetWallDistanceY.x, iY + offsetWallDistanceY.y, iZ + offsetWallDistanceY.z);
                T distanceY = std::sqrt(distanceNormal[0] * distanceNormal[0] + distanceNormal[1] * distanceNormal[1] + distanceNormal[2] * distanceNormal[2]);

                T cSmagoNew = cSmago;
                T kConstant = 0.0;
                // only consider cells close to body
                if (distanceY > distanceIP) {
                    cell.setExternalField(Descriptor<T>::ExternalField::smagoBeginsAt, 1, &cSmagoNew);
                    continue;
                }

                // distanceY > distanceIP: LES model
                // nutLES = csmago*csmago*||S||


                // RANS model
                // nutRANS = (ka*d_crt)*(ka*d_crt)*||S||
                if ((0 < distanceY) && (distanceY <= distanceCRT))
                    cSmagoNew = KK * distanceCRT;
                if ((0 < distanceY) && (distanceY <= distanceCRT2))
                    kConstant = 0.0; // KK * distanceCRT;
                // Hybrid model: RANS+LES
                // nutHybrid = sqrt((ka*Y)*(ka*Y)*K^gamma + cSmago*cSmago*(1.0-K^gamma))*sqrt((ka*Y)*(ka*Y)*K^gamma + cSmago*cSmago*(1.0-K^gamma))*||S||

                if ((distanceCRT < distanceY) && (distanceY <= distanceIP))
                {
                    T ratio = (distanceIP - distanceY) / (distanceIP - distanceCRT);
                    T coefK = (std::max)((std::min)(ratio, (T)1.0), (T)0.0);
                    T gamma = 2.0;
                    cSmagoNew = std::sqrt((KK * distanceY) * (KK * distanceY) * pow(coefK, gamma) + cSmago * cSmago * (1.0 - pow(coefK, gamma)));
                }
                if ((distanceCRT2 < distanceY) && (distanceY <= distanceIP))
                {
                    T ratio = (distanceIP - distanceY) / (distanceIP - distanceCRT2);
                    T coefK = (std::max)((std::min)(ratio, (T)1.0), (T)0.0);
                    T gamma = 2.0;
                    kConstant = (1.0 - pow(coefK, gamma));
                }
                /*
                                //Van Driest damping function
                                                T utauNode = utauField->get(iX+offsettauField.x, iY+offsettauField.y, iZ+offsettauField.z);
                                                const T * rhoVelocityNu = rhoVelocityNuField->get(iX+offsetrhoVelocityNu.x,iY+offsetrhoVelocityNu.y,iZ+offsetrhoVelocityNu.z);
                                                T nuNode = rhoVelocityNu[4];
                                                T yPlus = distanceY * utauNode / nuNode;
                                                T ePart = 1.0 - std::exp(-yPlus/26.0);
                                                T vanDriest = KK*distanceY/0.158*ePart;
                                                if (fabs(vanDriest-1.0)<1.0e-12) cSmagoNew = cSmagoNew * vanDriest;
                */
                //dynamic.setParameter(dynamicParams::smagorinskyConstant, cSmagoNew);
                cell.setExternalField(Descriptor<T>::ExternalField::smagoBeginsAt, 1, &cSmagoNew);
                //dynamic.setParameter(dynamicParams::KConstant, kConstant);
            }
        }
    }
}

template <typename T, template <typename U> class Descriptor>
SetLESCsmagoFunctional3D<T, Descriptor> *SetLESCsmagoFunctional3D<T, Descriptor>::clone() const
{
    return new SetLESCsmagoFunctional3D<T, Descriptor>(*this);
}

template <typename T, template <typename U> class Descriptor>
void SetLESCsmagoFunctional3D<T, Descriptor>::getTypeOfModification(std::vector<modif::ModifT> &modified) const
{
    modified[0] = modif::staticVariables;
    modified[1] = modif::nothing;
}

template<typename T, template<typename U> class Descriptor>
ModifyInitialStatFunction3D<T,Descriptor>::ModifyInitialStatFunction3D( T maxDistance_ )
    : maxDistance(maxDistance_)
{ }

template<typename T, template<typename U> class Descriptor>
void ModifyInitialStatFunction3D<T,Descriptor>::processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> blocks)
{
    BlockLattice3D<T,Descriptor> *lattice = dynamic_cast<BlockLattice3D<T,Descriptor>*>(blocks[0]);
    Dot3D offset = lattice->getLocation();
    TensorField3D<T, 3> *yDistance = dynamic_cast<TensorField3D<T, 3> *>(blocks[1]);
    Dot3D yDistanceOffset = computeRelativeDisplacement(*lattice, *yDistance);
    for(plint iX=domain.x0; iX<=domain.x1; ++iX) {
        for(plint iY=domain.y0; iY<=domain.y1; ++iY) {
            for(plint iZ=domain.z0; iZ<=domain.z1; ++iZ) {
                Array<T,3> wallDistance = yDistance->get(iX+yDistanceOffset.x, iY+yDistanceOffset.y, iZ+yDistanceOffset.z);
                T distance = std::sqrt(wallDistance[0]*wallDistance[0]+wallDistance[1]*wallDistance[1]+wallDistance[2]*wallDistance[2]);
                wallDistance[0] /= distance;
                wallDistance[1] /= distance;
                wallDistance[2] /= distance;

                if (distance < maxDistance) {
                    Cell<T,Descriptor>& cell = lattice->get(iX,iY,iZ);
                    T rhoBar, jSqr;
                    Array<T,3> j;
                    Array<T,Descriptor<T>::q> fEqOld, fEqNew;
                    momentTemplates<T,Descriptor>::get_rhoBar_j(cell, rhoBar, j);
                    cell.getDynamics().computeEquilibria(fEqOld, rhoBar, j, jSqr);
                    T jPer = j[0]*wallDistance[0]+j[1]*wallDistance[1]+j[2]*wallDistance[2];
                    j[0] -= jPer*wallDistance[0]*distance/maxDistance;
                    j[1] -= jPer*wallDistance[1]*distance/maxDistance;
                    j[2] -= jPer*wallDistance[2]*distance/maxDistance;
                    cell.getDynamics().computeEquilibria(fEqNew, rhoBar, j, jSqr);
                    for(plint iPop=0; iPop<Descriptor<T>::q; ++iPop) {
                        cell[iPop] += fEqNew[iPop]-fEqOld[iPop];
                    }
                }
            }
        }
    }
}

template<typename T, template<typename U> class Descriptor>
GenDatFunction3D<T,Descriptor>::GenDatFunction3D( T maxDistance_, T dx_, T height_ )
    : maxDistance(maxDistance_), dx(dx_), height(height_)
{ }

template<typename T, template<typename U> class Descriptor>
void GenDatFunction3D<T,Descriptor>::processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D*> blocks)
{
    typedef Descriptor<T> D;
    BlockLattice3D<T,Descriptor> *lattice = dynamic_cast<BlockLattice3D<T,Descriptor>*>(blocks[0]);
    Dot3D offset = lattice->getLocation();
    TensorField3D<T, 3> *yDistance = dynamic_cast<TensorField3D<T, 3> *>(blocks[1]);
    Dot3D yDistanceOffset = computeRelativeDisplacement(*lattice, *yDistance);
    ScalarField3D<int> *stencilField = dynamic_cast<ScalarField3D<int> *>(blocks[2]);
    Dot3D offsetStencil = computeRelativeDisplacement(*lattice, *stencilField);

    int noDynId = NoDynamics<T, Descriptor>().getId();
    
    for(plint iX=domain.x0; iX<=domain.x1; ++iX) {
        for(plint iY=domain.y0; iY<=domain.y1; ++iY) {
            for(plint iZ=domain.z0; iZ<=domain.z1; ++iZ) {
                const int dynamicID = lattice->get(iX, iY, iZ).getDynamics().getId();
                Array<T,3> wallDistance = yDistance->get(iX+yDistanceOffset.x, iY+yDistanceOffset.y, iZ+yDistanceOffset.z);
                T distance = std::sqrt(wallDistance[0]*wallDistance[0]+wallDistance[1]*wallDistance[1]+wallDistance[2]*wallDistance[2]);

                if (noDynId == dynamicID)
                {
                    // This section is for generating grid.dat
                    bool allNeighborSolid = true;
                    for (int qID = 0; qID < D::q; qID++)
                    {
                        const Dot3D neighbor(iX + D::c[qID][0], iY + D::c[qID][1], iZ + D::c[qID][2]);
                        const int dynamicIDNgb = lattice->get(neighbor.x, neighbor.y, neighbor.z).getDynamics().getId();
                        // fluid node owns solid nodes, it is a boundary node
                        if (noDynId != dynamicIDNgb)
                        {    
                            allNeighborSolid = false;
                            break;
                        }
                    }
                    if(allNeighborSolid) {
                        stencilField->get(iX + offsetStencil.x, iY + offsetStencil.y, iZ + offsetStencil.z) = -1;
                        continue;
                    } else {
                        stencilField->get(iX + offsetStencil.x, iY + offsetStencil.y, iZ + offsetStencil.z) = 1;
                        continue;
                    }
                }
                if(distance <= maxDistance || dx*(offset.z+iZ) <= height || height<=0) {
                    stencilField->get(iX + offsetStencil.x, iY + offsetStencil.y, iZ + offsetStencil.z) = 1;
                }
            }
        }
    }
}

template<typename T, template<typename U> class Descriptor>
void setExternal(MultiBlockLattice3D<T, Descriptor> &multiBlock)
{
    MultiBlockManagement3D const& management = multiBlock.getMultiBlockManagement();
    std::map<plint,BlockLattice3D<T,Descriptor>*> const& blockMap = multiBlock.getBlockLattices();

    //loop over blocks
    for(typename std::map<plint,BlockLattice3D<T,Descriptor>*>::const_iterator it = blockMap.begin(); it != blockMap.end(); ++it) {
        plint blockId = it->first;
        BlockLattice3D<T,Descriptor> *block = it->second;

        //loop over cells
        int nx = block->getNx();
        int ny = block->getNy();
        int nz = block->getNz();

        T cSmagoNew = 1.0;
        for(int iX=0; iX<nx; ++iX) {
            for(int iY=0; iY<ny; ++iY) {
                for(int iZ=0; iZ<nz; ++iZ) {
                    Cell<T,Descriptor>& cell = block->get(iX,iY,iZ);
                    cell.setExternalField(Descriptor<T>::ExternalField::smagoBeginsAt, 1, &cSmagoNew);
                }
            }
        }

    }
}

template <typename T>
void CopyDataToTriangleMesh3D<T>::process(Box3D domain, std::vector<ScalarField3D<T> *> scalarFields)
{
    
    typename RawConnectedTriangleMesh<T>::PVertexIterator vertexIterator = triangleMesh->vertexIterator();         
    std::vector<T> pressureData;
    std::vector<int> tagData;
    
    while (!vertexIterator->end()) {
        // set respective pressure data from scalarField of each process
        typename RawConnectedTriangleMesh<T>::PVertex pVertex = vertexIterator->next();
        Array<T, 3> coordinates = pVertex->get();
        plint tag = pVertex->tag(vertexIDTagging);

        int atomicBlockNum = scalarFields.size();
        for (int iBlock = 0; iBlock < atomicBlockNum; ++iBlock) {
            ScalarField3D<T> &atomicScalarField = *scalarFields[iBlock];
            T realX = coordinates[0] - atomicScalarField.getLocation().x;
            T realY = coordinates[1] - atomicScalarField.getLocation().y;
            T realZ = coordinates[2] - atomicScalarField.getLocation().z;

            T value = 0;
            int count = 0;

            for (plint ix = (plint)floor(realX); ix <= (plint)ceil(realX); ++ix) {
                for (plint iy = (plint)floor(realY); iy <= (plint)ceil(realY); ++iy) {
                    for (plint iz = (plint)floor(realZ); iz <= (plint)ceil(realZ); ++iz) {
                        if (ix < 0 || ix >= atomicScalarField.getNx() || 
                            iy < 0 || iy >= atomicScalarField.getNy() ||
                            iz < 0 || iz >= atomicScalarField.getNz() ||
                            fabs(atomicScalarField.get(ix, iy, iz)) < 1e-12) {
                            continue;
                        } 
                        count++;
                        value += atomicScalarField.get(ix, iy, iz);
                    }
                }
            }
            if (count != 0) {
                value /= count;
            } else {
                continue;
            }
 
            // overlap between atomic blocks 
            if (!tagData.empty() && tagData.back() == tag) {
                T pressure = pressureData.back();
                pressureData.back() = (pressure + value) / 2;
            } else {
                pressureData.push_back(value);
                tagData.push_back(tag);
            }
        
        }
    }
    collectSurfacePressure(tagData, pressureData);
}

template<typename T>
void CopyDataToTriangleMesh3D<T>::collectSurfacePressure(std::vector<int> &tagData, std::vector<T> &pressureData) {
    // initialize the recvBuf by main processor 
    char dataBuf[MAX_BUF_SIZE];      
    T recvPressureBuf[MAX_BUF_SIZE];
    int recvTagBuf[MAX_BUF_SIZE];

    memset(dataBuf, 0, MAX_BUF_SIZE);
    if (global::mpi().isMainProcessor()) {
        for (int i = 0; i < pressureData.size(); ++i) { 
            triangleMesh->vertex(tagData[i])->setProperty(propertyID, pressureData[i]);
        }
        
        int size = global::mpi().getSize();
        for (int iProc = 1; iProc < size; ++iProc) {
            MPI_Status status;
            int count = 0, position = 0;
            memset(recvPressureBuf, 0, MAX_BUF_SIZE);
            memset(recvTagBuf, 0, MAX_BUF_SIZE);
            memset(dataBuf, 0, MAX_BUF_SIZE);

            MPI_Recv(dataBuf, MAX_BUF_SIZE, MPI_PACKED, iProc, 1, global::mpi().getGlobalCommunicator(), MPI_STATUS_IGNORE);
            MPI_Unpack(dataBuf, MAX_BUF_SIZE, &position, &count, 1, MPI_INT, global::mpi().getGlobalCommunicator());
            MPI_Unpack(dataBuf, MAX_BUF_SIZE, &position, recvPressureBuf, count, MPI_DOUBLE, global::mpi().getGlobalCommunicator());
            MPI_Unpack(dataBuf, MAX_BUF_SIZE, &position, recvTagBuf, count, MPI_INT, global::mpi().getGlobalCommunicator());         
            
            for (int i = 0; i < count; ++i) {
                T newValue = recvPressureBuf[i];
                typename RawConnectedTriangleMesh<T>::PVertex pVertex = triangleMesh->vertex(recvTagBuf[i]);
                T oldValue = pVertex->property(propertyID);
                if (fabs(oldValue) < 1e-12) {
                    pVertex->setProperty(propertyID, newValue);
                } else if (fabs(newValue) < 1e-12) {
                    pVertex->setProperty(propertyID, oldValue);
                } else {
                    pVertex->setProperty(propertyID, (oldValue + newValue) / 2);
                }
                
            }
        }

    } else {
        int package_size = 0, size = pressureData.size();
        MPI_Pack(&size, 1, MPI_INT, dataBuf, MAX_BUF_SIZE, &package_size, global::mpi().getGlobalCommunicator());
        MPI_Pack(pressureData.data(), pressureData.size(), MPI_DOUBLE, dataBuf, MAX_BUF_SIZE, &package_size, global::mpi().getGlobalCommunicator());
        MPI_Pack(tagData.data(), tagData.size(), MPI_INT, dataBuf, MAX_BUF_SIZE, &package_size, global::mpi().getGlobalCommunicator());
        MPI_Send(dataBuf, package_size, MPI_PACKED, 0, 1, global::mpi().getGlobalCommunicator());
    }
}


template <typename T, template <typename U> class Descriptor, typename SurfaceData>
void CalculateDataOnTriangleMesh3D<T, Descriptor, SurfaceData>::processGenericBlocks(Box3D domain, std::vector<AtomicBlock3D *> blocks) {
    
    typename RawConnectedTriangleMesh<T>::PVertexIterator vertexIterator = triangleMesh->vertexIterator();
    Array<T, 3> totalUtau;
    totalUtau[0] = totalUtau[1] = totalUtau[2] = 0; 
    T totalRho = 0, totalCount = 0, averageRho;

    AtomicContainerBlock3D *offLatticeInfo = dynamic_cast<AtomicContainerBlock3D *>(blocks.back());
    Array<T, 9>& localForces = offLatticeModel3D->getForce(*offLatticeInfo);

    while (!vertexIterator->end()) {
        // set respective pressure data from scalarField of each process
        typename RawConnectedTriangleMesh<T>::PVertex pVertex = vertexIterator->next();
        Array<T, 3> coordinates = pVertex->get();
        Array<T, 3> normal = pVertex->normal();
        T area = pVertex->area();

        Array<T, 3> pressure;
        for (int i = 0; i < 3; ++i) {
            pressure[i] = normal[i] * pVertex->property(propertyID);
            localForces[i] += pressure[i] * area;
        }
    }

    BlockLattice3D<T, Descriptor>* lattice = dynamic_cast<BlockLattice3D<T, Descriptor> *>(blocks[numCompletionArgs]);
    TensorField3D<T, 4> const *modifyRhoVelocityField = dynamic_cast<TensorField3D<T, 4> const *>(blocks[6]);
    ScalarField3D<T> const *utauField = dynamic_cast<ScalarField3D<T> const *>(blocks[4]);
    ScalarField3D<int> const *stencilFlagField = dynamic_cast<ScalarField3D<int> const *>(blocks[2]);
    TensorField3D<T, 3> const *wallDistanceY = dynamic_cast<TensorField3D<T, 3> const *>(blocks[1]);

    const Dot3D offsetModify = computeRelativeDisplacement(*lattice, *modifyRhoVelocityField);
    const Dot3D offsetUtau = computeRelativeDisplacement(*lattice, *utauField);
    const Dot3D offsetStencil = computeRelativeDisplacement(*lattice, *stencilFlagField);
    const Dot3D offsetDistanceY = computeRelativeDisplacement(*lattice, *wallDistanceY);

    for (plint ix = domain.x0; ix <= domain.x1; ++ix) {
        for (plint iy = domain.y0; iy <= domain.y1; ++iy) {
            for (plint iz = domain.z0; iz <= domain.z1; ++iz) {
                // boundary node
                if (stencilFlagField->get(ix + offsetStencil.x, iy + offsetStencil.y, iz + offsetStencil.z) == 1) {
                    Array<T, 4> const &modifyRhoVelocity = modifyRhoVelocityField->get(ix + offsetModify.x, iy + offsetModify.y, iz + offsetModify.z);
                    const T utau = utauField->get(ix + offsetUtau.x, iy + offsetUtau.y, iz + offsetUtau.z);
                    const T rho = modifyRhoVelocity[0];
                   
                    Array<T, 3> const &distanceNormal = wallDistanceY->get(ix + offsetDistanceY.x, iy + offsetDistanceY.y, iz + offsetDistanceY.z);
                    Array<T, 3> normal;
                    T distance = std::sqrt(distanceNormal[0] * distanceNormal[0] + distanceNormal[1] * distanceNormal[1] + distanceNormal[2] * distanceNormal[2]);
                    normal[0] = distanceNormal[0] / (1.0e-12 + distance);
                    normal[1] = distanceNormal[1] / (1.0e-12 + distance);
                    normal[2] = distanceNormal[2] / (1.0e-12 + distance);  

                    Array<T, 3> vel;                  
                    vel[0] = modifyRhoVelocity[1];
                    vel[1] = modifyRhoVelocity[2];
                    vel[2] = modifyRhoVelocity[3];

                    Array<T, 3> velocityTangential, vtNorm;
                    T velocity = sqrt(vel[0] * vel[0] + vel[1] * vel[1] + vel[2] * vel[2]);
                    velocityTangential[0] = vel[0] - velocity * normal[0];
                    velocityTangential[1] = vel[1] - velocity * normal[1];
                    velocityTangential[2] = vel[2] - velocity * normal[2];
                    T tangentialVelocity = std::sqrt(velocityTangential[0] * velocityTangential[0] + velocityTangential[1] * velocityTangential[1] + velocityTangential[2] * velocityTangential[2]);
                    vtNorm[0] = velocityTangential[0] / tangentialVelocity;
                    vtNorm[1] = velocityTangential[1] / tangentialVelocity;
                    vtNorm[2] = velocityTangential[2] / tangentialVelocity;
                    
                    for (int i = 0; i < 3; ++i) {
                        T u = utau * vtNorm[i];
                        totalUtau[i] += u;
                    }
                    totalRho += rho;
                    totalCount += 1;                    
                }
            }
        }
    }

    if (totalCount != 0) {
        averageRho = totalRho / totalCount;
        totalUtau[0] /= totalCount;
	    totalUtau[1] /= totalCount;
	    totalUtau[2] /= totalCount;
    } else {
        averageRho = 0;
    }

    localForces[0] += totalUtau[0] * totalUtau[0] * averageRho * fScale * fScale;
    localForces[1] += totalUtau[1] * totalUtau[1] * averageRho * fScale * fScale;
    localForces[2] += totalUtau[2] * totalUtau[2] * averageRho * fScale * fScale;  

}

}  // namespace plb


#endif // NEAR_WALL_MODEL_3D_HH
