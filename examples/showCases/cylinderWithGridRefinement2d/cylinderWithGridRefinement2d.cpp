/* This file is part of the Palabos library.
 *
 * The Palabos softare is developed since 2011 by FlowKit-Numeca Group Sarl
 * (Switzerland) and the University of Geneva (Switzerland), which jointly
 * own the IP rights for most of the code base. Since October 2019, the
 * Palabos project is maintained by the University of Geneva and accepts
 * source code contributions from the community.
 *
 * Contact:
 * Jonas Latt
 * Computer Science Department
 * University of Geneva
 * 7 Route de Drize
 * 1227 Carouge, Switzerland
 * jonas.latt@unige.ch
 *
 * The most recent release of Palabos can be downloaded at
 * <https://palabos.unige.ch/>
 *
 * The library Palabos is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * The library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** \file
 * Flow around a 2D cylinder inside a channel, with the creation of a von
 * Karman vortex street. This example makes use of bounce-back nodes to
 * describe the shape of the cylinder. The outlet is modeled through a
 * Neumann (zero velocity-gradient) condition.
 */

#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <vector>

#include "palabos2D.h"
#include "palabos2D.hh"
#include "utility_cylinder2d_param.h"

using namespace plb;
using namespace plb::descriptors;
using namespace std;

typedef double T;
#define DESCRIPTOR D2Q9Descriptor
#define N 100
#define LOCATION_X (N * 0.6 * 2 * 2 * 2 * 2)
#define LOCATION_Y (N * 0.5 * 2 * 2 * 2 * 2)
#define RADIUS (N * 0.1 * 2 * 2 * 2 * 2)

//////// Select the collision model from data in the .xml file.
template <typename T, template <typename U> class Descriptor>
Dynamics<T, Descriptor> *getDynamics(Param<T> &param)
{
    Dynamics<T, Descriptor> *dyn;
    if (param.dynName == "BGK_Ma2") {  // BGK with second-order equilibrium
        dyn = new BGKdynamics<T, Descriptor>(param.omega);
    } else if (param.dynName == "RM")
    {  // Collision based on raw moment space (equivalent to Complete_BGK if hoOmega=SRT)
        dyn = new RMdynamics<T, Descriptor>(param.omega);
    } else if (param.dynName == "HM")
    {  // Collision based on Hermite moment space (equivalent to Complete_BGK if hoOmega=SRT)
        dyn = new HMdynamics<T, Descriptor>(param.omega);
    } else if (param.dynName == "CM")
    {  // Collision based on central moment space (equivalent to Complete_BGK if hoOmega=SRT)
        dyn = new CMdynamics<T, Descriptor>(param.omega);
    } else if (param.dynName == "CHM") {  // Collision based on central Hermite moment space
                                          // (equivalent to Complete_BGK if hoOmega=SRT)
        dyn = new CHMdynamics<T, Descriptor>(param.omega);
    } else if (param.dynName == "K") {  // Collision based on cumulant space
        dyn = new Kdynamics<T, Descriptor>(param.omega);
    } else if (param.dynName == "GH")
    {  // Collision based on Gauss-Hermite quadrature (HM with weighted scalar product, equivalent
       // to Complete_BGK if hoOmega=SRT)
        dyn = new GHdynamics<T, Descriptor>(param.omega);
    } else if (param.dynName == "RR") {  // Recursive regularization of populations (equivalent to
                                         // Complete_Regularized_BGK if hoOmega=SRT)
        dyn = new RRdynamics<T, Descriptor>(param.omega);
    } else {
        pcout << "Error: Dynamics name does not exist, please choose among BGK_Ma2, RM, HM, CM, "
                 "CHM, K, GH and RR."
              << std::endl;
        exit(-1);
    }

    return dyn;
}

/// Velocity on the parabolic Poiseuille profile
T poiseuilleVelocity(plint iY, IncomprFlowParam<T> const &parameters)
{
    T y = (T)iY / parameters.getResolution();
    return 4. * parameters.getLatticeU() * (y - y * y);
}

/// Linearly decreasing pressure profile
T poiseuillePressure(plint iX, IncomprFlowParam<T> const &parameters)
{
    T Lx = parameters.getNx() - 1;
    T Ly = parameters.getNy() - 1;
    return 8. * parameters.getLatticeNu() * parameters.getLatticeU() / (Ly * Ly)
           * (Lx / (T)2 - (T)iX);
}

/// Convert pressure to density according to ideal gas law
T poiseuilleDensity(plint iX, IncomprFlowParam<T> const &parameters)
{
    return poiseuillePressure(iX, parameters) * DESCRIPTOR<T>::invCs2 + (T)1;
}

/// A functional, used to initialize the velocity for the boundary conditions
template <typename T>
class PoiseuilleVelocity {
public:
    PoiseuilleVelocity(IncomprFlowParam<T> parameters_) : parameters(parameters_) { }
    void operator()([[maybe_unused]] plint iX, plint iY, Array<T, 2> &u) const
    {
        u[0] = poiseuilleVelocity(iY, parameters);
        u[1] = T();
    }

private:
    IncomprFlowParam<T> parameters;
};

/// A functional, used to initialize a pressure boundary to constant density
template <typename T>
class ConstantDensity {
public:
    ConstantDensity(T density_) : density(density_) { }
    T operator()([[maybe_unused]] plint iX, [[maybe_unused]] plint iY) const
    {
        return density;
    }

private:
    T density;
};

/// A functional, used to create an initial condition for the density and velocity
template <typename T>
class PoiseuilleVelocityAndDensity {
public:
    PoiseuilleVelocityAndDensity(IncomprFlowParam<T> parameters_) : parameters(parameters_) { }
    void operator()(plint iX, plint iY, T &rho, Array<T, 2> &u) const
    {
        rho = poiseuilleDensity(iX, parameters);
        u[0] = poiseuilleVelocity(iY, parameters);
        u[1] = T();
    }

private:
    IncomprFlowParam<T> parameters;
};

/// A functional, used to instantiate bounce-back nodes at the locations of the cylinder
template <typename T>
class CylinderShapeDomain2D : public plb::DomainFunctional2D {
public:
    CylinderShapeDomain2D(plb::plint cx_, plb::plint cy_, plb::plint radius) :
        cx(cx_), cy(cy_), radiusSqr(plb::util::sqr(radius))
    { }
    virtual bool operator()(plb::plint iX, plb::plint iY) const
    {
        return plb::util::sqr(iX - cx) + plb::util::sqr(iY - cy) <= radiusSqr;
    }
    virtual CylinderShapeDomain2D<T> *clone() const
    {
        return new CylinderShapeDomain2D<T>(*this);
    }

private:
    plb::plint cx;
    plb::plint cy;
    plb::plint radiusSqr;
};

void outsideCylinderSetup(
    MultiGridLattice2D<T, DESCRIPTOR> &lattice, const ConvectiveRefinementParameters<T> &parameters,
    OnLatticeBoundaryCondition2D<T, DESCRIPTOR> &boundaryCondition)
{
    const plint nx = parameters[0].getNx();
    const plint ny = parameters[0].getNy();
    pcout << "nx: " << nx << " ny: " << ny << endl;
    Box2D outlet(nx - 1, nx - 1, 1, ny - 2);

    // Create Velocity boundary conditions everywhere
    boundaryCondition.setVelocityConditionOnBlockBoundaries(lattice.getComponent(0), Box2D(0, 0, 1, ny - 2));
    boundaryCondition.setVelocityConditionOnBlockBoundaries(lattice.getComponent(0), Box2D(0, nx - 1, 0, 0));
    boundaryCondition.setVelocityConditionOnBlockBoundaries(
        lattice.getComponent(0), Box2D(0, nx - 1, ny - 1, ny - 1));
    // .. except on right boundary, where we prefer an outflow condition
    //    (zero velocity-gradient).
    boundaryCondition.setVelocityConditionOnBlockBoundaries(
        lattice.getComponent(0), Box2D(nx - 1, nx - 1, 1, ny - 2), boundary::outflow);

    setBoundaryVelocity(lattice.getComponent(0), lattice.getComponent(0).getBoundingBox(), PoiseuilleVelocity<T>(parameters[0]));
    initializeAtEquilibrium(
        lattice.getComponent(0), lattice.getComponent(0).getBoundingBox(), PoiseuilleVelocityAndDensity<T>(parameters[0]));

    setBoundaryVelocity(lattice.getComponent(1), lattice.getComponent(1).getBoundingBox(), PoiseuilleVelocity<T>(parameters[1]));   
    initializeAtEquilibrium(
        lattice.getComponent(1), lattice.getComponent(1).getBoundingBox(), PoiseuilleVelocityAndDensity<T>(parameters[1]));

    setBoundaryVelocity(lattice.getComponent(2), lattice.getComponent(2).getBoundingBox(), PoiseuilleVelocity<T>(parameters[2]));   
    initializeAtEquilibrium(
        lattice.getComponent(2), lattice.getComponent(2).getBoundingBox(), PoiseuilleVelocityAndDensity<T>(parameters[2]));

    setBoundaryVelocity(lattice.getComponent(3), lattice.getComponent(3).getBoundingBox(), PoiseuilleVelocity<T>(parameters[3]));   
    initializeAtEquilibrium(
        lattice.getComponent(3), lattice.getComponent(3).getBoundingBox(), PoiseuilleVelocityAndDensity<T>(parameters[3]));

    setBoundaryVelocity(lattice.getComponent(4), lattice.getComponent(4).getBoundingBox(), PoiseuilleVelocity<T>(parameters[4]));   
    initializeAtEquilibrium(
        lattice.getComponent(4), lattice.getComponent(4).getBoundingBox(), PoiseuilleVelocityAndDensity<T>(parameters[4]));

    plint cx = LOCATION_X;
    plint cy = LOCATION_Y;  // cy is slightly offset to avoid full symmetry,
                            //   and to get a Von Karman Vortex street.
    plint radius = RADIUS;
     
    defineDynamics(
        lattice.getComponent(4), lattice.getComponent(4).getBoundingBox(), new CylinderShapeDomain2D<T>(cx, cy, radius),
        new plb::BounceBack<T, DESCRIPTOR>);

    lattice.initialize();

}

/// Produce a GIF snapshot of the velocity-norm.
template <typename T, template <typename U> class Descriptor>
void writeGifs(MultiBlockLattice2D<T, Descriptor> lattice, plint iter, string fName)
{
    plint imSize = 300;
    ImageWriter<T> imageWriter("leeloo");
    imageWriter.writeScaledGif(
        createFileName(fName, iter, 6), *computeVelocityNorm(lattice), imSize, imSize);
}

int main(int argc, char *argv[])
{
    plbInit(&argc, &argv);

    global::directories().setOutputDir("./tmp/");
    global::setDefaultMultiScaleManager(new ConvectiveMultiScaleManager());    

    double lx = 2.6, ly = 1.0;
    double totalX = N * lx, totalY = N * ly;

    IncomprFlowParam<T> parameters(
        (T)1e-2,  // uMax
        (T)60.,  // Re
        N,      // N
        lx,       // lx
        ly        // ly
    );

    const T logT = (T)0.005;
#ifdef PLB_REGRESSION
    const T maxT = (T)0.1;
#else
    const T imSave = (T)0.06;
    const T vtkSave = (T)1.;
    const T maxT = (T)3.;
#endif

    writeLogFile(parameters, "Poiseuille flow");

    int numLevel = 5;
    int overlapWidth = 1;
    int behaviorLevel = 0, referenceLevel = 0;

    // we choose a convective refinement dx=dt and compute parameters for each level
    ConvectiveRefinementParameters<T> refinementParameters(numLevel, referenceLevel, parameters);

    MultiGridManagement2D management(
        parameters.getNx(), parameters.getNy(), numLevel, overlapWidth, behaviorLevel);


    Box2D refinementLevel0(
        4.0 / 26.0 * totalX, 8.0 / 26.0 * totalX, 
        0, totalY);  // refine the middle of the coarse domain

    management.refine(0, refinementLevel0);
    management.refine(1, refinementLevel0.multiply(2));
    management.refine(2, refinementLevel0.multiply(4));
    management.refine(3, refinementLevel0.multiply(8));

    Param<T> param(argv[1]);
    // Creating the multi-grid lattice
        ///// Initialize the relaxation matrix from the .xml file.
    Array<T, DESCRIPTOR<T>::numRelaxationTimes> allOmega;
    if (param.lbm == "D2Q9") {
        allOmega[0] = param.omega;      // relaxation of M20 and M02
        allOmega[1] = param.omega;      // relaxation of M11
        allOmega[2] = param.omega3;     // relaxation of M21 and M12
        allOmega[3] = param.omega4;     // relaxation of M22
        allOmega[4] = param.omegaBulk;  // relaxation of bulk moment (M20 + M02)
    } else {
        pcout << "Error: lattice does not exist." << std::endl;
        return EXIT_FAILURE;
    }

    Dynamics<T, DESCRIPTOR> *backgroundDynamics = getDynamics<T, DESCRIPTOR>(param);

    ///// Initialize relexation frequencies from those in "param"
    if (param.dynName == "RM") {
        RMdynamics<T, DESCRIPTOR>::allOmega = allOmega;
    } else if (param.dynName == "HM") {
        HMdynamics<T, DESCRIPTOR>::allOmega = allOmega;
    } else if (param.dynName == "CM") {
        CMdynamics<T, DESCRIPTOR>::allOmega = allOmega;
    } else if (param.dynName == "CHM") {
        CHMdynamics<T, DESCRIPTOR>::allOmega = allOmega;
    } else if (param.dynName == "K") {
        Kdynamics<T, DESCRIPTOR>::allOmega = allOmega;
    } else if (param.dynName == "GH") {
        GHdynamics<T, DESCRIPTOR>::allOmega = allOmega;
    } else if (param.dynName == "RR") {
        RRdynamics<T, DESCRIPTOR>::allOmega = allOmega;
    }

    // // perform a naive parallelization along the x axis
    plint procNumber = global::mpi().getSize();
    plint yTiles = 1;
    plint xTiles = procNumber;
    // object that performs the parallelization of the management
    ParallellizeBySquares2D *parallelizer = new ParallellizeBySquares2D(
        management.getBulks(), management.getBoundingBox(4), xTiles, yTiles);
    management.parallelize(parallelizer);

    MultiGridLattice2D<T, DESCRIPTOR> lattice =
        *MultiGridGenerator2D<T, DESCRIPTOR>::createRefinedLatticeCubicInterpolationFiltering(
            management, backgroundDynamics, behaviorLevel);

    OnLatticeBoundaryCondition2D<T, DESCRIPTOR> *outsideBoundaryCondition =
        createLocalBoundaryCondition2D<T, DESCRIPTOR>();

    outsideCylinderSetup(lattice, refinementParameters, *outsideBoundaryCondition);

    // Small code that allows us to visualize the refined domain as SVG
    ////////////////////////////////////////////////////////////////////////////
    std::vector<MultiScalarField2D<int> > dyn;
    std::vector<std::map<int, std::string> > idToName(numLevel);

    std::set<std::string> uniqueNames;
    for (plint iLevel = 0; iLevel < numLevel; ++iLevel) {
        dyn.push_back(*extractDynamicsChain(lattice.getComponent(iLevel), idToName[iLevel]));
        std::map<int, std::string>::iterator it = idToName[iLevel].begin();
        for (; it != idToName[iLevel].end(); ++it) {
            uniqueNames.insert(it->second);
        }
        pcout << getMultiBlockInfo(lattice.getComponent(iLevel));
    }

    std::map<std::string, int> nameToColor;
    std::set<std::string>::iterator it = uniqueNames.begin();
    int color = 0;
    for (; it != uniqueNames.end(); ++it) {
        nameToColor[*it] = color++;
    }

    SVGWriter2D test(management);
    test.writeDomainsWithDynamicsInfo("cylinder.svg", dyn, idToName, nameToColor);

    // Main loop over time iterations.
    for (plint iT = 0; iT * parameters.getDeltaT() < maxT; ++iT) {
        // At this point, the state of the lattice corresponds to the
        //   discrete time iT. However, the stored averages (getStoredAverageEnergy
        //   and getStoredAverageDensity) correspond to the previous time iT-1.
        // pcout << "delta x: " << parameters.getDeltaX() << endl;
        // pcout << "lattice U: " << parameters.getLatticeU() << endl;
        // pcout << "physical U: " << parameters.getPhysicalU() << endl;
        // pcout << "delta t: " << parameters.getDeltaT() << endl;
        if (iT % parameters.nStep(logT) == 0) {
            int interpLevel = numLevel - 1;
            MultiBlockLattice2D<T, DESCRIPTOR> r = *lattice.convertToLevel(interpLevel);
            writeGifs(r, iT, "cylinder2dWithRefine");
            pcout << "step " << iT << "; t=" << iT * parameters.getDeltaT();
        }

        // Lattice Boltzmann iteration step.
        lattice.collideAndStream();

        // At this point, the state of the lattice corresponds to the
        //   discrete time iT+1, and the stored averages are upgraded to time iT.
        if (iT % parameters.nStep(logT) == 0) {
            pcout << "; av energy =" << setprecision(10) << getStoredAverageEnergy<T>(lattice)
                  << "; av rho =" << getStoredAverageDensity<T>(lattice) << endl;
        }

    }
            
    delete outsideBoundaryCondition;

}
